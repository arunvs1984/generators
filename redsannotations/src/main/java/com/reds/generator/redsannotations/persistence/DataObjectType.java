package com.reds.generator.redsannotations.persistence;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * <b>Purpose:</b> Annotation to represent the field is data object type
 * @author Shineed Basheer
 *
 */
@Documented
@Retention(SOURCE)
@Target(ElementType.FIELD)
public @interface DataObjectType {

}
