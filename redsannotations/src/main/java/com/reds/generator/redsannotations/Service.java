/**
 * 
 */
package com.reds.generator.redsannotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 * <b>Purpose:</b>Annotation to mark a reds service interfacess
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface Service {
	String type() default "business";
	String name();
	String root() default "redsservices";
}
