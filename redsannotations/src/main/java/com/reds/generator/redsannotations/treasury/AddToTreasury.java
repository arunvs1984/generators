/**
 * 
 */
package com.reds.generator.redsannotations.treasury;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 * <b>Purpose:</b>If you want any object to be provided by treasury. Use this
 * annotation to add to treasury
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface AddToTreasury {
	
	boolean singleton() default true;
	boolean interfaceRequired() default false;

}
