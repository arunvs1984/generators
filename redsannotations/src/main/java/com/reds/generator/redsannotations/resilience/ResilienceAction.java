package com.reds.generator.redsannotations.resilience;

public enum ResilienceAction {
	NONE, BLOCK, TIME_OUT;
}
