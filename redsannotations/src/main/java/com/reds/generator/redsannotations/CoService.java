/**
 * 
 */
package com.reds.generator.redsannotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.reds.generator.redsannotations.resilience.ResilienceAction;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 * <b>Purpose:</b>Annotation to mark a reds service interfacess
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
@Repeatable(value = Required.class)
public @interface CoService {
	Class<?> service();

	String name();

	String version();
	
	ResilienceAction resilience() default ResilienceAction.NONE;

	String id() default "";
	
	/**
	 * @return Only used when Web Services
	 */
	boolean exposeAsWeb() default false;
	/**
	 * @return Normal Expose as Service
	 */
	boolean expose() default false;
	
}
