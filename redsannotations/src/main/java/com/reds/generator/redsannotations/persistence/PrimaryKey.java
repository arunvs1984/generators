/**
 * 
 */
package com.reds.generator.redsannotations.persistence;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(ElementType.FIELD)
/**
 * <b>Purpose:</b>Annotation to represent a Field is primary key
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface PrimaryKey {
	boolean autoIncrement() default true;

}
