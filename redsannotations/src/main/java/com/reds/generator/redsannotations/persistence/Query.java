/**
 * 
 */
package com.reds.generator.redsannotations.persistence;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 * <b>Purpose:</b>Annotation to represent an Object is Persistence capable
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface Query {

	String name();

	String argName() default "other";

	/**
	 * this.lastName == 'Jones' && this.age < age_limit && this.address = address
	 * 
	 * @return
	 */
	String filter() default "";

	/**
	 * int age_limit,String address
	 * 
	 * @return
	 */
	String declareParams() default "";

	/**
	 * age,address
	 * 
	 * @return
	 */
	String setParams() default "";

	boolean unique() default false;
}
