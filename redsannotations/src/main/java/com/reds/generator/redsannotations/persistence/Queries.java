/**
 * 
 */
package com.reds.generator.redsannotations.persistence;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 *
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface Queries {

	Query[] value() default {};
}
