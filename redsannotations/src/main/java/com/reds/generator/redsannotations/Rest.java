/**
 * 
 */
package com.reds.generator.redsannotations;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(ElementType.METHOD)
/**
 * <b>Purpose:</b>Annotation to mark a reds service interfacess
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface Rest {

}
