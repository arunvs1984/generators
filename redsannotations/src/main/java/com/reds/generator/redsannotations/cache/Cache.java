/**
 * 
 */
package com.reds.generator.redsannotations.cache;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(SOURCE)
@Target(TYPE)
/**
 * <b>Purpose:</b>Annotation to represent an Object is Persistence capable
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public @interface Cache {

	String name();

	
}
