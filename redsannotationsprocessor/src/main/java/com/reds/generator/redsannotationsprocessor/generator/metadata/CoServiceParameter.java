package com.reds.generator.redsannotationsprocessor.generator.metadata;

public class CoServiceParameter {
	private String type;
	private String name;
	private String fullName;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
