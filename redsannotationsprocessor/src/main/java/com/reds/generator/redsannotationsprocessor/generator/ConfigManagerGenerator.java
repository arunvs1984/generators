package com.reds.generator.redsannotationsprocessor.generator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;

import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigManagerMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigManagerMetaDataWrapper;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigMetaData;
import com.reds.generator.redsannotationsprocessor.tracker.RedsannotationsprocessorTrack;
import com.reds.library.redscommons.exception.RedscommonsException;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.platform.redsplatformcommons.config.AbstractConfigurationManager;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.MethodSpec.Builder;
import com.squareup.javapoet.TypeSpec;

import dagger.Module;
import dagger.Provides;

public class ConfigManagerGenerator extends AbstractRedsGenerator<ConfigManagerMetaData> {

		private Messager messager;
		private Filer filer;
		
	
	public ConfigManagerGenerator(Messager messager, Filer filer) {
			super();
			this.messager = messager;
			this.filer = filer;
		}
	
    private void log(String message) {
        messager.printMessage(Diagnostic.Kind.NOTE, message);
    }




	@Override
	public void generate(ConfigManagerMetaData metaData) {
		try {
			ConfigManagerMetaDataWrapper wrapper = new ConfigManagerMetaDataWrapper(metaData);
			String className = RedsStringUtils.capitalize(metaData.getName()+RedsGeneratorConstants.GENERATED_CONFIG_MANAGER_POSTFIX);
			wrapper.setActualClassName(className);
			MethodSpec constructor=getConstructor(wrapper);
			List<MethodSpec> methods = getMethods(wrapper);			
			methods.add(constructor);					
			com.squareup.javapoet.TypeSpec.Builder typeBuilder=	TypeSpec
					.classBuilder(className)
					.addJavadoc(getJavaDoc(className,metaData.getAuthor()))
					.superclass(AbstractConfigurationManager.class)
					.addAnnotation(Module.class)
					.addModifiers(Modifier.PUBLIC, Modifier.FINAL);					
			for(MethodSpec methodSpec:methods){
				typeBuilder.addMethod(methodSpec);
			}
			TypeSpec typeSpec =typeBuilder.build();

			JavaFile javaFile = JavaFile.builder(wrapper.getActualPackage()+"."+wrapper.getRedsGenfolderName(), typeSpec).build();
			javaFile.writeTo(System.out);			
			javaFile.writeTo(filer);
			log("Generated "+className);
		} catch (IOException e) {
			RedsannotationsprocessorTrack.me.error("Failed to generate config manager", e);
		}
	}

	
	private String getJavaDoc(String serviceName,String author) {
		String javaDoc="<b>Purpose: </b> Configuration class for "+serviceName+
				"\n\n @author <b>"+author+"</b>"
						+ "\n\n @since ";//+ RedsDateUtils.getCurrenTimeStamp();
		return javaDoc;
	}

	private MethodSpec getConstructor(ConfigManagerMetaDataWrapper wrapper) {
		Builder builder = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);

		String trackerName = wrapper.getTrackerName();
		ClassName tracker = ClassName.get(wrapper.getTrackerPackage(), trackerName);

		String treasuryName = wrapper.getTreasuryName();
		ClassName treasury = ClassName.get(wrapper.getTreasuryPackage(), treasuryName);

		ClassName redscommonsexcetpion = ClassName.get(RedscommonsException.class.getPackage().getName(),
				RedscommonsException.class.getSimpleName());
		builder.addStatement("super($T.serviceContext.getConfigLocation())", treasury);
		builder.addStatement("register()");
		String init = "try {\r\n" + "\tinit();\r\n" + "} catch ($T e) {\r\n"
				+ "\t$T.me.error(\"Failed to initialize configurations \", e);\r\n" + "}\n";
		builder.addCode(init, redscommonsexcetpion, tracker);
		return builder.build();

	}

	private MethodSpec getInitMethod(ConfigManagerMetaDataWrapper wrapper) {
		Builder builder = MethodSpec.methodBuilder("init").addModifiers(Modifier.PRIVATE)
				.addException(RedscommonsException.class).returns(void.class);
		for (ConfigMetaData configMetaData : wrapper.getConfigMetaData().getConfigs()) {
			ClassName configName = ClassName.get(wrapper.getConfigPackage(),
					RedsStringUtils.capitalize(configMetaData.getName()));
			builder.addStatement("super.initConfig(new $T().getDefault())", configName);
		}
		return builder.build();
	}

	private List<MethodSpec> getMethods(ConfigManagerMetaDataWrapper wrapper) {
		List<MethodSpec> methodSpecs = new ArrayList<>();

		Builder registerMethod = MethodSpec.methodBuilder(wrapper.getRegisterMethodName())
				.addModifiers(Modifier.PRIVATE).returns(void.class);

		Builder initMethod = MethodSpec.methodBuilder("init").addModifiers(Modifier.PRIVATE)
				.addException(RedscommonsException.class).returns(void.class);

		String trackerName = wrapper.getTrackerName();
		ClassName tracker = ClassName
				.get(wrapper.getTrackerPackage(), trackerName);
		ClassName redscommonsexcetpion = ClassName.get(RedscommonsException.class.getPackage().getName(),
				RedscommonsException.class.getSimpleName());

		for (ConfigMetaData configMetaData : wrapper.getConfigMetaData().getConfigs()) {
			String configName = RedsStringUtils.capitalize(configMetaData.getName());
			ClassName configClassName = ClassName.get(wrapper.getConfigPackage(), configName);
			registerMethod.addStatement("super.registerConfig($T.class)", configClassName);
			initMethod.addStatement("super.initConfig(new $T().getDefault())", configClassName);
			
			Builder provideMethod = MethodSpec
					.methodBuilder("provide" + configName)
					.addJavadoc("<b>Purpose: </b> To provide..."+configName)
					.addModifiers(Modifier.PUBLIC)
					.addAnnotation(Provides.class)
					.returns(configClassName);
			
			provideMethod.addCode(getProvideCode(), configClassName, configClassName, redscommonsexcetpion, tracker);
			
			methodSpecs.add(provideMethod.build());

		}
		methodSpecs.add(registerMethod.build());
		methodSpecs.add(initMethod.build());
		return methodSpecs;
	}

	private String getProvideCode() {
		String code = "try {\r\n" + ""
				+ "\treturn ($T) super.read(new $T());\r\n" + ""
						+ "} catch ($T e) {\r\n"
				+ "\t$T.me.error(\"Failed to read configuration\", e);\r\n" + "}\r\n"
				+ "return null;\n";
		return code;
	}

}
