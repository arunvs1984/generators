package com.reds.generator.redsannotationsprocessor.generator.metadata.component;

import java.util.ArrayList;
import java.util.List;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public class ComponentMetaData extends GeneratorMetaData {

	private ServicesMetaData parentServiceMetaData;

	private List<DataObjectMetaData> dataObjects = null;
	
	private List<CoServiceMetaData> coServiceMetaDatas=null;
	
	
	private ModuleMetaData configManagerModule =null;
	private CoServiceProviderMetaData serviceProviderMetaData=null;

	public ComponentMetaData(ServicesMetaData parentServiceMetaData) {
		super(parentServiceMetaData.getAuthor(), parentServiceMetaData.getName(), parentServiceMetaData.getLocation(),
				parentServiceMetaData.getLayout());
		this.parentServiceMetaData=parentServiceMetaData;
		this.dataObjects = new ArrayList<>();
		this.coServiceMetaDatas= new ArrayList<>();
		this.configManagerModule = new ModuleMetaData(parentServiceMetaData.getName()+RedsGeneratorConstants.GENERATED_CONFIG_MANAGER_POSTFIX,parentServiceMetaData.getLocation());
		this.serviceProviderMetaData=new CoServiceProviderMetaData(parentServiceMetaData.getName()+RedsGeneratorConstants.GENERATED_SERVICE_PROVIDER_POSTFIX,parentServiceMetaData.getLocation());
		
		
	}

	public ServicesMetaData getParentServiceMetaData() {
		return parentServiceMetaData;
	}

	public void setParentServiceMetaData(ServicesMetaData parentServiceMetaData) {
		this.parentServiceMetaData = parentServiceMetaData;
	}

	public List<DataObjectMetaData> getDataObjects() {
		return dataObjects;
	}

	public void setDataObjects(List<DataObjectMetaData> dataObjects) {
		this.dataObjects = dataObjects;
	}

	public ModuleMetaData getConfigManagerModule() {
		return configManagerModule;
	}

	public void setConfigManagerModule(ModuleMetaData configManagerModule) {
		this.configManagerModule = configManagerModule;
	}

	public List<CoServiceMetaData> getCoServiceMetaDatas() {
		return coServiceMetaDatas;
	}

	public CoServiceProviderMetaData getServiceProviderMetaData() {
		return serviceProviderMetaData;
	}

	
}
