package com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider;

import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public class ServiceProviderLayout extends AbstractProjectLayout {
	public ServiceProviderLayout(String rootPackage) {
		setRootPackage(rootPackage);
	}

}
