package com.reds.generator.redsannotationsprocessor.generator.metadata.component;

import com.reds.redsgenerator.api.AbstractMetaDataWrapper;

public class ComponentMetaDataWrapper extends AbstractMetaDataWrapper<ComponentMetaData> {
	private String registerMethodName = "register";
	
	public ComponentMetaDataWrapper(ComponentMetaData metaData) {
		super(metaData);
	}

	public String getRegisterMethodName() {
		return registerMethodName;
	}

	public void setRegisterMethodName(String registerMethodName) {
		this.registerMethodName = registerMethodName;
	}

	
}
