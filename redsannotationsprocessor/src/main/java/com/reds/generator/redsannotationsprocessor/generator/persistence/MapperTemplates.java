package com.reds.generator.redsannotationsprocessor.generator.persistence;

public class MapperTemplates {

	public String listMappingTemplate() {
		String template="public default List\\<<dataObject>> update<to>(List\\<<to>> tos,List\\<<dataObject>> dos, <to>ToDoMapper <to>ToDoMapper) {\r\n" + 
				"		if (tos == null) {\r\n" + 
				"			return dos;\r\n" + 
				"		}\r\n" + 
				"		if (null == dos) {\r\n" + 
				"			dos = new ArrayList\\<>();\r\n" + 
				"		}\r\n" + 
				"		List\\<<dataObject>> tempDos = new ArrayList\\<>();\r\n" + 
				"		for (<to> to : tos) {\r\n" + 
				"			boolean found = false;\r\n" + 
				"			for (<dataObject> dataObject : dos) {\r\n" + 
				"				if (dataObject.getDoId().equals(to.getId())) {\r\n" + 
				"					<to>ToDoMapper.update<dataObject>From<to>(to, dataObject);\r\n" + 
				"					tempDos.add(dataObject);\r\n" + 
				"					found = true;\r\n"+
				"                   break;\r\n" + 
				"				}\r\n" + 
				"			}\r\n" + 
				"			if (!found) {\r\n" + 
				"				<dataObject> dataObject = <to>ToDoMapper.to<dataObject>(to);\r\n" + 
				"				tempDos.add(dataObject);\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		dos.clear();\r\n" + 
				"		dos.addAll(tempDos);\r\n" + 
				"		return tempDos;\r\n" + 
				"	}";
		
		return template;
	}
}
