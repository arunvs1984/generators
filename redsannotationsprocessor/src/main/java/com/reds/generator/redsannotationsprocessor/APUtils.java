package com.reds.generator.redsannotationsprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.reds.generator.redsannotations.CoService;
import com.reds.generator.redsannotations.Required;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceInfo;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceMethod;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceParameter;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.persistence.FieldMetaData;
import com.reds.redsgenerator.api.type.DataType;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.LoaderClassPath;
import javassist.NotFoundException;

/**
 * @author shine
 *
 */
public class APUtils {

	@FunctionalInterface
	public interface GetClassValue {
		void execute() throws MirroredTypeException, MirroredTypesException;
	}

	public static String getClassJavaDoc(String author, String purpose, String date) {
		StringBuilder builder = new StringBuilder();
		builder.append("<b>Auto Generated </b><br>\n").append(" \n\n<b>Purpose: </b> ").append(purpose)
				.append("<br><br>\n\n").append("<b>Created On: </b> ").append(date).append("\n\n@author ")
				.append("</b>").append(author).append("</b>\n\n");

		return builder.toString();
	}

	public static FieldMetaData getPrimaryKey(List<FieldMetaData> fieldMetaDatas) {
		for (FieldMetaData metaData : fieldMetaDatas) {
			if (metaData.isPrimaryKey()) {
				return metaData;
			}
		}
		return null;
	}

	public static List<? extends TypeMirror> getTypeMirrorFromAnnotationValue(GetClassValue c) {
		try {
			c.execute();
		} catch (MirroredTypesException ex) {
			return ex.getTypeMirrors();
		}
		return null;
	}

	public static TypeElement asTypeElement(TypeMirror typeMirror, Types TypeUtils) {
		return (TypeElement) TypeUtils.asElement(typeMirror);
	}

	public static BodyDeclaration<?> getGetterMethod(String type, String fieldName) {
		String template = "public <dataType> <get><fieldNameCaps>() {\n" + "\t return <fieldNameSmall>;" + "\n}";
		ST st = new ST(template);
		st.add("dataType", type);
		if (type.contains("boolean")) {
			st.add("get", "is");
		} else {
			st.add("get", "get");
		}
		st.add("fieldNameSmall", fieldName);
		st.add("fieldNameCaps", RedsStringUtils.capitalize(fieldName));
		String getter = st.render();
		System.out.println("Getter :> " + getter);
		BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(getter);
		declaration.setBlockComment("Getter");
		return declaration;
	}

	public static BodyDeclaration<?> getSetterMethod(String type, String fieldName) {
		String template = "public void set<fieldNameCaps>(<dataType> <fieldNameSmall>) {\n"
				+ "\t this.<fieldNameSmall>=<fieldNameSmall>;" + "\n}";
		ST st = new ST(template);
		st.add("dataType", type);
		st.add("fieldNameSmall", fieldName);
		st.add("fieldNameCaps", RedsStringUtils.capitalize(fieldName));
		BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
		declaration.setBlockComment("Setter");
		return declaration;
	}

	public static String getInterfaceStructure(String className, String packageName, String... imports) {

		String template = "package <packageName>;\n<imports>" + "public interface <className> {}";
		ST st = new ST(template);
		st.add("className", className);
		st.add("packageName", packageName);
		StringBuilder importBuilder = new StringBuilder();
		if (null != imports) {
			for (String importString : imports) {
				importBuilder.append(importString).append("\n");
			}
		}
		st.add("imports", importBuilder.toString());
		return st.render();

	}

	public static String getServiceImplPath(ServiceMetaDataWrapper wrapper) {

		return wrapper.getActualPackagePath() + File.separator + wrapper.getMetaData().getName()
				+ RedsGeneratorConstants.SERVICE_IMPL_CLASS_POSTFIX;
	}

	public static void handleRequiredAnnotation(CompilationUnit classUnit, ClassOrInterfaceDeclaration c, String serviceName,
			String packageName, String version, boolean exposeWeb, boolean expose) {
		Optional<AnnotationExpr> annotation = c.getAnnotationByName(Required.class.getSimpleName());
		if (annotation.isPresent()) {
			AnnotationExpr annotationExpr = annotation.get();
			boolean alreadyExist = false;
			if (annotationExpr != null) {
				SingleMemberAnnotationExpr singleMemberAnnotationExpr = annotationExpr.asSingleMemberAnnotationExpr();
				Expression memberValuePair = singleMemberAnnotationExpr.getMemberValue();
				List<Node> childNodes = memberValuePair.getChildNodes();
				for (Node node : childNodes) {
					NodeList<MemberValuePair> pairs = ((NormalAnnotationExpr) node).getPairs();
					for (MemberValuePair pair : pairs) {
						System.out.println(pair);
						System.out.println(pair.getName());
						// TODO Need to check
						if (pair.toString().indexOf(serviceName) != -1) {
							alreadyExist = true;
						}

					}
				}
				if(!alreadyExist) {
					addCoServiceAnnotation(classUnit, memberValuePair, packageName, serviceName, version, exposeWeb,
							expose);	
				}else {
					System.out.println("Already Added");
				}
				
			}
		}
	}

	public static CompilationUnit getServiceImplClassCompilationUnit(String serviceImplClass)
			throws FileNotFoundException, RedsutilsException {
		File sourceFile = new File(serviceImplClass);
		return JavaParser.parse(sourceFile);
	}

	public static String getProjectPath(Filer apiFiller, boolean api) throws IOException {
		FileObject resource = apiFiller.createResource(StandardLocation.CLASS_OUTPUT, "", "tmp", (Element[]) null);
		String projectPath = Paths.get(resource.toUri()).getParent().getParent().toString();
		if (api) {
			projectPath = projectPath.substring(0, projectPath.lastIndexOf("api"));
		}
		return projectPath;
	}

	public static String getClassStructure(String className, String packageName) {

		String template = "package <packageName>;\n" + "public class <className> {}";
		ST st = new ST(template);
		st.add("className", className);
		st.add("packageName", packageName);
		return st.render();
	}

	public static boolean isMethodAlreadyDeclaredByName(ClassOrInterfaceDeclaration c, String methodName) {
		List method = c.getMethodsByName(methodName);
		if (method == null || method.isEmpty()) {
			return false;
		}
		return true;

	}

	public static boolean isMethodAlreadyDeclared(ClassOrInterfaceDeclaration c, String bodyDec) {
		List<BodyDeclaration<?>> bodies = c.getMembers();
		for (BodyDeclaration<?> body : bodies) {
			if (bodyDec.toString().equals(body.toString())) {
				return true;
			}
		}
		return false;

	}

	public static boolean isNotDataObject(String elementType) {
		for (DataType type : DataType.values()) {
			if (type.getValue().equals(elementType)) {
				return true;
			}
		}
		return false;
	}

	public static String removeLastComma(String value) {
		if (value != null) {
			if (value.lastIndexOf(',') > 0) {
				value = value.substring(0, value.lastIndexOf(','));
			}
		}

		return value;
	}

	public static CoServiceInfo getCoServiceInfo(String className, Class clazz) {

		CoServiceInfo coServiceInfo = new CoServiceInfo();
		final ClassPool pool = ClassPool.getDefault();
		final ClassLoader contextClassLoader = clazz.getClassLoader();

		if (contextClassLoader != null) {
			pool.appendClassPath(new LoaderClassPath(contextClassLoader));
		}

		try {
			System.out.println("Class Name : " + className);
			final CtClass ctClass = pool.get(className);
			ctClass.getClassFile().getMethods();
			coServiceInfo.setServiceName(ctClass.getSimpleName());
			for (CtMethod method : ctClass.getDeclaredMethods()) {
				CoServiceMethod coServicemethod = new CoServiceMethod();
				coServiceInfo.getMethods().add(coServicemethod);
				coServicemethod.setMethodName(method.getName());
				coServiceInfo.getMethods().add(coServicemethod);
				System.out.println("Name: " + method.getName());
				CtClass[] parameters = method.getParameterTypes();
				if (null != parameters) {
					for (CtClass ct : parameters) {
						CoServiceParameter parameter = new CoServiceParameter();
						parameter.setType(ct.getSimpleName());
						parameter.setFullName(ct.getName());
						parameter.setName(RedsStringUtils.toCamelCase(ct.getSimpleName(), true));
						coServicemethod.getParameters().add(parameter);
						if (parameter.getFullName().contains(".")) {
							coServicemethod.getImports().add(parameter.getFullName());
						}
						System.out.println("Parameter Full Name : " + ct.getName());
						System.out.println("Parameter Simple Name : " + ct.getSimpleName());
						System.out.println("Parameter Package Name : " + ct.getPackageName());
					}
				}
				CtClass ct = method.getReturnType();
				if (ct != null) {
					coServicemethod.setReturnType(ct.getSimpleName());
					coServicemethod.getImports().add(ct.getName());
					if (ct.getName().contains(".")) {
						coServicemethod.getImports().add(ct.getName());
					}
					System.out.println("Return Full Name : " + ct.getName());
					System.out.println("Return Simple Name : " + ct.getSimpleName());
					System.out.println("Return Package Name : " + ct.getPackageName());
				}

			}

		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return coServiceInfo;

	}

	public static String getCacheManagerStructure(String className, String packageName, String treasury,
			String configName) {

		String template = "package <packageName>;\r\n" + "\r\n"
				+ "import com.reds.generator.redsannotations.treasury.AddToTreasury;\r\n"
				+ "import com.reds.platform.servicecommons.exception.ServiceException;\r\n"
				+ "import com.reds.service.cachemanagementapi.AbstractCacheManager;\r\n"
				+ "import com.reds.service.cachemanagementapi.Cache;\r\n"
				+ "import com.reds.service.cachemanagementapi.exception.CacheManagementException;\r\n" + "\r\n" + "\r\n"
				+ "\r\n" + "@AddToTreasury\r\n" + "public class <className> extends AbstractCacheManager {\r\n" + "\r\n"
				+ "	public void init() throws ServiceException {		\r\n" + "		\r\n"
				+ "this.config = <treasury>.open.take<configName>();\r\n"
				+ "super.initCacheManagement(<treasury>.open.accessCacheManagement());\r\n" + "	}\r\n" + "	\r\n"
				+ "	public void close() {\r\n" + "		\r\n" + "	}\r\n" + "	\r\n" + "\r\n" + "}\r\n" + "";
		ST st = new ST(template);
		st.add("className", className);
		st.add("packageName", packageName);
		st.add("treasury", treasury);
		st.add("configName", configName);
		return st.render();
	}

	public static String getClassNameFromPackage(String objectPackage) {

		return objectPackage.substring(objectPackage.lastIndexOf('.') + 1, objectPackage.length());
	}

	public static void addCoServiceAnnotation(CompilationUnit classUnit, Expression memberValuePair, String packageName,
			String serviceName, String version, boolean exposeWeb, boolean expose) {
		ImportDeclaration importDeclaration = new ImportDeclaration(CoService.class.getName(), false, false);
		if (!classUnit.getImports().contains(importDeclaration)) {
			classUnit.getImports().add(importDeclaration);
		}
		importDeclaration = new ImportDeclaration(packageName, false, false);
		if (!classUnit.getImports().contains(importDeclaration)) {
			classUnit.getImports().add(importDeclaration);
		}

		if (exposeWeb) {
			memberValuePair.asArrayInitializerExpr().getValues()
					.add(JavaParser.parseAnnotation("@CoService(name=\"" + serviceName + "\",version=\"" + version
							+ "\",service = " + serviceName + ".class,exposeAsWeb=true)"));
		} else if (expose) {
			memberValuePair.asArrayInitializerExpr().getValues().add(JavaParser.parseAnnotation("@CoService(name=\""
					+ serviceName + "\",version=\"" + version + "\",service = " + serviceName + ".class,expose=true)"));
		} else {
			memberValuePair.asArrayInitializerExpr().getValues()
					.add(JavaParser.parseAnnotation("@CoService(name=\"" + serviceName + "\",version=\"" + version
							+ "\",service = " + serviceName + ".class,exposeAsWeb=false,expose=false)"));
		}

	}

}