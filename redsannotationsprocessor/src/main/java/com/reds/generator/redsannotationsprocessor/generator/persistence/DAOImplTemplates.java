package com.reds.generator.redsannotationsprocessor.generator.persistence;

public class DAOImplTemplates {
	
	
	public static String PlatformDAOImplPkg="import com.reds.service.datamanagementapi.persist.PlatformDAOImpl;";
	public static String PlatformJdbcDAOImplPkg="import com.reds.service.datamanagementapi.persist.PlatformJdbcDAOImpl;";
	public static String DataManagementExceptionPkg="import com.reds.service.datamanagementapi.exception.DataManagementException;";
	public static String DataManagementErrorCodesPkg="import com.reds.service.datamanagementapi.exception.DataManagementErrorCodes;";

	
	
	public DAOImplTemplates() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * toName
	 * queryName
	 * other
	 * @return
	 */
	public String getQueryTemplate() {
		String template="public List\\<<toName>> <queryName>(<toName> <other>) throws DataManagementException;";
		return template;
	}
	/**
	 * toName
	 * queryName
	 * other
	 * @return
	 */
	public String getQueryUniqueTemplate() {
		String template="public <toName> <queryName>(<toName> <other>) throws DataManagementException;";
		return template;
	}
	/**
	 * toName
	 * doName
	 * filter
	 * declareParam
	 * setParam
	 * queryName
	 * other
	 * 
	 * @return
	 */
	public String getQueryUniqueImplTemplate() {
		String template="public <toName> <queryName>(<toName> <other>) throws DataManagementException {\r\n" + 
				"		" + 
				"		Transaction tx = getTransaction();\r\n" + 
				"		PersistenceManager pm = null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n" + 
				"			pm = getPersistenceManager();\r\n" + 
				"			Query\\<<doName>> query = pm.newQuery(<doName>.class);\r\n" + 
				"			query.filter(\"<filter>\");\r\n" + 
				"			query.declareParameters(\"<declareParam>\");\r\n" + 
				"			query.setParameters(<setParam>);\r\n" + 
				"			List\\<<doName>> voList = (List\\<<doName>>) query.executeList();\r\n" + 
				"			if (voList != null && voList.size()>0) {\r\n" + 
				"				" + 
				"					<other>=this.mapper.to<toName>(pm.detachCopy(voList.get(0)));\r\n" + 
				"				" + 
				"			}\r\n" + 
				"			tx.commit();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to <queryName>\"+<other>, e);\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return <other>;\r\n" + 
				"	}";
		return template;
	}
	/**
	 * toName
	 * doName
	 * filter
	 * declareParam
	 * setParam
	 * queryName
	 * other
	 * 
	 * @return
	 */
	public String getQueryImplTemplate() {
		String template="public List\\<<toName>> <queryName>(<toName> <other>) throws DataManagementException {\r\n" + 
				"		List\\<<toName>> tos = new ArrayList\\<>();\r\n" + 
				"		Transaction tx = getTransaction();\r\n" + 
				"		PersistenceManager pm = null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n" + 
				"			pm = getPersistenceManager();\r\n" + 
				"			Query\\<<doName>> query = pm.newQuery(<doName>.class);\r\n" + 
				"			query.filter(\"<filter>\");\r\n" + 
				"			query.declareParameters(\"<declareParam>\");\r\n" + 
				"			query.setParameters(<setParam>);\r\n" + 
				"			List\\<<doName>> voList = (List\\<<doName>>) query.executeList();\r\n" + 
				"			if (voList != null) {\r\n" + 
				"				for (<doName> temp : voList) {\r\n" + 
				"					tos.add(this.mapper.to<toName>(pm.detachCopy(temp)));\r\n" + 
				"				}\r\n" + 
				"			}\r\n" + 
				"			tx.commit();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to <queryName>\"+<other>, e);\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return tos;\r\n" + 
				"	}";
		return template;
	}
	
	/**
	 * doName
	 * primaryKeyType
	 * 
	 * @return
	 */
	public String getDOTemplate() {
		String template="private <doName> get<doName>(<primaryKeyType> id, PersistenceManager pm) {\r\n" + 
				"		<doName> tempDO = null;\r\n" + 
				"		Query\\<<doName>> query = getQueryById(id, pm);\r\n" + 
				"		List\\<<doName>> voList = (List\\<<doName>>) query.executeList();\r\n" + 
				"		if (voList != null && voList.size() > 0) {\r\n" + 
				"			tempDO = voList.get(0);\r\n" + 
				"			if (tempDO != null) {\r\n" + 
				"				tempDO = pm.detachCopy(tempDO);\r\n" + 
				"			}\r\n" + 
				"		}\r\n" + 
				"		return tempDO;\r\n" + 
				"	}\r\n" + 
				"";
		
		return template;
	}
	
	/**
	 * doName
	 * 
	 * primaryKeyVar
	 * 
	 * @return
	 */
	public String getQueryById() {
		String template="private Query\\<<doName>> getQueryById(<primaryKeyType> <primaryKeyVar>, PersistenceManager pm) {\r\n" + 
				"		Query\\<<doName>> query = pm.newQuery(<doName>.class);\r\n" + 
				"		query.filter(\"this.<primaryKeyVar> ==  <primaryKeyVar>\");\r\n" + 
				"		query.declareParameters(\"<primaryKeyType> <primaryKeyVar>\");\r\n" + 
				"		query.setParameters(<primaryKeyVar>);\r\n" + 
				"		return query;\r\n" + 
				"	}";
		
		return template;
	}
	
	public String getDoId() {
		String template="public  String getDoId() {\r\n" + 
				"    	return String.valueOf(this.<primaryKeyVar>);\r\n" + 
				"    }";
		
		return template;
	}
	
	/**
	 * toName
	 * doName
	 * primaryKey
	 * @return
	 */
	public String insertTemplate() {
			
		String templateList="public void insert(List\\<<toName>> tos to, Connection connection) throws DataManagementException {\r\n" + 
				"		try (PreparedStatement stmt$ = connection.prepareStatement(INSERT_QUERY)) {\r\n" + 
				"	        for (<toName> to : tos) {"
				+"              setStatement(to, stmt$); \r\n"
				+"              stmt$.addBatch();"
				+"			    <tracker>.me.debug(\"Adding to batch {}  \", stmt$.toString());\r\n\" + "
				+ "         }"+
				"			stmt$.execute();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(1),\r\n" + 
				"					\"Faile to insert \" + super.finalTableName, e);\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"	} \n\n";
		String template="public void insert(<toName> to, Connection connection) throws DataManagementException {\r\n" + 
				"		try (PreparedStatement stmt$ = connection.prepareStatement(INSERT_QUERY)) {\r\n" + 
				"			setStatement(to, stmt$); \r\n" + 
				"			<tracker>.me.debug(\"Persisting {}  \", stmt$.toString());\r\n" + 
				"			stmt$.execute();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(1),\r\n" + 
				"					\"Faile to insert \" + super.finalTableName, e);\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"	}\n\n";
		
		String templateSet="private void setStatement(<toName> to, PreparedStatement stmt$) throws SQLException {"
				+ "   <setstatement>\r\n        "
				+ "}\n\n";
		
		return templateList + template + templateSet;
	}
	
	public String insertQueryTemplate() {
		String template="private void setInsertQuery() {\r\n" + 
				"		super.INSERT_QUERY=\"<insertQuery>\"\r\n" + 
				"		\r\n" + 
				"	}";
		
		return template;
	}
	
	
	
	/**
	 * toName
	 * doName
	 * primaryKey
	 * @return
	 */
	public String persistTemplate() {
		String template="@Override\r\n" + 
				"	public <toName> persist(<toName> to) throws DataManagementException {\r\n" + 
				"		<doName> dataObject = mapper.to<doName>(to);\r\n" + 
				"		Transaction tx = getTransaction();\r\n"
				+ "     PersistenceManager pm =null;\r\n" + 
				"		try {\r\n "
				+ "			RedsValidator.isValid(to);" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n"
				+ "			pm =getPersistenceManager();\r\n" + 
				"			<doName> tempDO = get<doName>(to.get<primaryKey>(), pm);\r\n" + 
				"			if (null != tempDO) {\r\n" + 
				"				dataObject = tempDO;\r\n" +
				"			mapper.update<doName>From<toName>(to, dataObject);\r\n" + 
				"			}" + 				
				"			dataObject = pm.makePersistent(dataObject);\r\n" + 
				"			tx.commit();\r\n" + 
				"			this.mapper.update<toName>From<doName>(dataObject, to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(1), \"Failed to execute persist\", e);\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return to;" + 
				"	}";
		
		return template;
	}
	
	/**
	 * toName
	 * doName
	 * primaryKey
	 * @return
	 */
	public String persistWithOutTransactionTemplate() {
		String template="@Override\r\n" + 
				"	public <toName> persistWithOutTxn(<toName> to) throws DataManagementException {\r\n" + 
				"		<doName> dataObject = mapper.to<doName>(to);\r\n" + 
				"     PersistenceManager pm =null;\r\n" + 
				"		try {\r\n "
				+ "         RedsValidator.isValid(to);" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n"
				+ "			pm =getPersistenceManager();\r\n" + 
				"			<doName> tempDO = get<doName>(to.get<primaryKey>(), pm);\r\n" + 
				"			if (null != tempDO) {\r\n" + 
				"				dataObject = tempDO;\r\n" +
				"			mapper.update<doName>From<toName>(to, dataObject);\r\n" + 
				"			}" + 				
				"			dataObject = pm.makePersistent(dataObject);\r\n" + 
				"			this.mapper.update<toName>From<doName>(dataObject, to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(1), \"Failed to execute persist\", e);\r\n" + 
				"		} "+ 
				"		return to;" + 
				"	}";
		
		return template;
	}
	
	/**
	 * toName
	 * doName
	 * 
	 * @return
	 */
	public String retrieveAllTemplate() {
		String template="@Override\r\n" + 
				"	public List\\<<toName>> retrieveAll() throws DataManagementException {\r\n" + 
				"		List\\<<toName>> toList = new ArrayList\\<>();\r\n" + 
				"		Transaction tx = getTransaction();\r\n"  
				+ "     PersistenceManager pm =null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n"  
				+ "			pm =getPersistenceManager();\r\n" + 
				"			Extent\\<<doName>> doExtend = pm.getExtent(<doName>.class, true);\r\n" + 
				"			tx.commit();\r\n" + 
				"			Iterator\\<<doName>> iterator = doExtend.iterator();\r\n" + 
				"			iterator.forEachRemaining(eachDo -> {\r\n" + 
				"				toList.add(this.mapper.to<toName>(eachDo));\r\n" + 
				"			});\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to retrieveAll\", e);\r\n" + 
				"\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return toList;" + 
				"	}";
				return template;
	}
	
	/**
	 * toName
	 * doName
	 * primaryKey
	 * @return
	 */
	public String retrieveTemplate() {
		String template="@Override\r\n" + 
				"	public <toName> retrieve(<toName> to) throws DataManagementException {\r\n" + 
				"Transaction tx = getTransaction();\r\n" 
				+"PersistenceManager pm =null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n" 
				+ "			pm =getPersistenceManager();\r\n" + 
				"			Query\\<<doName>> query =getQueryById(to.get<primaryKey>(), pm);" + 
				"			List\\<<doName>> voList = (List\\<<doName>>) query.executeList();\r\n" + 
				"			if (voList != null && voList.size() > 0) {\r\n" + 
				"				<doName> vo = voList.get(0);\r\n" + 
				"				to = this.mapper.to<toName>(vo);\r\n" + 
				"			}\r\n" + 
				"			tx.commit();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to retrieve\", e);\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return to;" + 
				"	}";
		return template;				
		
	}
	
	/**
	 * toName
	 * doName
	 * primaryKey
	 * @return
	 */
	public String deleteTemplate() {
		String template="	@Override\r\n" + 
				"	public <toName> delete(<toName> to) throws DataManagementException {\r\n" + 
				"	Transaction tx = getTransaction();\r\n"  
				+ " PersistenceManager pm =null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();		    		\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n"  
				+ "			pm =getPersistenceManager();\r\n" + 
				"			Query\\<<doName>> query =getQueryById(to.get<primaryKey>(), pm);" + 
				"		    query.deletePersistentAll();\r\n" + 
				"			tx.commit();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to delete \", e);\r\n" + 
				"		} finally {\r\n" + 
				"			 rollBack(tx);\r\n" + 
				"	         close(pm);\r\n" + 
				"		}\r\n" + 
				"		return to;" + 
				"}";
		
		return template;
	}
	
	public String deleteAllTemplate() {
		String template="@Override\r\n" + 
				"	public boolean deleteAll() throws DataManagementException {\r\n" + 
				"	Transaction tx = getTransaction();\r\n"  
				+ " PersistenceManager pm =null;\r\n" + 
				"		try {\r\n" + 
				"			tx.begin();\r\n" + 
				"			setFetchGroup(\"Fetch_<doName>\");\r\n" 
				+ "			pm =getPersistenceManager();\r\n" + 
				"			Query\\<<doName>> query = pm.newQuery(<doName>.class);\r\n" + 
				"			query.deletePersistentAll();\r\n" + 
				"			tx.commit();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new DataManagementException(DataManagementErrorCodes.errorCode(2), \"Failed to delete all \", e);\r\n" + 
				"\r\n" + 
				"		} finally {\r\n" + 
				"			rollBack(tx);\r\n" + 
				"			close(pm);\r\n" + 
				"		}\r\n" + 
				"		return true;" + 
				"	}";
		
		return template;
	}

	public String queryImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic List\\<<toName>> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).<functionName>(to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to <functionName>\"+ to, e);\r\n" + 
				"		}"
				+ "\n }";
		return template;
	}
	
	public String queryTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - list of  <toName>s with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic List\\<<toName>> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}

	public String queryUniqueTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance  of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}
	
	public String queryUniqueImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).<functionName>(to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to <functionName>\"+ to, e);\r\n" + 
				"		}"
				+ "\n }";
		return template;
	}

	public String saveToTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}
	
	
	public String saveToImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).persist(to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to persist\"+ to, e);\r\n" + 
				"		}"
				+ "\n }";
		return template;
	}
	
	public String updateToTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName> \r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}
	
	public String updateToImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n return <saveFunctionName>(to);"
				+ "\n\n}";
		return template;
	}
	
	public String retrieveToTemplate() {		
		String template="/**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName> \r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}
	
	public String retrieveToImplTemplate() {		
		String template="/**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName> \r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return to - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).retrieve(to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to retrieve\"+ to, e);\r\n" + 
				"		}"
				+ "\n\n}";
		return template;
	}
	
	public String retrieveAllToTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return list of to's - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic List\\<<toName>> <functionName>() throws <serviceName>Exception;";
		return template;
	}
	
	public String retrieveAllToImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return list of to's - instance of <toName> with id's enriched.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic List\\<<toName>> <functionName>() throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).retrieveAll();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to retrieveAll\", e);\r\n" + 
				"		}"
				+ "\n\n}";
		return template;
	}
	
	public String deleteAllToTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName> \r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return true - when  success . else false \r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic boolean <functionName>() throws <serviceName>Exception;";
		return template;
	}
	
	public String deleteAllToImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return true - when  success . else false \r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic boolean <functionName>() throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).deleteAll();\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to deleteAll\", e);\r\n" + 
				"		}"
				+ "\n\n}";
		return template;
	}
	
	public String deleteToTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return <toName> - deleted to \r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception;";
		return template;
	}
	
	public String deleteToImplTemplate() {		
		String template=" /**\r\n" + 
				"     * \\<b>Purpose:\\</b> To <functionName>\r\n" + 
				"     * @param to - instance of <toName>\r\n" + 
				"     * @return <toName> - deleted to.\r\n" + 
				"     * @throws <serviceName>Exception\r\n" + 
				"     */\n\npublic <toName> <functionName>(<toName> to) throws <serviceName>Exception{"
				+ "\n  try {\r\n" + 
				"			return new <daoName>(connection).delete(to);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			throw new <serviceName>Exception(<serviceName>ErrorCodes.errorCode(9), \"Failed to delete\"+ to, e);\r\n" + 
				"		}"
				+ "\n\n}";
		return template;
	}
	

	


	
		

}
