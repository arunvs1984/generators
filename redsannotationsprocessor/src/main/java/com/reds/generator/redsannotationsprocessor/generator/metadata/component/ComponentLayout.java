package com.reds.generator.redsannotationsprocessor.generator.metadata.component;

import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public class ComponentLayout extends AbstractProjectLayout {
	public ComponentLayout(String rootPackage) {
		setRootPackage(rootPackage);
	}

}
