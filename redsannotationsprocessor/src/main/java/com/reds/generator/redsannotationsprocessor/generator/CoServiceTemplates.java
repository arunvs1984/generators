package com.reds.generator.redsannotationsprocessor.generator;

public class CoServiceTemplates {
	
	/**
	 * serviceApi
	 * argumentsWithType
	 * @return
	 */
	public String getExposeWebTemplateInterface() {
		 String template="public Response <serviceApi>(<argumentsWithType>);";		
		return template;
				
		
	}
	
	/**
	 * serviceApi
	 * argumentsWithType
	 * returnType
	 * exception
	 * @return
	 */
	public String getExposeTemplateInterface() {
		 String template="public <returnType> <serviceApi>(<argumentsWithType>) throws <exception>;";		
		return template;
				
		
	}
	
	/**
	 * returnType
	 * serviceApi
	 * argumentsWithType
	 * exception
	 * exceptionCode
	 * errorNumber
	 * treasuaryName
	 * return
	 * 
	 * @return
	 */
	public String getExposeTemplate() {
		String template=" public <returnType> <serviceApi>(<argumentsWithType>) throws <exception> {\r\n" + 
				"        try {\r\n" + 
				"            <return> <treasuaryName>.open.access<serviceName>().<serviceApi>(<arguments>);\r\n" + 
				"        } catch (Exception e) {\r\n" + 
				"            throw new <exception>(<exceptionCode>.errorCode(<errorNumber>), \"Failed to <serviceApi>\", e);\r\n" + 
				"        }\r\n" + 
				"    }";
		return template;
	}
	/**
	 * serviceApi
	 * argumentsWithType
	 * arguments
	 * serviceName
	 * treasuaryName
	 * returnValue
	 * returnResult
	 *  tracker
	 * @return
	 */
	public String getExposeWebTemplate() {
		String template="<getOrpost>\r\n" + 
				"	@Path(\"/<serviceApi>\")\r\n" + 
				"	@Produces(MediaType.APPLICATION_JSON)\r\n" + 
				"	@Consumes(MediaType.APPLICATION_JSON)\r\n"+
				"   @Operation(summary = \"<serviceApiSeparated>\", "
				+ "            description = \"To <serviceApiSeparated>\", "
				+ "            responses = { @ApiResponse(responseCode = \"200\", description = \"<serviceApiSeparated> Success\"),"
				+ "                          @ApiResponse(responseCode = \"500\", description = \"Internal Server Error\") })" + 
				"	public Response <serviceApi>(<argumentsWithType>) {\r\n" + 
				"		try {\r\n"
				+ "          super.onRequestEntry(\"<serviceApi>\", <argumentsEntry>);\r\n" + 
				"		<returnValue> <treasuaryName>.open.access<serviceName>().<serviceApi>(<arguments>);\r\n"
				+ "          super.onResponseExit(\"<serviceApi>\", result);\r\n" + 
				"			return getSuccessResponse(result);\r\n" + 
				"		} catch (Exception e) {\r\n" + 
				"			<tracker>.me.error(\"Failed to <serviceApi>\", e);\r\n" + 
				"			return getFailureResponse(e, Status.INTERNAL_SERVER_ERROR);\r\n" + 
				"		}\r\n" + 
				"\r\n" + 
				"	}";
		
		return template;
	}

}
