package com.reds.generator.redsannotationsprocessor.generator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.lang.model.element.Modifier;
import javax.tools.Diagnostic;

import com.reds.generator.redsannotations.resilience.ResilienceAction;
import com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider.ServiceProviderMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider.ServiceProviderMetaDataWrapper;
import com.reds.generator.redsannotationsprocessor.tracker.RedsannotationsprocessorTrack;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.platform.servicecommons.AbstractServiceProvider;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.MethodSpec.Builder;
import com.squareup.javapoet.TypeSpec;

import dagger.Module;
import dagger.Provides;

public class ServiceProviderGenerator extends AbstractRedsGenerator<ServiceProviderMetaData> {

	private Messager messager;
	private Filer filer;

	public ServiceProviderGenerator(Messager messager, Filer filer) {
		super();
		this.messager = messager;
		this.filer = filer;
	}

	private void log(String message) {
		messager.printMessage(Diagnostic.Kind.NOTE, message);
	}

	@Override
	public void generate(ServiceProviderMetaData metaData) {
		try {
			ServiceProviderMetaDataWrapper wrapper = new ServiceProviderMetaDataWrapper(metaData);
			String className = RedsStringUtils
					.capitalize(metaData.getName() + RedsGeneratorConstants.GENERATED_SERVICE_PROVIDER_POSTFIX);
			wrapper.setActualClassName(className);
			MethodSpec constructor = getConstructor(wrapper);
			List<MethodSpec> methods = getMethods(wrapper);
			methods.add(constructor);
			com.squareup.javapoet.TypeSpec.Builder typeBuilder = TypeSpec.classBuilder(className)
					.addJavadoc(getJavaDoc(className, metaData.getAuthor()))
					.superclass(AbstractServiceProvider.class)
					.addAnnotation(Module.class)
					.addModifiers(Modifier.PUBLIC, Modifier.FINAL);
			for (MethodSpec methodSpec : methods) {
				typeBuilder.addMethod(methodSpec);
			}
			TypeSpec typeSpec = typeBuilder.build();

			JavaFile javaFile = JavaFile
					.builder(wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName(), typeSpec).build();
			javaFile.writeTo(System.out);
			javaFile.writeTo(filer);
			log("Generated " + className);
		} catch (IOException e) {
			RedsannotationsprocessorTrack.me.error("Failed to generate config manager", e);
		}
	}

	private String getJavaDoc(String serviceName, String author) {
		String javaDoc = "<b>Purpose: </b> Service Provider class for " + serviceName + "\n\n @author <b>" + author
				+ "</b>" + "\n\n @since " ;//+ RedsDateUtils.getCurrenTimeStamp();
		return javaDoc;
	}

	private MethodSpec getConstructor(ServiceProviderMetaDataWrapper wrapper) {
		Builder builder = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);
		String treasuryName = wrapper.getTreasuryName();
		ClassName treasury = ClassName.get(wrapper.getTreasuryPackage(), treasuryName);	
		builder.addStatement("super($T.serviceContext)", treasury);		
		return builder.build();

	}

	

	private List<MethodSpec> getMethods(ServiceProviderMetaDataWrapper wrapper) {
		List<MethodSpec> methodSpecs = new ArrayList<>();
		for (com.reds.redsgenerator.api.services.CoServiceMetaData serviceMetaData : wrapper.getMetaData().getServices()) {
			String serviceVersion=serviceMetaData.getVersion();
			String serviceName = RedsStringUtils.capitalize(serviceMetaData.getName());			
			
			String coService=serviceMetaData.getCoServiceClass();
			ResilienceAction action=serviceMetaData.getResilienceAction();
			String coServicePackageName=serviceMetaData.getCoServicePackage();
			String simpleClassName=RedsStringUtils.getSimpleName(coService);			
			ClassName coServiceClassName = ClassName.get(coServicePackageName, simpleClassName);
			String version=serviceVersion.replaceAll("\\.", "_");
			Builder provideMethod = MethodSpec.methodBuilder("provide" + serviceName+"_"+version)
					.addJavadoc("<b>Purpose: </b> To provide " + serviceName+serviceVersion)
					.addModifiers(Modifier.PUBLIC)
					.addAnnotation(Provides.class)
					.returns(coServiceClassName);
			
			provideMethod.addCode(getProvideCode(serviceName,serviceVersion),
					coServiceClassName);
			methodSpecs.add(provideMethod.build());
		}
		
		return methodSpecs;
	}

	private String getProvideCode(String name,String version) {
		String code = "return ($T) super.getServiceContext().getService(\""+name+"\", \""+version+"\");\n";
		return code;
	}

}
