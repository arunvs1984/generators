package com.reds.generator.redsannotationsprocessor.generator;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.to.PlatformTO;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import com.reds.platform.redsplatformcommons.service.ServiceType;

public class ServiceMetaData implements PlatformTO {

	private ServiceType type;
	private String name;
	private String version;
	private String implementation;
	

	public ServiceType getType() {
		return type;
	}

	public void setType(ServiceType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String getId() {
		
		return new PlatformLayout().getServicMetaDataName();
	}

	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}

	public static void main(String[] args) throws RedsXmlException {
		RedsXmlManagerImpl<ServiceMetaData> xml = new RedsXmlManagerImpl<>("./");
		ServiceMetaData metaData = new ServiceMetaData();
		metaData.setName("redsdateprovider");
		metaData.setVersion("1.0");
		metaData.setType(ServiceType.PLATFORM);
		metaData.setImplementation("");
		xml.write(metaData);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(">").append(type).append(":").append(name).append(":").append(version).append("\n");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((implementation == null) ? 0 : implementation.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceMetaData other = (ServiceMetaData) obj;
		if (implementation == null) {
			if (other.implementation != null)
				return false;
		} else if (!implementation.equals(other.implementation))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

}
