package com.reds.generator.redsannotationsprocessor.generator.code;

public class DependencyMetaData {
	private String location;
	private String packageName;
	private String version;
	private String name;
	private String dependency;
	private boolean exposeAsWeb=false;
	private boolean expose=false;
	private String group;
	private boolean sameProject;
	private String apiName;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDependency() {
		return dependency;
	}
	public boolean isExposeAsWeb() {
		return exposeAsWeb;
	}
	public void setExposeAsWeb(boolean exposeAsWeb) {
		this.exposeAsWeb = exposeAsWeb;
	}
	public boolean isExpose() {
		return expose;
	}
	public void setExpose(boolean expose) {
		this.expose = expose;
	}
	public void setDependency(String dependency) {
		this.dependency = dependency;
	}
	@Override
	public String toString() {
		return "DependencyMetaData [location=" + location + ", packageName=" + packageName + ", version=" + version
				+ ", name=" + name + ", dependency=" + dependency + ", exposeAsWeb=" + exposeAsWeb + ", expose="
				+ expose + "]";
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public boolean isSameProject() {
		return sameProject;
	}
	public void setSameProject(boolean sameProject) {
		this.sameProject = sameProject;
	}
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	

}
