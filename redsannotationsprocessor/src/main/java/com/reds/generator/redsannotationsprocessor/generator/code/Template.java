package com.reds.generator.redsannotationsprocessor.generator.code;

public class Template {
	
	private String configuration="package <packageName>;\r\n" + 
			"\r\n" + 
			"import com.reds.generator.redsannotations.Configuration; \r\n" + 
			"import com.reds.library.redscommons.config.PlatformConfig; \r\n" + 
			"\r\n" + 
			"/**\r\n" + 
			" * \\<b>Purpose:\\</b> <desc>\r\n" + 
			" *\r\n" + 
			" * @author \\<b><author>\\</b>\r\n" + 
			" *\r\n" + 
			" * \r\n" + 
			" */\r\n" + 
			"@Configuration\r\n" + 
			"public class <className> implements PlatformConfig {\r\n" + 
			"\r\n" + 
			"	/**\r\n" + 
			"	 * \r\n" + 
			"	 */\r\n" + 
			"	private static final long serialVersionUID = 1L;\r\n" + 
			"\r\n" + 
			"	/**\r\n" + 
			"	 * Holds the name of the configurations\r\n" + 
			"	 */\r\n" + 
			"	private String id=\"<id>\";	\r\n" + 
			"	/**\r\n" + 
			"	 * Holds the description of the configurations\r\n" + 
			"	 */\r\n" + 
			"	private String description=\"<desc>\";\r\n" + 
			"\r\n" + 
			"	/**\r\n" + 
			"	 * To get Unique Id for this <className>\r\n" + 
			"	 */\r\n" + 
			"	@Override\r\n" + 
			"	public String getId() {\r\n" + 
			"		return id;\r\n" + 
			"	}\r\n" + 
			"\r\n" + 
			"	/**\r\n" + 
			"	 * To get description of <className>\r\n" + 
			"	 */\r\n" + 
			"	@Override\r\n" + 
			"	public String getDescription(){\r\n" + 
			"	 return description;	\r\n" + 
			"	}\r\n" + 
			"\r\n" + 
			"	@Override\r\n" + 
			"	public PlatformConfig getDefault() {\r\n" + 
			"		return this;\r\n" + 
			"	}\r\n" + 
			"\r\n" + 
			"}";

	public String getConfiguration() {
		return configuration;
	}

}
