package com.reds.generator.redsannotationsprocessor;

public interface AnnotationNames {
	public static final String SERVICE="com.reds.generator.redsannotations.Service";
	public static final String CONFIG="com.reds.generator.redsannotations.Configuration";
	public static final String REQUIRED="com.reds.generator.redsannotations.Required";

}
