package com.reds.generator.redsannotationsprocessor.generator.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorApiUtils;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.persistence.FieldMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.core.RedsGeneratorUtils;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class MapperGenerator {

	private boolean mapperChange = false;

	public void generateMapper(PersistenceCapableMetaData persistenceMetaData, ServiceMetaDataWrapper wrapper,
			String persistencePath) {
		try {
			CompilationUnit componentClassUnit = getTODAOMapperCompilationUnit(persistenceMetaData, persistencePath);

			componentClassUnit.getImports()
					.add(new ImportDeclaration(persistenceMetaData.getQualifiedClassName(), false, false));

			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
				
				StringBuilder usesClasses = new StringBuilder();

				if (!c.getJavadoc().isPresent()) {
					c.setJavadocComment(
							APUtils.getClassJavaDoc(
									wrapper.getMetaData().getAuthor(), "Mapper class for "
											+ persistenceMetaData.getSimpleName() + ".<br>\nThis class Map TO to DO.",
									RedsDateUtils.getCurrenTimeStamp()));
				}

				/* To TO */
				String methodName = "to" + persistenceMetaData.getSimpleName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public <toName> " + methodName + "(<doName> dataObject);";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Convert Data Object to Transfer Object");
					c.addMember(declaration);
					mapperChange = true;
				}

				/* To TO */
				methodName = "to" + persistenceMetaData.getDoName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public <doName> " + methodName + "(<toName> transferObject);";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Convert Transfer Object to Data Object");
					c.addMember(declaration);
					mapperChange = true;
				}

				/* update DO from TO */
				methodName = "update" + persistenceMetaData.getDoName() + "From" + persistenceMetaData.getSimpleName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public void " + methodName
							+ "(<toName> transferObject,@MappingTarget <doName> dataObject);";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Update Data Object From Transfer Object");
					ST mappingsAnnoatation = new ST("@Mappings({<mappings>})");
					StringBuilder mappings = new StringBuilder();
					for (FieldMetaData field : persistenceMetaData.getFields()) {
						String capitalizeFieldName=RedsStringUtils.capitalize(field.getFieldName());	
						if (field.isList()) {
							
							System.out.println("Mapper -> "+convertLowerCaseFirstCharacter(field.getToName())+RedsGeneratorConstants.MAPPER_POSTFIX);		
							mappings.append("@Mapping(target = \""+field.getFieldName()+"\", expression = \"java(update"+field.getToName()+"(transferObject.get"+capitalizeFieldName+"(),dataObject.get"+capitalizeFieldName+"(),"+ convertLowerCaseFirstCharacter(field.getToName())+RedsGeneratorConstants.MAPPER_POSTFIX+"))\"),");
											
														
							ST fieldSt = new ST(new MapperTemplates().listMappingTemplate());
							fieldSt.add("to", field.getToName());							
							
							fieldSt.add("dataObject", field.getToName()+RedsGeneratorConstants.DATAOBJECT_POSTFIX);
							System.out.println(fieldSt.render());
							BodyDeclaration<?> fieldBodyDeclaration = JavaParser.parseBodyDeclaration(fieldSt.render());
							fieldBodyDeclaration.setBlockComment("Update Data Objects From Transfer Objects");
							c.addMember(fieldBodyDeclaration);
							
							
							ImportDeclaration mappingImport = new ImportDeclaration(org.mapstruct.Mapping.class.getName(), false, false);
							if (!componentClassUnit.getImports().contains(mappingImport)) {
								componentClassUnit.getImports().add(mappingImport);
							}
							
							ImportDeclaration mappingTargetImport = new ImportDeclaration(org.mapstruct.MappingTarget.class.getName(), false, false);
							if (!componentClassUnit.getImports().contains(mappingTargetImport)) {
								componentClassUnit.getImports().add(mappingTargetImport);
							}
							
							ImportDeclaration importTos = new ImportDeclaration(field.getToImportString(), false, false);
							if (!componentClassUnit.getImports().contains(importTos)) {
								componentClassUnit.getImports().add(importTos);
							}
							
							ImportDeclaration list = new ImportDeclaration(List.class.getName(), false, false);
							if (!componentClassUnit.getImports().contains(list)) {
								componentClassUnit.getImports().add(list);
							}
							
							ImportDeclaration arrayList = new ImportDeclaration(ArrayList.class.getName(), false, false);
							if (!componentClassUnit.getImports().contains(arrayList)) {
								componentClassUnit.getImports().add(arrayList);
							}
							
							usesClasses.append(field.getToName()+RedsGeneratorConstants.MAPPER_POSTFIX+".class,");
						}
						
						if(field.isDataObject()) {
							usesClasses.append(field.getToName()+RedsGeneratorConstants.MAPPER_POSTFIX+".class,");
						}
					}
					
					
					mappingsAnnoatation.add("mappings", mappings.toString());
					System.out.println(mappingsAnnoatation.render());
					declaration.addAnnotation(JavaParser.parseAnnotation(mappingsAnnoatation.render()));
					c.addMember(declaration);
					mapperChange = true;
				}
				
				ImportDeclaration mapperImport = new ImportDeclaration(org.mapstruct.Mapper.class.getName(), false, false);
				if (!componentClassUnit.getImports().contains(mapperImport)) {
					componentClassUnit.getImports().add(mapperImport);
				}
				
				ImportDeclaration mappingsImports = new ImportDeclaration(org.mapstruct.Mappings.class.getName(), false, false);
				if (!componentClassUnit.getImports().contains(mappingsImports)) {
					componentClassUnit.getImports().add(mappingsImports);
				}

				/* update TO from DO */
				methodName = "update" + persistenceMetaData.getSimpleName() + "From" + persistenceMetaData.getDoName();

				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public void " + methodName
							+ "(<doName> dataObject,@MappingTarget <toName> transferObject);";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Update Transfer Object From Data Object");
					

					c.addMember(declaration);
					mapperChange = true;
				}
				if (!c.isAnnotationPresent(Mapper.class)) {
					mapperChange = true;
					ST mapperTemplate = new ST("@Mapper(uses = { <usesClasses> })");
					String uses=usesClasses.toString();
					if(uses.indexOf(',')>0) {
						uses=uses.substring(0,uses.lastIndexOf(','));
					}
					mapperTemplate.add("usesClasses", uses);
					c.addAnnotation(JavaParser.parseAnnotation(mapperTemplate.render()));
				}

			});

			if (mapperChange) {
				System.out.println("Change in Mapper");
				String sourcePath = getMapperSourcePath(persistenceMetaData, persistencePath);
				String newSource = componentClassUnit.toString();
				RedsFileUtils.write(sourcePath, newSource);
				// TODO Create Dummy Mapper Implementation
				createDummyMapperImpl(persistenceMetaData, persistencePath);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static String convertLowerCaseFirstCharacter(String string) {
	    return string == null || string.isEmpty() ? "" : Character.toLowerCase(string.charAt(0)) + string.substring(1);
	}

	private void createDummyMapperImpl(PersistenceCapableMetaData persistenceMetaData, String persistencePath) {
		try {
			String implPersistencePath = persistencePath.replaceFirst("main", "generated");
			CompilationUnit componentClassUnit = getTODAOMapperImplCompilationUnit(persistenceMetaData,
					implPersistencePath);
			componentClassUnit.getImports()
					.add(new ImportDeclaration(persistenceMetaData.getQualifiedClassName(), false, false));
			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
				/* To TO */
				String methodName = "to" + persistenceMetaData.getSimpleName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public <toName> " + methodName + "(<doName> dataObject){return null;}";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Convert Data Object to Transfer Object");
					c.addMember(declaration);
				}
				/* To TO */
				methodName = "to" + persistenceMetaData.getDoName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public <doName> " + methodName + "(<toName> transferObject){return null;}";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Convert Transfer Object to Data Object");
					c.addMember(declaration);
				}
				/* update DO from TO */
				methodName = "update" + persistenceMetaData.getDoName() + "From" + persistenceMetaData.getSimpleName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public void " + methodName
							+ "(<toName> transferObject,@MappingTarget <doName> dataObject){}";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Update Data Object From Transfer Object");
					c.addMember(declaration);
				}
				/* update TO from DO */
				methodName = "update" + persistenceMetaData.getSimpleName() + "From" + persistenceMetaData.getDoName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = "public void " + methodName
							+ "(<doName> dataObject,@MappingTarget <toName> transferObject){}";
					ST st = new ST(template);
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("doName", persistenceMetaData.getDoName());
					BodyDeclaration<?> declaration = JavaParser.parseBodyDeclaration(st.render());
					declaration.setBlockComment("Update Transfer Object From Data Object");
					c.addMember(declaration);
				}

			});
			String sourcePath = getMapperImplSourcePath(persistenceMetaData, implPersistencePath);
			String newSource = componentClassUnit.toString();
			RedsFileUtils.write(sourcePath, newSource);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private CompilationUnit getTODAOMapperImplCompilationUnit(PersistenceCapableMetaData metaData,
			String persistencePath) throws FileNotFoundException, RedsutilsException {
		String sourcePath = getMapperImplSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			/* Mapper Package and DO Package expected to be same */
			String packageName = metaData.getDoPackage();
			String[] imports = { "import " + MappingTarget.class.getName() + ";" };
			RedsFileUtils.write(sourcePath, getToDoMapperImplStructure(metaData.getMapperName(), packageName, imports));
		}
		return JavaParser.parse(componentSourceFile);
	}

	private CompilationUnit getTODAOMapperCompilationUnit(PersistenceCapableMetaData metaData, String persistencePath)
			throws FileNotFoundException, RedsutilsException {
		String sourcePath = getMapperSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			/* Mapper Package and DO Package expected to be same */
			String packageName = metaData.getDoPackage();
			String[] imports = { "import " + MappingTarget.class.getName() + ";" };
			RedsFileUtils.write(sourcePath, getToDoMapperStructure(metaData.getMapperName(), packageName, imports));
		}
		return JavaParser.parse(componentSourceFile);
	}

	private String getToDoMapperStructure(String className, String packageName, String... imports) {

		String template = "package <packageName>;\n<imports>" + "public interface <className> {}";
		ST st = new ST(template);
		st.add("className", className);
		st.add("packageName", packageName);
		StringBuilder importBuilder = new StringBuilder();
		for (String importString : imports) {
			importBuilder.append(importString).append("\n");
		}
		st.add("imports", importBuilder.toString());
		return st.render();
	}

	private String getToDoMapperImplStructure(String className, String packageName, String... imports) {

		String template = "package <packageName>;\n<imports>"
				+ "public class <className>Impl implements <className> {}";
		ST st = new ST(template);
		st.add("className", className);
		st.add("packageName", packageName);
		StringBuilder importBuilder = new StringBuilder();
		for (String importString : imports) {
			importBuilder.append(importString).append("\n");
		}
		st.add("imports", importBuilder.toString());
		return st.render();
	}

	private String getMapperSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = metaData.getMapperName();
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

	private String getMapperImplSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = metaData.getMapperName() + "Impl";
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

}
