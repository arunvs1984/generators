package com.reds.generator.redsannotationsprocessor;

import static com.github.javaparser.JavaParser.parseBodyDeclaration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.inject.Singleton;
import javax.tools.Diagnostic;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.SimpleName;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.CoServiceProviderMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ComponentMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ComponentMetaDataWrapper;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.DataObjectMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ModuleMetaData;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.AddToTreasuryMetaData;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;

import dagger.Component;
import dagger.Provides;

public class ComponentGenerator extends AbstractRedsGenerator<ComponentMetaData> {

	private Messager messager;
	private Filer filer;

	public ComponentGenerator(Messager messager, Filer filer) {
		super();
		this.messager = messager;
		this.filer = filer;
	}

	@Override
	public void generate(ComponentMetaData metaData) {
		generateWithJavaParser(metaData);
	}

	private void generateWithJavaParser(ComponentMetaData metaData) {
		// Read Component
		ComponentMetaDataWrapper wrapper = new ComponentMetaDataWrapper(metaData);
		generateComponent(wrapper);
		generateModule(wrapper);

	}

	private void generateModule(ComponentMetaDataWrapper wrapper) {
		try {
			CompilationUnit componentClassUnit = getModuleClassCompilationUnit(wrapper);
			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
				for (AddToTreasuryMetaData addToTreasuryMetaData : wrapper.getMetaData().getParentServiceMetaData()
						.getAddToTreasuryObjects()) {
					String method = getModuleMethod(addToTreasuryMetaData, componentClassUnit,c);
					if (null != method) {
						BodyDeclaration<?> declaration = parseBodyDeclaration(method);
						c.addMember(declaration);
					}
				}
			});
			String sourcePath = getModuleSourcePath(wrapper);
			String newSource = componentClassUnit.toString();
			RedsFileUtils.write(sourcePath, newSource);

		} catch (FileNotFoundException |

				RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String getModuleMethod(AddToTreasuryMetaData addToTreasuryMetaData,
			CompilationUnit componentClassUnit,ClassOrInterfaceDeclaration c) {
		
		String objectName = "";
		String importPackage = "";
		if (addToTreasuryMetaData.isInterfaceRequired()) {
			objectName = addToTreasuryMetaData.getInterfaceName();
			importPackage = addToTreasuryMetaData.getInterfacePackage() + "." + objectName;
		} else {
			objectName = addToTreasuryMetaData.getImplName();
			importPackage = addToTreasuryMetaData.getImplPackage() + "." + objectName;
		}
		String methodName="provide"+objectName;
		if (!c.getMethodsByName(methodName).isEmpty()) { 
			return null;
		}
		ST template = new ST("@Provides\r\n" + 
				"	<singleton>\r\n" + 
				"	public <object> <methodName>() {\r\n" + 
				"		return new <objectImpl>();\r\n" + 
				"	}");
		template.add("object", objectName);
		template.add("methodName", methodName);
		template.add("objectImpl", addToTreasuryMetaData.getImplName());
		if(addToTreasuryMetaData.isSingleton()) {
		template.add("singleton", "@Singleton");
		}
		
		ImportDeclaration importDeclaration = new ImportDeclaration(importPackage, false, false);
		componentClassUnit.getImports().add(importDeclaration);
		
	
		
		return template.render();
	}

	private void generateComponent(ComponentMetaDataWrapper wrapper) {
		try {

			CompilationUnit componentClassUnit = getComponentClassCompilationUnit(wrapper);
			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				for (DataObjectMetaData meteData : wrapper.getMetaData().getDataObjects()) {
					String configName = meteData.getName();
					String methodName = "take" + configName;
					if (c.getMethodsByName(methodName).isEmpty()) {
						ClassOrInterfaceType otherType = new ClassOrInterfaceType();
						otherType.setName(new SimpleName(configName));
						MethodDeclaration methodDeclaration = c.addMethod(methodName, Modifier.PUBLIC);
						methodDeclaration.setName(methodName);
						methodDeclaration.setType(otherType);
						methodDeclaration.setPublic(true);
						methodDeclaration.removeBody();
						methodDeclaration.setJavadocComment(
								"<b>Auto Generated </b><br> \n<b>Purpose: </b> To provide " + configName);
						ImportDeclaration importDeclaration = new ImportDeclaration(
								wrapper.getConfigPackage() + "." + configName, false, false);
						componentClassUnit.getImports().add(importDeclaration);
					}
				}
				for (AddToTreasuryMetaData addToTreasuryMetaData : wrapper.getMetaData().getParentServiceMetaData()
						.getAddToTreasuryObjects()) {
					String objectName = "";
					String importPackage = "";
					if (addToTreasuryMetaData.isInterfaceRequired()) {
						objectName = addToTreasuryMetaData.getInterfaceName();
						importPackage = addToTreasuryMetaData.getInterfacePackage() + "." + objectName;
					} else {
						objectName = addToTreasuryMetaData.getImplName();
						importPackage = addToTreasuryMetaData.getImplPackage() + "." + objectName;
					}
					String methodName = "take" + objectName;
					if (c.getMethodsByName(methodName).isEmpty()) {
						ClassOrInterfaceType otherType = new ClassOrInterfaceType();
						otherType.setName(new SimpleName(objectName));
						MethodDeclaration methodDeclaration = c.addMethod(methodName);
						methodDeclaration.setName(methodName);
						methodDeclaration.setType(otherType);
						methodDeclaration.setPublic(true);
						methodDeclaration.removeBody();
						methodDeclaration.setJavadocComment(
								"<b>Auto Generated </b><br> \n</b><b>Purpose: </b> To take " + objectName);
						ImportDeclaration importDeclaration = new ImportDeclaration(importPackage, false, false);
						componentClassUnit.getImports().add(importDeclaration);
					}
				}
				for (CoServiceMetaData meteData : wrapper.getMetaData().getCoServiceMetaDatas()) {
					String serviceName = meteData.getName();
					String methodName = "access" + serviceName;
					if (c.getMethodsByName(methodName).isEmpty()) {
						ClassOrInterfaceType otherType = new ClassOrInterfaceType();
						otherType.setName(new SimpleName(serviceName));
						MethodDeclaration methodDeclaration = c.addMethod(methodName);
						methodDeclaration.setName(methodName);
						methodDeclaration.setType(otherType);
						methodDeclaration.setPublic(true);
						methodDeclaration.removeBody();
						methodDeclaration
								.setJavadocComment("<b>Auto Generated </b><br> \n</b><b>Purpose: </b> To access "
										+ serviceName + meteData.getVersion());
						ImportDeclaration importDeclaration = new ImportDeclaration(
								meteData.getCoServicePackage() + "." + serviceName, false, false);
						log(importDeclaration.toString());
						componentClassUnit.getImports().add(importDeclaration);
					}
				}

			});
			String sourcePath = getComponentSourcePath(wrapper);
			String newSource = componentClassUnit.toString();
			RedsFileUtils.write(sourcePath, newSource);

		} catch (FileNotFoundException | RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private CompilationUnit getComponentClassCompilationUnit(ComponentMetaDataWrapper wrapper)
			throws FileNotFoundException {
		String sourcePath = getComponentSourcePath(wrapper);
		File componentSourceFile = new File(sourcePath);
		return JavaParser.parse(componentSourceFile);
	}

	private CompilationUnit getModuleClassCompilationUnit(ComponentMetaDataWrapper wrapper)
			throws FileNotFoundException {
		String sourcePath = getModuleSourcePath(wrapper);
		File componentSourceFile = new File(sourcePath);
		return JavaParser.parse(componentSourceFile);
	}

	private String getModuleSourcePath(ComponentMetaDataWrapper wrapper) {
		String className = wrapper.getMetaData().getName() + RedsGeneratorConstants.INJECT_MODULE_POSTFIX;
		String sourcePath = wrapper.getInjectPackagePath() + File.separator + className + ".java";
		return sourcePath;
	}

	private String getComponentSourcePath(ComponentMetaDataWrapper wrapper) {
		String className = wrapper.getMetaData().getName() + RedsGeneratorConstants.INJECT_COMPONENT_POSTFIX;
		String sourcePath = wrapper.getInjectPackagePath() + File.separator + className + ".java";
		return sourcePath;
	}

	private void generateWithPoet(ComponentMetaData metaData) {
//		try {
//			ComponentMetaDataWrapper wrapper = new ComponentMetaDataWrapper(metaData);
//			String className = RedsStringUtils
//					.capitalize(metaData.getName() + RedsGeneratorConstants.GENERATED_COMPONENT_POSTFIX);
//			wrapper.setActualClassName(className);
//			List<MethodSpec> methods = getMethods(wrapper);
//
//			com.squareup.javapoet.TypeSpec.Builder typeBuilder = TypeSpec.interfaceBuilder(className)
//					.addJavadoc(getJavaDoc(className, metaData.getAuthor())).addAnnotation(Singleton.class)
//					.addAnnotation(getComponentAnnoatation(wrapper)).addModifiers(Modifier.PUBLIC);
//
//			for (MethodSpec methodSpec : methods) {
//				typeBuilder.addMethod(methodSpec);
//			}
//			TypeSpec typeSpec = typeBuilder.build();
//
//			JavaFile javaFile = JavaFile
//					.builder(wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName(), typeSpec).build();
//			javaFile.writeTo(System.out);
//			javaFile.writeTo(filer);
//			log("Generated " + className);
//		} catch (IOException e) {
//			RedsannotationsprocessorTrack.me.error("Failed to generate config manager", e);
//		}
	}

	private void log(String message) {
		messager.printMessage(Diagnostic.Kind.NOTE, message);
	}

	private AnnotationSpec getComponentAnnoatation(ComponentMetaDataWrapper wrapper) {
		com.squareup.javapoet.AnnotationSpec.Builder builder = AnnotationSpec.builder(Component.class);

		/* Configuration Manager Module */
		ModuleMetaData configManagerModule = wrapper.getMetaData().getConfigManagerModule();
		String moduleName = RedsStringUtils.capitalize(configManagerModule.getName());
		String configModulePackage = wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName();
		ClassName moduleClassName = ClassName.get(configModulePackage, moduleName);

		/* ServiceProvider Module */
		CoServiceProviderMetaData coServiceProviderMetaData = wrapper.getMetaData().getServiceProviderMetaData();
		String coServicemoduleName = RedsStringUtils.capitalize(coServiceProviderMetaData.getName());
		String coServiceModulePackage = wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName();
		ClassName coServiceModuleClassName = ClassName.get(coServiceModulePackage, coServicemoduleName);

		builder.addMember("modules", "{$T.class,$T.class}", moduleClassName, coServiceModuleClassName);

		return builder.build();

	}

	private String getJavaDoc(String serviceName, String author) {
		String javaDoc = "<b>Purpose: </b> Generated Component class for " + serviceName + "\n\n @author <b>" + author
				+ "</b>" + "\n\n @since " + RedsDateUtils.getCurrenTimeStamp();
		return javaDoc;
	}

	private List<MethodDeclaration> getMethodDeclaration(ComponentMetaDataWrapper wrapper,
			final ClassOrInterfaceDeclaration c) {

		return null;
	}

	private List<MethodSpec> getMethods(ComponentMetaDataWrapper wrapper) {
		List<MethodSpec> methodSpecs = new ArrayList<>();
//		for (DataObjectMetaData meteData : wrapper.getMetaData().getDataObjects()) {
//			String configName = RedsStringUtils.capitalize(meteData.getName());
//			ClassName objectClassName = ClassName.get(wrapper.getConfigPackage(), configName);
//			Builder interfaceMethod = MethodSpec.methodBuilder("take" + configName)
//					.addJavadoc("<b>Purpose: </b> To provide" + configName)
//					.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(objectClassName);
//			methodSpecs.add(interfaceMethod.build());
//		}
//
//		for (CoServiceMetaData meteData : wrapper.getMetaData().getCoServiceMetaDatas()) {
//			String serviceName = RedsStringUtils.capitalize(meteData.getName());
//			ClassName objectClassName = ClassName.get(meteData.getCoServicePackage(), serviceName);
//			String version = meteData.getVersion().replaceAll("\\.", "_");
//			Builder interfaceMethod = MethodSpec.methodBuilder("access" + serviceName + "_" + version)
//					.addJavadoc("<b>Purpose: </b> To access " + serviceName + meteData.getVersion())
//					.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(objectClassName);
//			methodSpecs.add(interfaceMethod.build());
//		}

		return methodSpecs;
	}

}
