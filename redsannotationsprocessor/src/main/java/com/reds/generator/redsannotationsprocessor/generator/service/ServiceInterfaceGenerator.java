package com.reds.generator.redsannotationsprocessor.generator.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.jdo.annotations.PrimaryKey;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.generator.redsannotationsprocessor.exception.RedsannotationsprocessorErrorCodes;
import com.reds.generator.redsannotationsprocessor.exception.RedsannotationsprocessorException;
import com.reds.generator.redsannotationsprocessor.generator.CoServiceTemplates;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceInfo;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceMethod;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceParameter;
import com.reds.generator.redsannotationsprocessor.generator.persistence.DAOImplTemplates;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.api.services.persistence.QueriesMetaData;
import com.reds.redsgenerator.api.services.persistence.QueryMetaData;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class ServiceInterfaceGenerator extends AbstractRedsGenerator<ServicesMetaData> {

	private Messager messager;
	private Filer filer;
	private ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper = null;
	private String serviceInterfacePath;

	public ServiceInterfaceGenerator(Messager messager, Filer filer) {
		super();
		this.messager = messager;
		this.filer = filer;
	}

	public void exposeAsWeb(ServicesMetaData servicesMetaData) throws RedsGeneratorException {
		System.out.println("Exposing Other Services");
		this.serviceMetaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(servicesMetaData);
		String tempPackageString = serviceMetaDataWrapper.getApiPackage().replaceAll("\\.", "/");
		this.serviceInterfacePath = serviceMetaDataWrapper.getMetaData().getLocation() + File.separator
				+ serviceMetaDataWrapper.getMetaData().getApiName() + File.separator
				+ serviceMetaDataWrapper.getMetaData().getLayout().getSrc() + File.separator + tempPackageString;
		// + File.separator + RedsGeneratorConstants.REDS_PERSISTENCE_FOLDER;
		try {
			CompilationUnit classUnit = getServiceInterfaceClassCompilationUnit(serviceMetaDataWrapper,
					serviceInterfacePath);
			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				List<CoServiceMetaData> coServiceMetaDatas = serviceMetaDataWrapper.getMetaData()
						.getCoServiceMetaDatas();
				for (CoServiceMetaData coServiceMetaData : coServiceMetaDatas) {
					if (coServiceMetaData.isExposeAsWeb() || coServiceMetaData.isExpose()) {

						String coServiceFullName = coServiceMetaData.getCoServiceClass();
						CoServiceInfo serviceInfo = APUtils.getCoServiceInfo(coServiceFullName, getClass());
						for (CoServiceMethod method : serviceInfo.getMethods()) {
							String methodName = method.getMethodName();
							if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
								ST st = null;
								if (coServiceMetaData.isExposeAsWeb()) {
									String template = new CoServiceTemplates().getExposeWebTemplateInterface();
									st = new ST(template);
								} else if (coServiceMetaData.isExpose()) {
									String template = new CoServiceTemplates().getExposeTemplateInterface();
									st = new ST(template);
									st.add("returnType", method.getReturnType());
									st.add("exception",
											RedsStringUtils
													.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
															+ RedsGeneratorConstants.EXCEPTION_POSTFIX));

									ImportDeclaration exection = JavaParser.parseImport(
											"import " + this.serviceMetaDataWrapper.getExceptionPackage() + "."
													+ RedsStringUtils.capitalize(
															this.serviceMetaDataWrapper.getMetaData().getName()
																	+ RedsGeneratorConstants.EXCEPTION_POSTFIX)
													+ ";");
									if (!classUnit.getImports().contains(exection)) {
										classUnit.getImports().add(exection);
									}

								} else {
									break;
								}

								st.add("serviceApi", methodName);
								StringBuilder builder = new StringBuilder();
								StringBuilder argumentBuilder = new StringBuilder();
								for (CoServiceParameter parameter : method.getParameters()) {
									builder.append(parameter.getType()).append(" ").append(parameter.getName())
											.append(",");
									argumentBuilder.append(parameter.getName()).append(",");
								}
								String argumentsWithType = APUtils.removeLastComma(builder.toString());
								st.add("argumentsWithType", argumentsWithType);
								System.out.println(st.render());
								c.addMember(JavaParser.parseBodyDeclaration(st.render()));

								for (String importString : method.getImports()) {

									if (importString.indexOf('.') < 0) {
										continue;
									}

									System.out.println("import " + importString + ";");

									ImportDeclaration imp = JavaParser.parseImport("import " + importString + ";");
									if (!classUnit.getImports().contains(imp)) {
										classUnit.getImports().add(imp);
									}

								}

								if (coServiceMetaData.isExposeAsWeb()) {

									ImportDeclaration consumes = JavaParser
											.parseImport("import javax.ws.rs.core.Response;");
									if (!classUnit.getImports().contains(consumes)) {
										classUnit.getImports().add(consumes);
									}
								}
							}
						}
					}

					ImportDeclaration list = new ImportDeclaration(List.class.getName(), false, false);
					if (!classUnit.getImports().contains(list)) {
						classUnit.getImports().add(list);
					}
					ImportDeclaration exection = JavaParser
							.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + ".*;");
					if (!classUnit.getImports().contains(exection)) {
						classUnit.getImports().add(exection);
					}
				}

			});

			String sourcePath = getServiceInterfaceSourcePath(serviceMetaDataWrapper, serviceInterfacePath);
			String newSource = classUnit.toString();
			RedsFileUtils.write(sourcePath, newSource);

		} catch (

		Exception e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to add apis to service interface", e);
		}
	}

	@Override
	public void generate(ServicesMetaData servicesMetaData) throws RedsGeneratorException {
		try {
			this.serviceMetaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(servicesMetaData);
			String tempPackageString = serviceMetaDataWrapper.getApiPackage().replaceAll("\\.", "/");
			this.serviceInterfacePath = serviceMetaDataWrapper.getMetaData().getLocation() + File.separator
					+ serviceMetaDataWrapper.getMetaData().getApiName() + File.separator
					+ serviceMetaDataWrapper.getMetaData().getLayout().getSrc() + File.separator + tempPackageString;
			// + File.separator + RedsGeneratorConstants.REDS_PERSISTENCE_FOLDER;

			CompilationUnit classUnit = getServiceInterfaceClassCompilationUnit(serviceMetaDataWrapper,
					serviceInterfacePath);
			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				/* EXPOSE CRUD */
				List<PersistenceCapableMetaData> persistenceCapables = this.serviceMetaDataWrapper.getMetaData()
						.getPersistenceCapables();
				for (PersistenceCapableMetaData persistenceCapable : persistenceCapables) {
					if (!persistenceCapable.isDaoRequired()) {
						/* If No DAO Required. No Need to Expose CRUD */
						continue;
					}
					if (persistenceCapable.isExposeCrud()) {
						/* Save */
						addSaveMethod(c, persistenceCapable);
						/* Update */
						addUpdateMethod(c, persistenceCapable);
						/* Retrieve */
						addRetrieveMethod(c, persistenceCapable);
						/* Retrieve All */
						addRetrieveAllMethod(c, persistenceCapable);
						/* Delete */
						addDeleteMethod(c, persistenceCapable);
						/* Delete All */
						addDeleteAllMethod(c, persistenceCapable);

						/* All Query Methods */
						addQueryMethods(c, persistenceCapable);

						ImportDeclaration to = JavaParser
								.parseImport("import " + persistenceCapable.getQualifiedClassName() + ";");
						if (!classUnit.getImports().contains(to)) {
							classUnit.getImports().add(to);
						}

					}
				}

				ImportDeclaration list = new ImportDeclaration(List.class.getName(), false, false);
				if (!classUnit.getImports().contains(list)) {
					classUnit.getImports().add(list);
				}
				ImportDeclaration exection = JavaParser
						.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + ".*;");
				if (!classUnit.getImports().contains(exection)) {
					classUnit.getImports().add(exection);
				}

			});
			String sourcePath = getServiceInterfaceSourcePath(serviceMetaDataWrapper, serviceInterfacePath);
			String newSource = classUnit.toString();
			RedsFileUtils.write(sourcePath, newSource);

		} catch (

		Exception e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to add apis to service interface", e);
		}
	}

	private void addRetrieveMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.RETRIEVE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().retrieveToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
		}
	}

	private void addRetrieveAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.RETRIEVE_ALL_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().retrieveAllToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
		}
	}

	private void addDeleteAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.DELETE_ALL_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().deleteAllToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
		}
	}

	private void addDeleteMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.DELETE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().deleteToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
		}
	}

	private void addUpdateMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.UPDATE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().updateToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
		}
	}

	private void addQueryMethods(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		QueriesMetaData metaDatas = persistenceMetaData.getQueriesMetaData();
		if (metaDatas == null) {
			return;
		}
		for (QueryMetaData metaData : metaDatas.getQueries()) {
			String methodName = metaData.getName();
			if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
				String template = null;
				if (metaData.isUnique()) {
					template = new DAOImplTemplates().queryUniqueTemplate();
				} else {
					template = new DAOImplTemplates().queryTemplate();
				}

				ST st = new ST(template);
				String toName = persistenceMetaData.getSimpleName();
				st.add("toName", toName);
				st.add("functionName", methodName);
				st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
				c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			}

		}
	}

	private void addSaveMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.SAVE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().saveToTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));

		}
	}

	private CompilationUnit getServiceInterfaceClassCompilationUnit(ServiceMetaDataWrapper<ServicesMetaData> metaData,
			String serviceInterfacePath)
			throws FileNotFoundException, RedsutilsException, RedsannotationsprocessorException {
		String sourcePath = getServiceInterfaceSourcePath(metaData, serviceInterfacePath);
		File sourceFile = new File(sourcePath);
		if (!sourceFile.exists()) {
			throw new RedsannotationsprocessorException(RedsannotationsprocessorErrorCodes.errorCode(1),
					"Service Interface Class Not found. Always Expecting One");
		}
		return JavaParser.parse(sourceFile);
	}

	private String getServiceInterfaceSourcePath(ServiceMetaDataWrapper<ServicesMetaData> metaData,
			String serviceInterfacePath) {
		String className = metaData.getMetaData().getName();
		String sourcePath = serviceInterfacePath + File.separator + className + ".java";
		return sourcePath;
	}

}
