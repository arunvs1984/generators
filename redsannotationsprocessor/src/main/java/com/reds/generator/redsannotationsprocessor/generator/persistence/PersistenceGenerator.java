package com.reds.generator.redsannotationsprocessor.generator.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.FetchGroup;
import javax.jdo.annotations.FetchGroups;
import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.ForeignKeyAction;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.tools.Diagnostic;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.BodyDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.reds.generator.redsannotations.CoService;
import com.reds.generator.redsannotations.Required;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.CoServiceProviderMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ComponentMetaDataWrapper;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ModuleMetaData;
import com.reds.generator.redsannotationsprocessor.generator.service.ServiceImpleGenerator;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.persistence.FieldMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;

import dagger.Component;

public class PersistenceGenerator extends AbstractRedsGenerator<ServicesMetaData> {

	private Messager messager;
	private Filer filer;
	private String persistencePath;
	private ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper = null;

	private MapperGenerator mapperGenerator = null;
	private DaoGenerator daoGenerator = null;
	private ServiceImpleGenerator serviceImpleGenerator = null;

	public PersistenceGenerator(Messager messager, Filer filer, ServiceImpleGenerator serviceImpleGenerator) {
		super();
		this.messager = messager;
		this.filer = filer;
		this.mapperGenerator = new MapperGenerator();
		this.daoGenerator = new DaoGenerator();
		this.serviceImpleGenerator = serviceImpleGenerator;
	}

	@Override
	public void generate(ServicesMetaData metaData) {
		generateWithJavaParser(metaData);
	}

	private void generateWithJavaParser(ServicesMetaData metaData) {
		// Read Component
		this.serviceMetaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(metaData);
		generateDataObjects(serviceMetaDataWrapper);

	}

	private void generateDataObjects(ServiceMetaDataWrapper<ServicesMetaData> wrapper) {

		String tempPackageString = wrapper.getActualPackage().replaceAll("\\.", "/");

		this.persistencePath = wrapper.getMetaData().getLocation() + File.separator
				+ wrapper.getMetaData().getCamelCaseName() + File.separator + wrapper.getMetaData().getLayout().getSrc()
				+ File.separator + tempPackageString + File.separator + RedsGeneratorConstants.REDS_PERSISTENCE_FOLDER;

		String persistencePathDo = wrapper.getMetaData().getLocation() + File.separator
				+ wrapper.getMetaData().getApiCamelCaseName() + File.separator
				+ wrapper.getMetaData().getLayout().getSrc() + File.separator + tempPackageString + File.separator
				+ RedsGeneratorConstants.REDS_PERSISTENCE_FOLDER;

		System.out.println("DO Path " + persistencePathDo);
		for (PersistenceCapableMetaData persistenceMetaData : wrapper.getMetaData().getPersistenceCapables()) {
			try {
				persistenceMetaData.setDoPackage(
						wrapper.getActualPackage() + "." + RedsGeneratorConstants.REDS_PERSISTENCE_FOLDER);
				CompilationUnit componentClassUnit = getDataObjectClassCompilationUnit(persistenceMetaData,
						persistencePathDo);

				componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
					try {

						if (!c.getJavadoc().isPresent()) {
							c.setJavadocComment(APUtils.getClassJavaDoc(wrapper.getMetaData().getAuthor(),
									"Data Object class for " + persistenceMetaData.getSimpleName()
											+ ".<br>\nThis class used by persistence for persist data to corresponding stores",
									RedsDateUtils.getCurrenTimeStamp()));
						}

						List<BodyDeclaration<?>> gettersAndSetters = new ArrayList<>();
						StringBuilder memberBuilder = new StringBuilder();
						/* Adding All Fields */
						for (FieldMetaData fieldMetaData : persistenceMetaData.getFields()) {
							String name = fieldMetaData.getFieldName();

							if (!c.getFieldByName(name).isPresent()) {
								String type = fieldMetaData.getType();
								if (fieldMetaData.isArray()) {

									type = type + "[]";
								}
								c.addPublicField(type, name);
								gettersAndSetters.add(APUtils.getGetterMethod(type, name));
								gettersAndSetters.add(APUtils.getSetterMethod(type, name));
							}

							if (null != fieldMetaData.getImportString()) {
								ImportDeclaration fieldImport = new ImportDeclaration(fieldMetaData.getImportString(),
										false, false);
								if (!componentClassUnit.getImports().contains(fieldImport)) {
									componentClassUnit.getImports().add(fieldImport);
								}
							}

							boolean primaryKey = fieldMetaData.isPrimaryKey();

							FieldDeclaration field = c.getFieldByName(name).get();
							String columName = fieldMetaData.getFieldName().toUpperCase();
							if (!field.isAnnotationPresent(PrimaryKey.class) && primaryKey) {
								field.addAnnotation(PrimaryKey.class);
								boolean isAutoIncriment = fieldMetaData.isPrimaryAutoIncrement();

								if (isAutoIncriment) {
									field.addAnnotation(
											JavaParser.parseAnnotation("@Persistent(defaultFetchGroup=\"true\", "
													+ "valueStrategy=IdGeneratorStrategy.NATIVE,column = \"" + columName
													+ "\")"));
								} else {
									field.addAnnotation(JavaParser.parseAnnotation(
											" @PrimaryKey\n@Persistent(defaultFetchGroup=\"true\",column = \""
													+ columName + "\""));
								}

								ImportDeclaration persistenceNode = new ImportDeclaration(Persistent.class.getName(),
										false, false);
								if (!componentClassUnit.getImports().contains(persistenceNode)) {
									componentClassUnit.getImports().add(persistenceNode);
								}
								ImportDeclaration idGeneratorStrategyNode = new ImportDeclaration(
										IdGeneratorStrategy.class.getName(), false, false);
								if (!componentClassUnit.getImports().contains(idGeneratorStrategyNode)) {
									componentClassUnit.getImports().add(idGeneratorStrategyNode);
								}
								ImportDeclaration primaryKeyNode = new ImportDeclaration(PrimaryKey.class.getName(),
										false, false);
								if (!componentClassUnit.getImports().contains(primaryKeyNode)) {
									componentClassUnit.getImports().add(primaryKeyNode);
								}

							}
							boolean isList = fieldMetaData.isList();
							boolean isDataObject = fieldMetaData.isDataObject();
							if (!field.isAnnotationPresent(ForeignKey.class) && (isList || isDataObject)) {

								memberBuilder.append("@Persistent(name = \"" + fieldMetaData.getFieldName()
										+ "\",column=\"" + columName + "\")),");

								field.addAnnotation(JavaParser.parseAnnotation(
										"@Persistent(defaultFetchGroup=\"true\",column=\"" + columName + "\")"));
								if (isList) {
									ImportDeclaration elementImport = new ImportDeclaration(Element.class.getName(),
											false, false);
									if (!componentClassUnit.getImports().contains(elementImport)) {
										componentClassUnit.getImports().add(elementImport);
									}
									field.addAnnotation(JavaParser.parseAnnotation("@Element(dependent = \"true\")"));
								}
								field.addAnnotation(
										JavaParser.parseAnnotation("@ForeignKey(deleteAction=ForeignKeyAction.RESTRICT"
												+ ",updateAction=ForeignKeyAction.RESTRICT)"));
								ImportDeclaration foreignKeyImport = new ImportDeclaration(ForeignKey.class.getName(),
										false, false);
								if (!componentClassUnit.getImports().contains(foreignKeyImport)) {
									componentClassUnit.getImports().add(foreignKeyImport);
								}
								ImportDeclaration foreignKeyActionImport = new ImportDeclaration(
										ForeignKeyAction.class.getName(), false, false);
								if (!componentClassUnit.getImports().contains(foreignKeyActionImport)) {
									componentClassUnit.getImports().add(foreignKeyActionImport);
								}
							} else {
								if (!field.isAnnotationPresent(Persistent.class)) {
									field.addAnnotation(JavaParser.parseAnnotation(
											"@Persistent(defaultFetchGroup=\"true\",column=\"" + columName + "\")"));
								}
							}

						}

						/* Adding Getters and Setters for filed */
						for (BodyDeclaration<?> body : gettersAndSetters) {
							c.addMember(body);
						}

						ImportDeclaration FetchGroupsImport = new ImportDeclaration(FetchGroups.class.getName(), false,
								false);
						if (!componentClassUnit.getImports().contains(FetchGroupsImport)) {
							componentClassUnit.getImports().add(FetchGroupsImport);
						}

						ImportDeclaration FetchGroupImport = new ImportDeclaration(FetchGroup.class.getName(), false,
								false);
						if (!componentClassUnit.getImports().contains(FetchGroupImport)) {
							componentClassUnit.getImports().add(FetchGroupImport);
						}

						ImportDeclaration PersistenceCapableImport = new ImportDeclaration(
								PersistenceCapable.class.getName(), false, false);
						if (!componentClassUnit.getImports().contains(PersistenceCapableImport)) {
							componentClassUnit.getImports().add(PersistenceCapableImport);
						}

						ImportDeclaration IdentityTypeImport = new ImportDeclaration(IdentityType.class.getName(),
								false, false);
						if (!componentClassUnit.getImports().contains(IdentityTypeImport)) {
							componentClassUnit.getImports().add(IdentityTypeImport);
						}

						AnnotationExpr fetchGroupExpr = getFetchGroupAnnotation(persistenceMetaData, memberBuilder);
						AnnotationExpr annotationExpr = getPersistenceCapableAnnotation(persistenceMetaData);
						c.getAnnotations().clear();
						c.addAnnotation(fetchGroupExpr);
						c.addAnnotation(annotationExpr);
						if(!APUtils.isMethodAlreadyDeclaredByName(c, "getDoId")){
							addGetId(c, persistenceMetaData);
						}
						
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				});

				String sourcePath = getDataObjectSourcePath(persistenceMetaData, persistencePathDo);
				String newSource = componentClassUnit.toString();
				RedsFileUtils.write(sourcePath, newSource);
				mapperGenerator.generateMapper(persistenceMetaData, wrapper, persistencePath);

				if (persistenceMetaData.isDaoRequired()) {

					daoGenerator.generateDAO(persistenceMetaData, wrapper, persistencePath);
					daoGenerator.generateDAOImpl(persistenceMetaData, wrapper, persistencePath);
					daoGenerator.generateJdbcDAOImpl(persistenceMetaData, wrapper, persistencePath);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void addGetId(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().getDoId();
		ST st = new ST(template);
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("primaryKeyVar", fieldMetaData.getFieldName());
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private AnnotationExpr getFetchGroupAnnotation(PersistenceCapableMetaData persistenceMetaData,
			StringBuilder memberBuilder) {
		String template = "@FetchGroups({ @FetchGroup(name = \"Fetch_<doName>\", " + "members = { <members> }) })";
		ST st = new ST(template);
		String members = memberBuilder.toString();
		if (members.lastIndexOf(',') > 0) {
			members = members.substring(0, members.lastIndexOf(','));
		}
		st.add("doName", persistenceMetaData.getDoName());
		st.add("members", members);
		return JavaParser.parseAnnotation(st.render());
	}

	private String getMemebers(PersistenceCapableMetaData persistenceMetaData) {

		return null;
	}

	private AnnotationExpr getPersistenceCapableAnnotation(PersistenceCapableMetaData persistenceMetaData) {
		String template = "@PersistenceCapable(identityType=IdentityType.APPLICATION,table=\"<tableName>\",detachable=\"true\")";
		String tableName = persistenceMetaData.getSimpleName().toUpperCase();

		// this.serviceMetaDataWrapper.getMetaData().getShortName() + "_"
		// + persistenceMetaData.getDoName();
		ST st = new ST(template);
		st.add("tableName", tableName);
		return JavaParser.parseAnnotation(st.render());
	}

	private CompilationUnit getServiceImplClassCompilationUnit(String serviceImplClass)
			throws FileNotFoundException, RedsutilsException {
		File sourceFile = new File(serviceImplClass);
		return JavaParser.parse(sourceFile);
	}

	private CompilationUnit getDataObjectClassCompilationUnit(PersistenceCapableMetaData metaData,
			String persistencePath) throws FileNotFoundException, RedsutilsException {
		String sourcePath = getDataObjectSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			String packageName = metaData.getDoPackage();
			RedsFileUtils.write(sourcePath, APUtils.getClassStructure(metaData.getDoName(), packageName));
		}
		return JavaParser.parse(componentSourceFile);
	}

	private String getDataObjectSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = metaData.getDoName();
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

	private void log(String message) {
		messager.printMessage(Diagnostic.Kind.NOTE, message);
	}

	private AnnotationSpec getComponentAnnoatation(ComponentMetaDataWrapper wrapper) {
		com.squareup.javapoet.AnnotationSpec.Builder builder = AnnotationSpec.builder(Component.class);

		/* Configuration Manager Module */
		ModuleMetaData configManagerModule = wrapper.getMetaData().getConfigManagerModule();
		String moduleName = RedsStringUtils.capitalize(configManagerModule.getName());
		String configModulePackage = wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName();
		ClassName moduleClassName = ClassName.get(configModulePackage, moduleName);

		/* ServiceProvider Module */
		CoServiceProviderMetaData coServiceProviderMetaData = wrapper.getMetaData().getServiceProviderMetaData();
		String coServicemoduleName = RedsStringUtils.capitalize(coServiceProviderMetaData.getName());
		String coServiceModulePackage = wrapper.getActualPackage() + "." + wrapper.getRedsGenfolderName();
		ClassName coServiceModuleClassName = ClassName.get(coServiceModulePackage, coServicemoduleName);

		builder.addMember("modules", "{$T.class,$T.class}", moduleClassName, coServiceModuleClassName);

		return builder.build();

	}

}
