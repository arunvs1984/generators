package com.reds.generator.redsannotationsprocessor.generator.code;

import java.io.File;

import org.stringtemplate.v4.ST;

import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;

public class ConfigurationGenerator {

	public void generateConfiguration(ConfigurationMetaData metaData) {
		try {
			ST st = new ST(new Template().getConfiguration());
			st.add("packageName", metaData.getPackageName());
			st.add("desc", metaData.getDesc());
			st.add("author", metaData.getAuthor());
			st.add("className", metaData.getName());
			st.add("id", metaData.getId());
			
			String code = st.render();
			String location = metaData.getLocation();
			location=location+File.separator+metaData.getName()+".java";
			System.out.println(code);
			System.out.println("Writing to "+location);
			RedsFileUtils.write(location, code);
		} catch (RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
