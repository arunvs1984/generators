package com.reds.generator.redsannotationsprocessor.generator.persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.library.redsutils.validation.RedsValidator;
import com.reds.redsgenerator.api.services.persistence.FieldMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.api.services.persistence.QueriesMetaData;
import com.reds.redsgenerator.api.services.persistence.QueryMetaData;
import com.reds.redsgenerator.api.type.DataType;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class DaoGenerator {

	private boolean daoChange = false;
	private boolean daoImplChange = false;

	public void generateDAO(PersistenceCapableMetaData persistenceMetaData, ServiceMetaDataWrapper wrapper,
			String persistencePath) {
		genDao(persistenceMetaData, wrapper, persistencePath);

	}

	private void genDao(PersistenceCapableMetaData persistenceMetaData, ServiceMetaDataWrapper wrapper,
			String persistencePath) {
		try {
			CompilationUnit componentClassUnit = getDAOClassCompilationUnit(persistenceMetaData, persistencePath);

			componentClassUnit.getImports()
					.add(new ImportDeclaration(persistenceMetaData.getQualifiedClassName(), false, false));

			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
				NodeList<ClassOrInterfaceType> extendedTypes = c.getExtendedTypes();
				if (extendedTypes == null || extendedTypes.isEmpty()) {
					extendedTypes = new NodeList<>();
					extendedTypes.add(JavaParser
							.parseClassOrInterfaceType("PlatformDAO<" + persistenceMetaData.getSimpleName() + ">"));
					c.setExtendedTypes(extendedTypes);
					c.setJavadocComment(APUtils.getClassJavaDoc(wrapper.getMetaData().getAuthor(),
							"Interface class for " + persistenceMetaData.getSimpleName() + " Data Access Object ",
							RedsDateUtils.getCurrenTimeStamp()));
					daoChange = true;
				}
				/* Adding Queries */
				QueriesMetaData metaDatas = persistenceMetaData.getQueriesMetaData();
				if (null != metaDatas) {
					for (QueryMetaData metaData : metaDatas.getQueries()) {
						String methodName = metaData.getName();
						if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
							String template = null;
							if (metaData.isUnique()) {
								template = new DAOImplTemplates().getQueryUniqueTemplate();
							} else {
								template = new DAOImplTemplates().getQueryTemplate();
							}

							ST st = new ST(template);
							st.add("queryName", methodName);
							st.add("toName", persistenceMetaData.getSimpleName());
							st.add("other", metaData.getArgName());
							c.addMember(JavaParser.parseBodyDeclaration(st.render()));
							daoChange = true;
						}
					}
				}
				ImportDeclaration list = new ImportDeclaration(List.class.getName(), false, false);
				if (!componentClassUnit.getImports().contains(list)) {
					componentClassUnit.getImports().add(list);
				}

				ImportDeclaration exection = JavaParser.parseImport(DAOImplTemplates.DataManagementExceptionPkg);
				if (!componentClassUnit.getImports().contains(exection)) {
					componentClassUnit.getImports().add(exection);
				}

			});

			if (daoChange) {
				System.out.println("Change in DAO");
				String sourcePath = getDAOSourcePath(persistenceMetaData, persistencePath);
				String newSource = componentClassUnit.toString();
				RedsFileUtils.write(sourcePath, newSource);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private CompilationUnit getDAOClassCompilationUnit(PersistenceCapableMetaData metaData, String persistencePath)
			throws FileNotFoundException, RedsutilsException {
		String sourcePath = getDAOSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			String packageName = metaData.getDoPackage();
			String[] imports = { "import com.reds.service.datamanagementapi.persist.PlatformDAO;" };
			RedsFileUtils.write(sourcePath, APUtils.getInterfaceStructure(metaData.getDaoName(), packageName, imports));
		}
		return JavaParser.parse(componentSourceFile);
	}

	private CompilationUnit getJdbcDAOImplClassCompilationUnit(PersistenceCapableMetaData metaData,
			String persistencePath) throws FileNotFoundException, RedsutilsException {
		String sourcePath = getJdbcDAOImplSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			String packageName = metaData.getDoPackage();
			RedsFileUtils.write(sourcePath, APUtils.getClassStructure("Jdbc" + metaData.getDaoImplName(), packageName));

		}
		return JavaParser.parse(componentSourceFile);
	}

	private String getDAOSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = metaData.getDaoName();
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

	public void generateJdbcDAOImpl(PersistenceCapableMetaData persistenceMetaData, ServiceMetaDataWrapper wrapper,
			String persistencePath) {
		try {
			CompilationUnit componentClassUnit = getJdbcDAOImplClassCompilationUnit(persistenceMetaData,
					persistencePath);
			System.out.println("Creating JDBC for "+persistenceMetaData.getSimpleName());
			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				if (!c.getJavadoc().isPresent()) {
					c.setJavadocComment(APUtils.getClassJavaDoc(wrapper.getMetaData().getAuthor(),
							"JDBC DAO Implementation class for " + persistenceMetaData.getSimpleName() + "<br>",
							RedsDateUtils.getCurrenTimeStamp()));
				}

				NodeList<ClassOrInterfaceType> extendedTypes = c.getExtendedTypes();
				if (extendedTypes == null || extendedTypes.isEmpty()) {

					String mapperIml = persistenceMetaData.getMapperName() + "Impl";
					componentClassUnit.getImports()
							.add(new ImportDeclaration(persistenceMetaData.getQualifiedClassName(), false, false));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport(DAOImplTemplates.PlatformJdbcDAOImplPkg));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport(DAOImplTemplates.DataManagementExceptionPkg));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport(DAOImplTemplates.DataManagementErrorCodesPkg));

					componentClassUnit.getImports().add(JavaParser.parseImport("import java.sql.Connection;"));
					componentClassUnit.getImports().add(JavaParser
							.parseImport("import com.reds.service.datamanagementapi.config.ConnectionConfig;"));
					
					componentClassUnit.getImports().add(JavaParser.parseImport("import java.sql.PreparedStatement;"));

					componentClassUnit.getImports().add(JavaParser.parseImport(
							"import " + wrapper.getTreasuryPackage() + "." + wrapper.getTreasuryName() + ";"));

					componentClassUnit.getImports().add(JavaParser.parseImport(
							"import " + wrapper.getTrackerPackage() + "." + wrapper.getTrackerName() + ";"));

					

					extendedTypes = new NodeList<>();
					extendedTypes.add(JavaParser.parseClassOrInterfaceType(
							"PlatformJdbcDAOImpl<" + persistenceMetaData.getSimpleName() + ">"));

					c.setExtendedTypes(extendedTypes);
					String superTemplate = "super(connConfig, tableName);";
					c.addConstructor(Modifier.PUBLIC).addParameter("ConnectionConfig ", "connConfig")
							.addParameter("String", "tableName").addThrownException(Exception.class).createBody()
							.addStatement(superTemplate).addStatement("setInsertQuery();");

					addInsertMethod(wrapper.getTrackerName(),c, persistenceMetaData);

					daoImplChange = true;
				}

				NodeList<ClassOrInterfaceType> implementedTypes = c.getImplementedTypes();
				if (implementedTypes == null || implementedTypes.isEmpty()) {
					implementedTypes = new NodeList<>();
					implementedTypes.add(JavaParser.parseClassOrInterfaceType(persistenceMetaData.getDaoName()));
					c.setImplementedTypes(implementedTypes);
					daoImplChange = true;
				}

			});

			if (daoImplChange) {
				System.out.println("Change in DAO Impl");
				String sourcePath = getJdbcDAOImplSourcePath(persistenceMetaData, persistencePath);
				String newSource = componentClassUnit.toString();
				RedsFileUtils.write(sourcePath, newSource);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateDAOImpl(PersistenceCapableMetaData persistenceMetaData, ServiceMetaDataWrapper wrapper,
			String persistencePath) {
		try {
			CompilationUnit componentClassUnit = getDAOImplClassCompilationUnit(persistenceMetaData, persistencePath);

			componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				if (!c.getJavadoc().isPresent()) {
					c.setJavadocComment(APUtils.getClassJavaDoc(wrapper.getMetaData().getAuthor(),
							"DAO Implementation class for " + persistenceMetaData.getSimpleName() + "<br>",
							RedsDateUtils.getCurrenTimeStamp()));
				}

				NodeList<ClassOrInterfaceType> extendedTypes = c.getExtendedTypes();
				if (extendedTypes == null || extendedTypes.isEmpty()) {

					String mapperIml = persistenceMetaData.getMapperName() + "Impl";
					componentClassUnit.getImports()
							.add(new ImportDeclaration(persistenceMetaData.getQualifiedClassName(), false, false));

					componentClassUnit.getImports().add(JavaParser.parseImport(DAOImplTemplates.PlatformDAOImplPkg));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport(DAOImplTemplates.DataManagementExceptionPkg));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport(DAOImplTemplates.DataManagementErrorCodesPkg));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + PersistenceManagerFactory.class.getName() + ";"));

					componentClassUnit.getImports().add(JavaParser
							.parseImport("import " + persistenceMetaData.getDoPackage() + "." + mapperIml + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + Transaction.class.getName() + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + Extent.class.getName() + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + Query.class.getName() + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + PersistenceManager.class.getName() + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + ArrayList.class.getName() + ";"));

					componentClassUnit.getImports()
							.add(JavaParser.parseImport("import " + Iterator.class.getName() + ";"));

					componentClassUnit.getImports().add(JavaParser.parseImport("import " + List.class.getName() + ";"));
					componentClassUnit.getImports().add(JavaParser.parseImport("import " + RedsValidator.class.getName() + ";"));

					componentClassUnit.getImports().add(JavaParser.parseImport(
							"import " + wrapper.getTreasuryPackage() + "." + wrapper.getTreasuryName() + ";"));

					c.addPrivateField(persistenceMetaData.getMapperName(), "mapper");

					extendedTypes = new NodeList<>();
					extendedTypes.add(JavaParser
							.parseClassOrInterfaceType("PlatformDAOImpl<" + persistenceMetaData.getSimpleName() + ">"));

					c.setExtendedTypes(extendedTypes);
					String superTemplate = "super(" + wrapper.getTreasuryName()
							+ ".open.accessDataManagement().getPMFactory(connection));";
					c.addConstructor(Modifier.PUBLIC).addParameter("String ", "connection")
							.addThrownException(Exception.class).createBody().addStatement(superTemplate)
							.addStatement("this.mapper= new " + mapperIml + "();");
					superTemplate = "super(pmf, externalPm);";
					c.addConstructor(Modifier.PUBLIC).addParameter("PersistenceManagerFactory", "pmf")
							.addParameter("PersistenceManager", "externalPm").addThrownException(Exception.class)
							.createBody().addStatement(superTemplate)
							.addStatement("this.mapper= new " + mapperIml + "();");

					addGetDataObjectDO(c, persistenceMetaData);

					addGetQueryById(c, persistenceMetaData);
					addPersistMethod(c, persistenceMetaData);
					addRetrieveAllMethod(c, persistenceMetaData);
					addRetrieveMethod(c, persistenceMetaData);
					addDeleteAllMethod(c, persistenceMetaData);
					addDeleteMethod(c, persistenceMetaData);
					daoImplChange = true;
				}

				addQueriesImplMethods(c, persistenceMetaData);

				NodeList<ClassOrInterfaceType> implementedTypes = c.getImplementedTypes();
				if (implementedTypes == null || implementedTypes.isEmpty()) {
					implementedTypes = new NodeList<>();
					implementedTypes.add(JavaParser.parseClassOrInterfaceType(persistenceMetaData.getDaoName()));
					c.setImplementedTypes(implementedTypes);
					daoImplChange = true;
				}

			});

			if (daoImplChange) {
				System.out.println("Change in DAO Impl");
				String sourcePath = getDAOImplSourcePath(persistenceMetaData, persistencePath);
				String newSource = componentClassUnit.toString();
				RedsFileUtils.write(sourcePath, newSource);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addQueriesImplMethods(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		QueriesMetaData metaDatas = persistenceMetaData.getQueriesMetaData();
		if (metaDatas != null) {
			for (QueryMetaData metaData : metaDatas.getQueries()) {
				String methodName = metaData.getName();
				if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
					String template = null;
					if (metaData.isUnique()) {
						template = new DAOImplTemplates().getQueryUniqueImplTemplate();
					} else {
						template = new DAOImplTemplates().getQueryImplTemplate();
					}

					ST st = new ST(template);
					st.add("queryName", methodName);
					st.add("doName", persistenceMetaData.getDoName());
					st.add("toName", persistenceMetaData.getSimpleName());
					st.add("filter", metaData.getFilter());
					st.add("declareParam", metaData.getDeclareParams());
					String setParam = metaData.getSetParams();
					StringBuilder actualParam = new StringBuilder();
					if (setParam != null) {
						String[] setParams = setParam.split(",");
						for (String param : setParams) {
							if (param.indexOf('(') > 0) {
								actualParam.append(metaData.getArgName()).append(".").append(param);
							} else {
								actualParam.append(metaData.getArgName()).append(".get")
										.append(RedsStringUtils.capitalize(param)).append("(),");
							}

						}
					}
					setParam = APUtils.removeLastComma(actualParam.toString());
					st.add("setParam", setParam);
					st.add("other", metaData.getArgName());
					System.out.println(st.render());
					c.addMember(JavaParser.parseBodyDeclaration(st.render()));
					daoImplChange = true;
				}
			}
		}

	}

	private void addGetDataObjectDO(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().getDOTemplate();
		ST st = new ST(template);
		String doName = persistenceMetaData.getDoName();
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("doName", doName);
		st.add("primaryKeyType", fieldMetaData.getType());
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private void addGetQueryById(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().getQueryById();
		ST st = new ST(template);
		String doName = persistenceMetaData.getDoName();
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("doName", doName);
		st.add("primaryKeyVar", fieldMetaData.getFieldName());
		st.add("primaryKeyType", fieldMetaData.getType());
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private void addInsertMethod(String tracker,ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().insertTemplate();
		ST assignInsertSt = null;
		int index = 0;
		StringBuilder insertSettings = new StringBuilder();
		StringBuilder insertBuilder = new StringBuilder();
		StringBuilder markerBuilder = new StringBuilder();
		

		for (FieldMetaData field : persistenceMetaData.getFields()) {
			
			index++;
			DataType dataType = DataType.getDataType(field.getType());
			insertBuilder.append(field.getFieldName().toUpperCase()).append(",");
			markerBuilder.append("?,");
			System.out.println("Processing Fields"+ field.getFieldName()+">"+dataType);
			switch (dataType) {
			case BLOB:
				break;
			case BOOLEAN:
				assignInsertSt = new ST("\tstmt$.setBoolean(<index>, to.get<fieldName>());\n");
				break;
			case BYTE:				
				if(field.isArray()) {
					assignInsertSt = new ST("\tstmt$.setBytes(<index>, to.get<fieldName>());\n");
				}else {
					assignInsertSt = new ST("\tstmt$.setByte(<index>, to.get<fieldName>());\n");
				}
				break;
			case BYTES:
				assignInsertSt = new ST("\tstmt$.setBytes(<index>, to.get<fieldName>());\n");
				break;
			case CHAR:
				assignInsertSt = new ST("\tstmt$.setString(<index>, String.valueOf(to.get<fieldName>()));\n");
				break;
			case DATE:
				assignInsertSt = new ST(
						"\tstmt$.setDate(<index>,  new java.sql.Date(to.get<fieldName>()!=null?to.get<fieldName>().getTime():null));\n");
				break;
			case DO:
				break;
			case DOUBLE:
				assignInsertSt = new ST("\tstmt$.setDouble(<index>, to.get<fieldName>());\n");
				break;
			case ENUM:

				break;
			case FLOAT:
				assignInsertSt = new ST("\tstmt$.setFloat(<index>, to.get<fieldName>());\n");
				break;
			case GENERIC:
				break;
			case INT:
				assignInsertSt = new ST("\tstmt$.setInt(<index>, to.get<fieldName>());\n");
				break;
			case LIST:
				break;
			case LONG:
				assignInsertSt = new ST("\tstmt$.setLong(<index>, to.get<fieldName>());\n");
				break;
			case MAP:
				break;
			case OBJECT:
				assignInsertSt = new ST("\tstmt$.setObject(<index>, to.get<fieldName>());\n");
				break;
			case SHORT:
				assignInsertSt = new ST("\tstmt$.setShort(<index>, to.get<fieldName>());\n");
				break;
			case STRING:
				assignInsertSt = new ST("\tstmt$.setString(<index>, to.get<fieldName>());\n");
				break;
			case TimeStamp:
				assignInsertSt = new ST(
						"\tstmt$.setTimestamp(<index>, new java.sql.Timestamp(to.get<fieldName>()!=null?to.get<fieldName>().getTime():null));\n");
				break;
			case VOID:
				break;
			default:
				assignInsertSt = new ST();
				break;
			}
			assignInsertSt.add("fieldName", RedsStringUtils.capitalize(field.getFieldName()));
			assignInsertSt.add("index", index);
			insertSettings.append(assignInsertSt.render());
		}
		ST st = new ST(template);
		String toName = persistenceMetaData.getSimpleName();
		st.add("toName", toName);
		st.add("setstatement", insertSettings.toString());
		st.add("tracker", tracker);
		
		String insertMethod=st.render();
		System.out.println(insertMethod);
		c.addMember(JavaParser.parseBodyDeclaration(insertMethod));
		
		String insertTemplateString = new DAOImplTemplates().insertQueryTemplate();
		ST insertTemplate = new ST("INSERT INTO \"+super.finalTableName +\" (<insertColumn>) VALUES (<markers>);");
		insertTemplate.add("insertColumn", RedsStringUtils.removeLastComma(insertBuilder.toString()));
		insertTemplate.add("markers", RedsStringUtils.removeLastComma(markerBuilder.toString()));
		
		ST setQuery = new ST(insertTemplateString);
		setQuery.add("insertQuery", insertTemplate.render());
		String setMethod=setQuery.render();
		System.out.println(setMethod);
		c.addMember(JavaParser.parseBodyDeclaration(setMethod));

	}

	private void addPersistMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().persistTemplate();
		ST st = new ST(template);
		String toName = persistenceMetaData.getSimpleName();
		String doName = persistenceMetaData.getDoName();
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("toName", toName);
		st.add("doName", doName);
		st.add("primaryKey", RedsStringUtils.capitalize(fieldMetaData.getFieldName()));
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));

		template = new DAOImplTemplates().persistWithOutTransactionTemplate();
		st = new ST(template);
		toName = persistenceMetaData.getSimpleName();
		doName = persistenceMetaData.getDoName();
		fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("toName", toName);
		st.add("doName", doName);
		st.add("primaryKey", RedsStringUtils.capitalize(fieldMetaData.getFieldName()));
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));

	}

	private void addRetrieveAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().retrieveAllTemplate();
		ST st = new ST(template);
		String toName = persistenceMetaData.getSimpleName();
		String doName = persistenceMetaData.getDoName();
		st.add("toName", toName);
		st.add("doName", doName);
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private void addRetrieveMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().retrieveTemplate();
		ST st = new ST(template);
		String toName = persistenceMetaData.getSimpleName();
		String doName = persistenceMetaData.getDoName();
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		st.add("toName", toName);
		st.add("doName", doName);
		st.add("primaryKey", RedsStringUtils.capitalize(fieldMetaData.getFieldName()));
		st.add("primaryKeyVar", fieldMetaData.getFieldName());
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private void addDeleteAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().deleteAllTemplate();
		ST st = new ST(template);
		String doName = persistenceMetaData.getDoName();
		st.add("doName", doName);
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private void addDeleteMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String template = new DAOImplTemplates().deleteTemplate();
		ST st = new ST(template);
		String doName = persistenceMetaData.getDoName();
		FieldMetaData fieldMetaData = APUtils.getPrimaryKey(persistenceMetaData.getFields());
		String toName = persistenceMetaData.getSimpleName();
		st.add("toName", toName);
		st.add("doName", doName);
		st.add("primaryKey", RedsStringUtils.capitalize(fieldMetaData.getFieldName()));
		c.addMember(JavaParser.parseBodyDeclaration(st.render()));
	}

	private CompilationUnit getDAOImplClassCompilationUnit(PersistenceCapableMetaData metaData, String persistencePath)
			throws FileNotFoundException, RedsutilsException {
		String sourcePath = getDAOImplSourcePath(metaData, persistencePath);
		File componentSourceFile = new File(sourcePath);
		if (!componentSourceFile.exists()) {
			String packageName = metaData.getDoPackage();
			RedsFileUtils.write(sourcePath, APUtils.getClassStructure(metaData.getDaoImplName(), packageName));

		}
		return JavaParser.parse(componentSourceFile);
	}

	private String getDAOImplSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = metaData.getDaoImplName();
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

	private String getJdbcDAOImplSourcePath(PersistenceCapableMetaData metaData, String persistencePath) {
		String className = "Jdbc" + metaData.getDaoImplName();
		String sourcePath = persistencePath + File.separator + className + ".java";
		return sourcePath;
	}

}
