package com.reds.generator.redsannotationsprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.reds.generator.redsannotations.CoService;
import com.reds.generator.redsannotations.Configuration;
import com.reds.generator.redsannotations.Required;
import com.reds.generator.redsannotations.Service;
import com.reds.generator.redsannotations.cache.Cache;
import com.reds.generator.redsannotations.cache.Cacheable;
import com.reds.generator.redsannotations.persistence.DataObjectType;
import com.reds.generator.redsannotations.persistence.EnumType;
import com.reds.generator.redsannotations.persistence.PersistenceCapable;
import com.reds.generator.redsannotations.persistence.PrimaryKey;
import com.reds.generator.redsannotations.persistence.Queries;
import com.reds.generator.redsannotations.persistence.Query;
import com.reds.generator.redsannotations.treasury.AddToTreasury;
import com.reds.generator.redsannotationsprocessor.exception.RedsannotationsprocessorErrorCodes;
import com.reds.generator.redsannotationsprocessor.exception.RedsannotationsprocessorException;
import com.reds.generator.redsannotationsprocessor.generator.ComponentGenerator;
import com.reds.generator.redsannotationsprocessor.generator.ConfigManagerGenerator;
import com.reds.generator.redsannotationsprocessor.generator.ServiceProviderGenerator;
import com.reds.generator.redsannotationsprocessor.generator.cache.CacheManagerGenerator;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ComponentMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.DataObjectMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigManagerMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider.ServiceProviderMetaData;
import com.reds.generator.redsannotationsprocessor.generator.persistence.PersistenceGenerator;
import com.reds.generator.redsannotationsprocessor.generator.service.ServiceImpleGenerator;
import com.reds.generator.redsannotationsprocessor.generator.service.ServiceInterfaceGenerator;
import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.xml.RedsXmlManager;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.library.redsutils.json.Json;
import com.reds.redsgenerator.api.RedsGeneratorApiUtils;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.AddToTreasuryMetaData;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.cache.CacheMetaData;
import com.reds.redsgenerator.api.services.persistence.FieldMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.api.services.persistence.QueriesMetaData;
import com.reds.redsgenerator.api.services.persistence.QueryMetaData;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

@SupportedAnnotationTypes({ AnnotationNames.CONFIG, AnnotationNames.SERVICE, AnnotationNames.REQUIRED })
@SupportedSourceVersion(SourceVersion.RELEASE_9)
public class RedsAnnotationProcessor extends AbstractProcessor {

	private ConfigManagerGenerator configManagerGenerator = null;
	private ServiceProviderGenerator serviceProviderGenerator = null;
	private ServiceInterfaceGenerator serviceInterfaceGenerator = null;
	private PersistenceGenerator persistenceGenerator = null;
	private ServiceImpleGenerator serviceImpleGenerator = null;
	private CacheManagerGenerator cacheManagerGenerator = null;
	private ComponentGenerator componentGenerator = null;
	private ServicesMetaData servicesMetaData = null;
	private Messager messager;
	private Filer filer;

	private TypeElement currentElement;
	private Types TypeUtils;
	private Elements elementUtils;
	private Path serviceRoot;
	private ServiceInfo serviceInfo = null;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		System.out.println(RedsDateUtils.getNow() + "SHINEED INIT>>>>>>>>>>>>" + getSupportedAnnotationTypes() + ":"
				+ getSupportedSourceVersion());
		System.out.println("Initializing Annotation Processor.");
		// super.init(processingEnv);
		this.processingEnv = processingEnv;
		this.TypeUtils = processingEnv.getTypeUtils();
		this.messager = processingEnv.getMessager();
		this.elementUtils = processingEnv.getElementUtils();
		this.filer = processingEnv.getFiler();
		this.configManagerGenerator = new ConfigManagerGenerator(messager, filer);
		this.componentGenerator = new ComponentGenerator(messager, filer);
		this.serviceProviderGenerator = new ServiceProviderGenerator(messager, filer);
		this.serviceImpleGenerator = new ServiceImpleGenerator();
		this.persistenceGenerator = new PersistenceGenerator(messager, filer, this.serviceImpleGenerator);
		this.serviceInterfaceGenerator = new ServiceInterfaceGenerator(messager, filer);
		this.cacheManagerGenerator = new CacheManagerGenerator(this.serviceImpleGenerator);
		System.out.println("Filer" + filer);
		System.out.println("Messager" + messager);
	}

	private void printError(String msg) {
		this.processingEnv.getMessager().printMessage(Kind.ERROR, msg, currentElement);
	}

	private void printWarning(String msg) {
		this.processingEnv.getMessager().printMessage(Kind.WARNING, msg, currentElement);
	}

	private void printNotice(String msg) {
		this.processingEnv.getMessager().printMessage(Kind.NOTE, msg, currentElement);
	}

	private static AnnotationMirror getAnnotationMirror(TypeElement typeElement, String className) {
		for (AnnotationMirror m : typeElement.getAnnotationMirrors()) {
			if (m.getAnnotationType().toString().equals(className)) {
				return m;
			}
		}
		return null;
	}

	private static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key) {
		for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror.getElementValues()
				.entrySet()) {
			if (entry.getKey().getSimpleName().toString().equals(key)) {
				return entry.getValue();
			}
		}
		return null;
	}

	private TypeElement getAnnotationValueAsType(AnnotationMirror annotationMirror, String key) {
		AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);
		if (annotationValue == null) {
			return null;
		}
		TypeMirror typeMirror = (TypeMirror) annotationValue.getValue();
		if (typeMirror == null) {
			return null;
		}
		return (TypeElement) TypeUtils.asElement(typeMirror);
	}

	public TypeMirror getTypeMirrorFoCoService(CoService coService) {
		try {
			coService.service();
		} catch (MirroredTypeException mte) {
			return mte.getTypeMirror();
		}
		return null;

	}

	private String toFilename(TypeElement element) {
		return element.getQualifiedName().toString().replace('.', '/') + ".java";
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		System.out.println("REDSANNO>" + processingEnv + annotations.size() + " > " + annotations);
		if (annotations.size() == 0) {
			return true;
		}
		boolean isApi = false;
		for (Element serviceElement : roundEnv.getElementsAnnotatedWith(Service.class)) {
			if (serviceElement instanceof TypeElement) {
				try {
					TypeElement typeElement = (TypeElement) serviceElement;
					initServiceRootPath(typeElement, true);
					processServiceAnnotation(typeElement);
				} catch (RedsutilsException | IOException | RedsXmlException | RedsannotationsprocessorException e1) {
					printError("Failed to parse Service Annotataion " + e1.getLocalizedMessage());
					e1.printStackTrace();
				}

			}
			/* Service Annotation is expecting only at Api module */
			isApi = true;
		}

		if (isApi) {
			try {
				handlePresistanceCapable(roundEnv);
				handleCacheable(roundEnv);
				handleQueries(roundEnv);
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			handleAddToTreasury(roundEnv);
			try {
				saveServiceMetaData(this.serviceRoot);
			} catch (RedsutilsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				/* EXPOSING CRUD and Other Services if Configured */
				this.serviceInterfaceGenerator.generate(servicesMetaData);
			} catch (RedsGeneratorException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			/* No more Annotataions Expecting in API */
			return true;
		}

		if (this.servicesMetaData == null) {

			try {
				initServiceRootPath(null, isApi);
				// FileObject fileObject = filer.getResource(StandardLocation.CLASS_OUTPUT, "",
				// RedsGeneratorConstants.AUTO_GEN_JSON);
				// Path autoGenFilePath = Paths.get(fileObject.toUri());
				String implemenatationGenPath = getSrcMainPath(serviceRoot) + RedsGeneratorConstants.AUTO_GEN_JSON;

				this.servicesMetaData = Json.fromJsonFile(implemenatationGenPath, ServicesMetaData.class);

			} catch (Exception e1) {
				printError("Failed to read service metadata " + e1.getLocalizedMessage());
				e1.printStackTrace();
			}
			if (this.servicesMetaData == null) {
				printError("No Service MetaData Found");
				return true;
			}

			handleAddToTreasury(roundEnv);

		}
		ConfigManagerMetaData configMetaData = new ConfigManagerMetaData(this.servicesMetaData);
		ComponentMetaData componentMetaData = new ComponentMetaData(this.servicesMetaData);
		for (Element e : roundEnv.getElementsAnnotatedWith(Configuration.class)) {
			if (e instanceof TypeElement) {
				TypeElement te = (TypeElement) e;
				System.out.println("CONFIGURATION>>>>>>>>>>>>" + te.getSimpleName());
				Name configName = te.getSimpleName();
				ConfigMetaData config1 = new ConfigMetaData(configName.toString(), this.servicesMetaData.getLocation());
				configMetaData.getConfigs().add(config1);
				componentMetaData.getDataObjects()
						.add(new DataObjectMetaData(configName.toString(), this.servicesMetaData.getLocation()));
			}

		}
		ServiceProviderMetaData serviceProviderMetaData = new ServiceProviderMetaData(this.servicesMetaData);
		System.out.println("Creating ServiceProvider MetaData");
		for (Element e : roundEnv.getElementsAnnotatedWith(Required.class)) {
			if (e instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) e;
				System.out.println("REQUIRED>>>>>>>>>>>>" + typeElement.getSimpleName());
				Required required = typeElement.getAnnotation(Required.class);
				CoService[] coServices = required.value();
				for (CoService coService : coServices) {
					TypeElement serviceClass = APUtils.asTypeElement(getTypeMirrorFoCoService(coService), TypeUtils);
					System.out.println("Co-Service: to String " + serviceClass.toString());
					System.out.println(
							"Co-Service: serviceClass.getClass().getName() " + serviceClass.getClass().getName());
					System.out
							.println("Co-Service: serviceClass.getQualifiedName() " + serviceClass.getQualifiedName());
					CoServiceMetaData metaData = new CoServiceMetaData(coService.id(), coService.name(),
							servicesMetaData.getLocation(), coService.version(), serviceClass.toString(),
							coService.resilience());
					metaData.setExpose(coService.expose());
					metaData.setExposeAsWeb(coService.exposeAsWeb());
					serviceProviderMetaData.getServices().add(metaData);
					componentMetaData.getCoServiceMetaDatas().add(metaData);
					servicesMetaData.getCoServiceMetaDatas().add(metaData);

				}
			}

		}
		this.serviceProviderGenerator.generate(serviceProviderMetaData);
		this.configManagerGenerator.generate(configMetaData);
		this.componentGenerator.generate(componentMetaData);
		this.persistenceGenerator.generate(servicesMetaData);
		this.serviceImpleGenerator.addRequiredExposes(new ServiceMetaDataWrapper<ServicesMetaData>(servicesMetaData));
		try {
			this.serviceInterfaceGenerator.exposeAsWeb(servicesMetaData);
		} catch (RedsGeneratorException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		this.cacheManagerGenerator.generate(servicesMetaData);

		/* Writing to file */
		if (serviceRoot != null && this.servicesMetaData != null) {
			String implemenatationGenPath = getSrcMainPath(serviceRoot) + RedsGeneratorConstants.AUTO_GEN_JSON;
			try {
				System.out.println("Writing Service metadata to path "+ implemenatationGenPath);
				Json.toJsonFile(implemenatationGenPath, this.servicesMetaData);
			} catch (RedsutilsException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return true;
	}

	private void handleCacheable(RoundEnvironment roundEnv) throws FileNotFoundException {
		for (Element element : roundEnv.getElementsAnnotatedWith(Cacheable.class)) {
			if (element instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) element;
				processCacheableAnnotation(typeElement);
			}
		}

	}

	private void handlePresistanceCapable(RoundEnvironment roundEnv) throws FileNotFoundException {
		for (Element element : roundEnv.getElementsAnnotatedWith(PersistenceCapable.class)) {
			if (element instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) element;
				processPresistanceCapableAnnotation(typeElement);
			}
		}

	}

	private void handleQueries(RoundEnvironment roundEnv) throws FileNotFoundException {
		for (Element element : roundEnv.getElementsAnnotatedWith(Queries.class)) {
			if (element instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) element;
				processQueriesAnnotation(typeElement);
			}
		}

	}

	private void processQueriesAnnotation(TypeElement typeElement) throws FileNotFoundException {
		String qualifiedName = typeElement.getQualifiedName().toString();
		PersistenceCapableMetaData persistenceMetaData = null;
		for (PersistenceCapableMetaData persistenceCapableMetaData : this.servicesMetaData.getPersistenceCapables()) {
			if (persistenceCapableMetaData.getQualifiedClassName().equals(qualifiedName)) {
				persistenceMetaData = persistenceCapableMetaData;
				break;
			}

		}
		if (persistenceMetaData != null) {
			Queries queries = typeElement.getAnnotation(Queries.class);
			Query[] querys = queries.value();
			QueriesMetaData queriesMetaData = new QueriesMetaData();
			persistenceMetaData.setQueriesMetaData(queriesMetaData);
			for (Query query : querys) {
				QueryMetaData queryMetaData = new QueryMetaData();
				queryMetaData.setArgName(query.argName());
				queryMetaData.setDeclareParams(query.declareParams());
				queryMetaData.setFilter(query.filter());
				queryMetaData.setSetParams(query.setParams());
				queryMetaData.setName(query.name());
				queryMetaData.setUnique(query.unique());
				queriesMetaData.getQueries().add(queryMetaData);
			}
		}

	}

	private void processCacheableAnnotation(TypeElement typeElement) throws FileNotFoundException {
		Cacheable cacheable = typeElement.getAnnotation(Cacheable.class);
		Cache[] caches = cacheable.value();
		if (caches != null) {
			for (Cache cache : caches) {

				CacheMetaData cacheMetaData = new CacheMetaData();
				cacheMetaData.setName(cache.name());
				String qualifiedName = typeElement.getQualifiedName().toString();
				cacheMetaData.setObjectName(qualifiedName);
				System.out.println(qualifiedName + ">" + cache.name());
				this.servicesMetaData.getCacheableMetaData().getCacheMetaDatas().add(cacheMetaData);
			}

		}

	}

	private void processPresistanceCapableAnnotation(TypeElement typeElement) throws FileNotFoundException {
		PersistenceCapable persistenceCapable = typeElement.getAnnotation(PersistenceCapable.class);
		PersistenceCapableMetaData metaData = new PersistenceCapableMetaData();
		if (persistenceCapable.exposes()) {
			metaData.setExposeCrud(true);
		}
		if (persistenceCapable.daoRequired()) {
			metaData.setDaoRequired(true);
		}
		String qualifiedName = typeElement.getQualifiedName().toString();
		metaData.setQualifiedClassName(qualifiedName);
		CompilationUnit componentClassUnit = getPersistenceClassClassCompilationUnit(qualifiedName);
		componentClassUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

			List<FieldDeclaration> fields = c.getFields();
			for (FieldDeclaration declaration : fields) {
				FieldMetaData fieldMetaData = new FieldMetaData();
				VariableDeclarator variable = declaration.getVariable(0);
				if (variable == null) {
					continue;
				}

				fieldMetaData.setFieldName(variable.getNameAsString());

				Optional<Expression> initializer = variable.getInitializer();
				if (initializer != null) {
					if (initializer.isPresent()) {
						Expression expression = initializer.get();
						if (null != expression) {
							// TODO
							// fieldMetaData.setInitialValue(expression.toString());
						}
					}
				}

				String elementType = declaration.getElementType().asString();

				System.out.println("Processing " + fieldMetaData.getFieldName() + " " + elementType);

				if (declaration.getElementType().getParentNode().get().toString().indexOf("[]") > 0) {
					fieldMetaData.setArray(true);
				}

				if (declaration.isAnnotationPresent(EnumType.class.getSimpleName())) {
					fieldMetaData.setEnumType(true);
					fieldMetaData.setType(elementType);
					fieldMetaData.setImportString(
							qualifiedName.substring(0, qualifiedName.lastIndexOf('.')) + "." + elementType);
				} else if (declaration.getElementType().isClassOrInterfaceType()
						&& !APUtils.isNotDataObject(elementType)) {
					if (elementType.startsWith("List<")) {

						elementType = "List<"
								+ declaration.getElementType().asClassOrInterfaceType().getChildNodes().get(1)
								+ RedsGeneratorConstants.DATAOBJECT_POSTFIX + ">";
						fieldMetaData.setType(elementType);
						fieldMetaData.setToType("List<"
								+ declaration.getElementType().asClassOrInterfaceType().getChildNodes().get(1) + ">");
						fieldMetaData.setToName(
								declaration.getElementType().asClassOrInterfaceType().getChildNodes().get(1) + "");
						fieldMetaData.setImportString(List.class.getName());
						fieldMetaData.setToImportString(qualifiedName.substring(0, qualifiedName.lastIndexOf('.')) + "."
								+ fieldMetaData.getToName());
						fieldMetaData.setList(true);

					} else {
						fieldMetaData.setType(elementType + RedsGeneratorConstants.DATAOBJECT_POSTFIX);
						fieldMetaData.setToType(elementType);
						fieldMetaData.setToName(elementType);
						fieldMetaData.setToImportString(
								qualifiedName.substring(0, qualifiedName.lastIndexOf('.')) + "." + elementType);
						fieldMetaData.setDataObject(true);
					}
				} else if (elementType.equals(Date.class.getSimpleName())) {
					fieldMetaData.setImportString(Date.class.getName());
					fieldMetaData.setType(elementType);
				} else if (elementType.equals(Timestamp.class.getSimpleName())) {
					fieldMetaData.setImportString(Timestamp.class.getName());
					fieldMetaData.setType(elementType);
				} else {
					fieldMetaData.setType(elementType);
				}

				if (declaration.isAnnotationPresent(DataObjectType.class.getSimpleName())) {
					// TODO
				}
				if (declaration.isAnnotationPresent(PrimaryKey.class.getSimpleName())) {
					// Optional<AnnotationExpr> primaryKey =
					// declaration.getAnnotationByClass(PrimaryKey.class);
					// primaryKey.get().getA
					fieldMetaData.setPrimaryKey(true);
					// fieldMetaData.setPrimaryAutoIncrement(primaryKey.autoIncrement());
				}

				metaData.getFields().add(fieldMetaData);
			}
		});

		this.servicesMetaData.getPersistenceCapables().add(metaData);

	}

//	private void addImportForClassOrInterfaceType(CompilationUnit componentClassUnit, FieldMetaData fieldMetaData,
//			FieldDeclaration declaration) {
//		for(ImportDeclaration importDeclaration: componentClassUnit.getImports()) {
//			if(importDeclaration.getNameAsString().endsWith(fieldMetaData.getFieldName())) {
//				fieldMetaData.setImportString(importDeclaration.getNameAsString());	
//			}
//		}
//		
//	}

	private CompilationUnit getPersistenceClassClassCompilationUnit(String qualifiedName) throws FileNotFoundException {
		String implPath = this.serviceRoot + File.separator + this.servicesMetaData.getApiCamelCaseName()
				+ File.separator + this.servicesMetaData.getServiceLayout().getSrc() + File.separator
				+ qualifiedName.replaceAll("\\.", "/") + ".java";

		File componentSourceFile = new File(implPath);
		return JavaParser.parse(componentSourceFile);
	}

	private void handleAddToTreasury(RoundEnvironment roundEnv) {
		for (Element addToTreasuryElement : roundEnv.getElementsAnnotatedWith(AddToTreasury.class)) {
			if (addToTreasuryElement instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) addToTreasuryElement;
				processAddToTreasuryAnnotation(typeElement);
			}
		}
	}

	private void processAddToTreasuryAnnotation(TypeElement typeElement) {
		AddToTreasury addToTreasury = typeElement.getAnnotation(AddToTreasury.class);
		AddToTreasuryMetaData addToTreasuryMetaData = new AddToTreasuryMetaData();
		PackageElement packageElement = this.elementUtils.getPackageOf(typeElement);
		addToTreasuryMetaData.setInterfaceRequired(addToTreasury.interfaceRequired());
		if (addToTreasury.interfaceRequired()) {
			List<? extends TypeMirror> superInterface = typeElement.getInterfaces();
			if (superInterface != null && superInterface.size() >= 1) {
				/* Expecting only one Interface for objects annotated with add To Treasury */
				// TypeElement element=superInterface.get(0);
				String fullyQualifiedName = superInterface.get(0).toString();
				String simpleName = RedsGeneratorApiUtils.getSimpleNameFormPackage(fullyQualifiedName);
				addToTreasuryMetaData.setInterfaceName(simpleName);
				addToTreasuryMetaData.setInterfacePackage(fullyQualifiedName);

			}
		}

		addToTreasuryMetaData.setImplPackage(packageElement.getQualifiedName().toString());
		addToTreasuryMetaData.setImplName(typeElement.getSimpleName().toString());
		addToTreasuryMetaData.setSingleton(addToTreasury.singleton());
		this.servicesMetaData.getAddToTreasuryObjects().add(addToTreasuryMetaData);

	}

	private void processServiceAnnotation(TypeElement typeElement) throws IOException, RedsutilsException {
		Service service = typeElement.getAnnotation(Service.class);
		String serviceType = service.type();
		String author = "REDS GENERATOR";
		String name = service.name();

		// TODO Need to find a way to get version. Its not fair to collect from code
		String version = "";

		String location = this.serviceRoot.toString();
		ServiceLayout layout = new ServiceLayout();
		this.servicesMetaData = new ServicesMetaData(author, name, location, layout, version);

	}

	private void initServiceRootPath(TypeElement typeElement, boolean isApi)
			throws IOException, RedsXmlException, RedsannotationsprocessorException {
		if (this.serviceRoot != null) {
			/* Already Initialized */
			return;
		}
		String parentName = "";
		String projectPath = APUtils.getProjectPath(filer, isApi);
		String pathString = projectPath + File.separator + new ServiceLayout().getSrcResources() + "/service.xml";//
		Path path = new File(pathString).toPath();
		if (path.toFile().exists()) {
			RedsXmlManager<ServiceInfo> serviceMetaDataManager = new RedsXmlManagerImpl<ServiceInfo>(
					path.getParent().toAbsolutePath().toString());
			this.serviceInfo = serviceMetaDataManager.read(new ServiceInfo(), ServiceInfo.class);

			parentName = new File(projectPath).getParentFile().getName();

		}

		if (null != typeElement) {
			Service service = typeElement.getAnnotation(Service.class);
			parentName = service.root();
		}
		System.out.println(" Parent Name Found  " + parentName);
		printNotice("Parent Name Found  " + parentName);

		this.serviceRoot = getServiceRoot(path, parentName);

		if (this.serviceRoot == null) {
			printError("Root Service NOT FOUND. Please check service annotation's root field " + this.serviceRoot);
			throw new RedsannotationsprocessorException(RedsannotationsprocessorErrorCodes.errorCode(1),
					"Service Root Folder Not Found. Please Check Service Annotation's root field.");
		} else {
			printNotice("Root Service Found: " + this.serviceRoot);
		}

	}

	private void saveServiceMetaData(Path serviceRoot) throws RedsutilsException {
		String implemenatationGenPath = getSrcMainPath(serviceRoot) + RedsGeneratorConstants.AUTO_GEN_JSON;
		Json.toJsonFile(implemenatationGenPath, this.servicesMetaData);
	}

	private String getSrcMainPath(Path serviceRoot) {
		String serviceName = "";
		if (this.serviceInfo != null) {
			serviceName = RedsStringUtils.toCamelCase(this.serviceInfo.getName(), true);
		} else {
			serviceName = this.servicesMetaData.getCamelCaseName();
		}
		String redServicePath = serviceRoot + File.separator + serviceName + File.separator + "src" + File.separator
				+ "main" + File.separator + "java" + File.separator;
		return redServicePath;
	}

	private Path getServiceRoot(Path childPath, String parentName) {

		Path genPath = RedsFileUtils.recursivelyGetParent(childPath, parentName);
		return genPath;
	}

	@Override
	public Set<String> getSupportedOptions() {
		return Collections.singleton("org.gradle.annotation.processing.aggregating");
	}

}
