package com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager;

import com.reds.redsgenerator.api.AbstractServiceMetaDataWrapper;

public class ConfigManagerMetaDataWrapper extends AbstractServiceMetaDataWrapper {
	private String registerMethodName = "register";
	private ConfigManagerMetaData configMetaData;

	public ConfigManagerMetaDataWrapper(ConfigManagerMetaData metaData) {
		super(metaData.getParentServiceMetaData());
		this.configMetaData=metaData;
	}

	public String getRegisterMethodName() {
		return registerMethodName;
	}

	public void setRegisterMethodName(String registerMethodName) {
		this.registerMethodName = registerMethodName;
	}

	public ConfigManagerMetaData getConfigMetaData() {
		return configMetaData;
	}

	public void setConfigMetaData(ConfigManagerMetaData configMetaData) {
		this.configMetaData = configMetaData;
	}

	
}
