package com.reds.generator.redsannotationsprocessor.generator.cache;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.generator.redsannotationsprocessor.generator.service.ServiceImpleGenerator;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.cache.CacheMetaData;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class CacheManagerGenerator {

	private String cacheManagementServiceName = "CacheManagement";
	private String cacheManagementConfig = "CacheConfiguration";
	private String cacheManagementConfigPackage = "com.reds.service.cachemanagementapi.cacheconfig.CacheConfiguration";
	private String cacheManagementServicePackage = "com.reds.service.cachemanagementapi.CacheManagement";

	private ServiceImpleGenerator serviceImplGenerator;

	public CacheManagerGenerator(ServiceImpleGenerator serviceImplGenerator) {
		this.serviceImplGenerator = serviceImplGenerator;

	}

	private ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper;

	public void generate(ServicesMetaData metaData) {
		try {
			this.serviceMetaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(metaData);

			if (serviceMetaDataWrapper.getMetaData().getCacheableMetaData() == null
					|| serviceMetaDataWrapper.getMetaData().getCacheableMetaData().getCacheMetaDatas().size() <= 0) {
				return;
			}

			String className = serviceMetaDataWrapper.getMetaData().getName()
					+ RedsGeneratorConstants.SERVICE_CACHE_MANAGER_POST;

			String cacheManagerPath = serviceMetaDataWrapper.getCacheManagerPath() + File.separator + className
					+ ".java";

			String packageName = serviceMetaDataWrapper.getCacheManagerPackage();

			CompilationUnit classUnit = getCacheManagerClassCompilationUnit(cacheManagerPath, className, packageName,
					this.serviceMetaDataWrapper.getTreasuryName(), this.serviceMetaDataWrapper.getConfigClassName());

			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				if (serviceMetaDataWrapper.getMetaData().getCacheableMetaData() != null) {
					if (serviceMetaDataWrapper.getMetaData().getCacheableMetaData().getCacheMetaDatas().size() > 0) {
						try {
							handleCache(classUnit, c,
									serviceMetaDataWrapper.getMetaData().getCacheableMetaData().getCacheMetaDatas());
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (RedsutilsException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}

			});

			String newSource = classUnit.toString();
			System.out.println(newSource);
			RedsFileUtils.write(cacheManagerPath, newSource);

			this.serviceImplGenerator.addCache(serviceMetaDataWrapper, cacheManagementServiceName,
					cacheManagementServicePackage);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void handleCache(CompilationUnit classUnit, ClassOrInterfaceDeclaration c,
			List<CacheMetaData> cacheMetaDatas) throws FileNotFoundException, RedsutilsException {

		if (!c.getFieldByName("config").isPresent()) {

			c.addPrivateField(serviceMetaDataWrapper.getConfigClassName(), "config");

			ImportDeclaration configImport = JavaParser
					.parseImport("import " + this.serviceMetaDataWrapper.getConfigPackage() + "."
							+ serviceMetaDataWrapper.getConfigClassName() + ";");

			classUnit.addImport(configImport);

		}

		StringBuilder initMethods = new StringBuilder();
		StringBuilder config = new StringBuilder();
		StringBuilder configInit = new StringBuilder();
		StringBuilder closeMethods = new StringBuilder();

		for (CacheMetaData metaData : cacheMetaDatas) {
			String name = metaData.getName();
			String objectPackage = metaData.getObjectName();
			String objectName = APUtils.getClassNameFromPackage(objectPackage);
			ImportDeclaration objectImport = JavaParser.parseImport("import " + objectPackage + ";");
			if (!classUnit.getImports().contains(objectImport)) {
				classUnit.addImport(objectImport);
			}

			if (!c.getFieldByName(name).isPresent()) {

				c.addPrivateField("Cache<" + objectName + ">", name);
				c.addMember(APUtils.getGetterMethod("Cache<" + objectName + ">", name));

				String template = new CacheTemplate().getInitCache();
				ST st = new ST(template);
				st.add("cacheNameCap", RedsStringUtils.capitalize(name));
				st.add("cacheName", name);
				st.add("errorCode", RedsStringUtils.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
						+ RedsGeneratorConstants.ERRORCODE_POSTFIX));
				String initMethod = st.render();
				c.addMember(JavaParser.parseBodyDeclaration(initMethod));

				ImportDeclaration exectionCode = JavaParser
						.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + "."
								+ RedsStringUtils.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
										+ RedsGeneratorConstants.ERRORCODE_POSTFIX)
								+ ";");
				if (!classUnit.getImports().contains(exectionCode)) {
					classUnit.getImports().add(exectionCode);
				}

				initMethods.append("init").append(RedsStringUtils.capitalize(name)).append("();\n");
				closeMethods.append("cacheManagement.removeCache(config.get").append(RedsStringUtils.capitalize(name))
						.append("Config());\n");

				config.append(name).append("Config,");

				configInit.append("this.").append(name).append("Config = new CacheConfiguration();\r\n" + "		this.")
						.append(name).append("Config.setName(\"")
						.append(this.serviceMetaDataWrapper.getMetaData().getName()).append(".").append(name)
						.append("\");");

			}

		}

		List<MethodDeclaration> methods = c.getMethodsByName("init");
		if (methods != null && methods.size() > 0 && !initMethods.toString().isEmpty()) {
			MethodDeclaration declaration = methods.get(0);
			if (declaration != null) {
				// declaration.removeBody();
				String blockStatement = "{" + initMethods.toString() + "}";
				// new CacheTemplate().getInit(this.serviceMetaDataWrapper.getTreasuryName(),
				// this.serviceMetaDataWrapper.getConfigClassName(), initMethods.toString());
				System.out.println(blockStatement);
				// declaration.setBody(JavaParser.parseBlock(blockStatement));

				if (!declaration.getBody().get().getStatements().contains(JavaParser.parseStatement(blockStatement))) {
					declaration.getBody().get().addStatement(JavaParser.parseStatement(blockStatement));
				}
			}

		}

		List<MethodDeclaration> close = c.getMethodsByName("close");
		if (close != null && close.size() > 0 && !closeMethods.toString().isEmpty()) {
			MethodDeclaration declaration = close.get(0);
			if (declaration != null) {
				// declaration.removeBody();
				String blockStatement = "{" + closeMethods.toString() + "}";
				System.out.println(blockStatement);
				// declaration.setBody(JavaParser.parseBlock(blockStatement));

				if (!declaration.getBody().get().getStatements().contains(JavaParser.parseStatement(blockStatement))) {
					declaration.getBody().get().addStatement(JavaParser.parseStatement(blockStatement));
				}

			}

		}

		addToConfig(config.toString(), configInit.toString());

		ImportDeclaration treasury = JavaParser.parseImport("import " + this.serviceMetaDataWrapper.getTreasuryPackage()
				+ "." + this.serviceMetaDataWrapper.getTreasuryName() + ";");
		if (!classUnit.getImports().contains(treasury)) {
			classUnit.getImports().add(treasury);
		}

	}

	private void addToConfig(String config, String configInit) throws FileNotFoundException, RedsutilsException {

		String className = serviceMetaDataWrapper.getConfigClassName();

		String configPath = serviceMetaDataWrapper.getConfigPackagePath() + File.separator + className + ".java";
		System.out.println(configPath + ">" + config + " >#> " + configInit);

		String sourcePath = configPath;
		File sourceFile = new File(sourcePath);
		CompilationUnit classUnit = JavaParser.parse(sourceFile);
		classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
			if (config != null && !config.isEmpty()) {
				String configFinal = APUtils.removeLastComma(config);
				String fields[] = configFinal.split(",");
				for (String field : fields) {

					if (!c.getFieldByName(field).isPresent()) {
						System.out.println("Field>" + field);
						c.addPrivateField(cacheManagementConfig, field);
						c.addMember(APUtils.getGetterMethod(cacheManagementConfig, field));
						c.addMember(APUtils.getSetterMethod(cacheManagementConfig, field));
					}

				}
			}

			List<ConstructorDeclaration> init = c.getConstructors();
			if (init == null || init.size() <= 0) {
				c.addConstructor(Modifier.PUBLIC);
				init = c.getConstructors();
			}
			for (ConstructorDeclaration declaration : init) {

				if (declaration != null && !configInit.isEmpty()) {
					String blockStatement = "{" + configInit + "}";

					if (!declaration.getBody().getStatements().contains(JavaParser.parseStatement(blockStatement))) {
						declaration.getBody().addStatement(JavaParser.parseBlock(blockStatement));
						System.out.println(blockStatement);
					}

				}
			}

		});

		ImportDeclaration configImport = JavaParser.parseImport("import " + cacheManagementConfigPackage + ";");
		if (!classUnit.getImports().contains(configImport)) {
			classUnit.getImports().add(configImport);
		}

		String newSource = classUnit.toString();
		System.out.println(newSource);
		RedsFileUtils.write(configPath, newSource);
	}

	private CompilationUnit getCacheManagerClassCompilationUnit(String cacheManagerPath, String className,
			String packageName, String treasury, String configName) throws RedsutilsException, FileNotFoundException {
		String sourcePath = cacheManagerPath;
		File sourceFile = new File(sourcePath);
		if (!sourceFile.exists()) {
			RedsFileUtils.write(sourcePath,
					APUtils.getCacheManagerStructure(className, packageName, treasury, configName));
		}
		return JavaParser.parse(sourceFile);
	}

}
