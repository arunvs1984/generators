package com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider;

import com.reds.redsgenerator.api.AbstractMetaDataWrapper;

public class ServiceProviderMetaDataWrapper extends AbstractMetaDataWrapper<ServiceProviderMetaData> {

	public ServiceProviderMetaDataWrapper(ServiceProviderMetaData metaData) {
		super(metaData);
	}

}
