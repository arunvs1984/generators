package com.reds.generator.redsannotationsprocessor.generator.metadata;

import java.util.ArrayList;
import java.util.List;

public class CoServiceInfo {
	
	private String serviceName;

	private List<CoServiceMethod> methods = null;

	public CoServiceInfo() {
		methods= new ArrayList<>();
	}

	public List<CoServiceMethod> getMethods() {
		return methods;
	}

	public void setMethods(List<CoServiceMethod> methods) {
		this.methods = methods;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
