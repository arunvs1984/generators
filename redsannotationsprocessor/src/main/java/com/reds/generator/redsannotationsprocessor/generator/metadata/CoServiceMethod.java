package com.reds.generator.redsannotationsprocessor.generator.metadata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CoServiceMethod {
	private String methodName;
	private String returnType;
	private List<CoServiceParameter> parameters;
	private Set<String> imports;

	public CoServiceMethod() {
		this.parameters = new ArrayList<>();
		this.imports = new HashSet<>();
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public List<CoServiceParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<CoServiceParameter> parameters) {
		this.parameters = parameters;
	}

	public Set<String> getImports() {
		return imports;
	}

	public void setImports(Set<String> imports) {
		this.imports = imports;
	}

}
