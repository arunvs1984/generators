package com.reds.generator.redsannotationsprocessor.generator.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

import org.stringtemplate.v4.ST;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MemberValuePair;
import com.github.javaparser.ast.expr.NormalAnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.reds.generator.redsannotations.Api;
import com.reds.generator.redsannotations.CoService;
import com.reds.generator.redsannotations.Required;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.generator.redsannotationsprocessor.generator.CoServiceTemplates;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceInfo;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceMethod;
import com.reds.generator.redsannotationsprocessor.generator.metadata.CoServiceParameter;
import com.reds.generator.redsannotationsprocessor.generator.persistence.DAOImplTemplates;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;
import com.reds.redsgenerator.api.services.persistence.QueriesMetaData;
import com.reds.redsgenerator.api.services.persistence.QueryMetaData;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class ServiceImpleGenerator {

	private String dataManagementServiceName = "DataManagement";
	private String dataManagementServicePackage = "com.reds.service.datamanagementapi.DataManagement";

	public ServiceImpleGenerator() {

	}

	private ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper;

	public void addCache(ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper, String cacheServiceName,
			String cacheServicePackage) {
		try {
			this.serviceMetaDataWrapper = serviceMetaDataWrapper;
			String serviceImplPath = APUtils.getServiceImplPath(serviceMetaDataWrapper) + ".java";
			CompilationUnit classUnit = APUtils.getServiceImplClassCompilationUnit(serviceImplPath);
			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				if (serviceMetaDataWrapper.getMetaData().getCacheableMetaData().getCacheMetaDatas().size() > 0) {
					handleCacheRequiredAnnotation(classUnit, c, cacheServiceName, cacheServicePackage);

					List<MethodDeclaration> methods = c.getMethodsByName("start");
					if (methods != null && methods.size() > 0) {
						MethodDeclaration declaration = methods.get(0);
						if (declaration != null) {
							String statement = this.serviceMetaDataWrapper.getMetaData().getName()
									+ "Treasury.open.take" + this.serviceMetaDataWrapper.getMetaData().getName()
									+ "CacheManager().init();";
							if (!declaration.getBody().get().getStatements()
									.contains(JavaParser.parseStatement(statement))) {
								declaration.getBody().get().addStatement(JavaParser.parseStatement(statement));
							}
						}

					}

					methods = c.getMethodsByName("stop");
					if (methods != null && methods.size() > 0) {
						MethodDeclaration declaration = methods.get(0);
						if (declaration != null) {
							String statement = this.serviceMetaDataWrapper.getMetaData().getName()
									+ "Treasury.open.take" + this.serviceMetaDataWrapper.getMetaData().getName()
									+ "CacheManager().close();";
							if (!declaration.getBody().get().getStatements()
									.contains(JavaParser.parseStatement(statement))) {
								declaration.getBody().get().addStatement(JavaParser.parseStatement(statement));
							}
						}

					}

				}

			});

			String newSource = classUnit.toString();
			System.out.println(newSource);
			RedsFileUtils.write(serviceImplPath, newSource);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addRequiredExposes(ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper) {
		try {
			this.serviceMetaDataWrapper = serviceMetaDataWrapper;
			String serviceImplPath = APUtils.getServiceImplPath(serviceMetaDataWrapper) + ".java";
			CompilationUnit classUnit = APUtils.getServiceImplClassCompilationUnit(serviceImplPath);
			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {

				if (serviceMetaDataWrapper.getMetaData().getPersistenceCapables().size() > 0) {
					handleRequiredAnnotation(classUnit, c);
					exposeCrud(serviceMetaDataWrapper, c, classUnit);
				}

				exposeAsWeb(serviceMetaDataWrapper, c, classUnit);
			});

			String newSource = classUnit.toString();
			RedsFileUtils.write(serviceImplPath, newSource);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void handleCacheRequiredAnnotation(CompilationUnit classUnit, ClassOrInterfaceDeclaration c,
			String cacheServiceName, String cacheServicePackage) {

		Optional<AnnotationExpr> annotation = c.getAnnotationByName(Required.class.getSimpleName());

		if (annotation.isPresent()) {
			AnnotationExpr annotationExpr = annotation.get();
			boolean cacheManagementAlreadyExist = false;
			if (annotationExpr != null) {

				SingleMemberAnnotationExpr singleMemberAnnotationExpr = annotationExpr.asSingleMemberAnnotationExpr();
				Expression memberValuePair = singleMemberAnnotationExpr.getMemberValue();
				List<Node> childNodes = memberValuePair.getChildNodes();
				for (Node node : childNodes) {
					NodeList<MemberValuePair> pairs = ((NormalAnnotationExpr) node).getPairs();
					for (MemberValuePair pair : pairs) {
						System.out.println(pair);
						System.out.println(">>#>>" + pair.getName());

						// TODO Need to check
						if (pair.getName().toString().indexOf(cacheServiceName) == -1) {
							cacheManagementAlreadyExist = true;
						}

					}
				}
				if (!cacheManagementAlreadyExist) {

					ImportDeclaration importDeclaration = new ImportDeclaration(CoService.class.getName(), false,
							false);
					classUnit.getImports().add(importDeclaration);
					importDeclaration = new ImportDeclaration(cacheServicePackage, false, false);
					classUnit.getImports().add(importDeclaration);
					System.out.println(">>#>>" + JavaParser.parseAnnotation("@CoService(name=\"" + cacheServiceName
							+ "\",version=\"1.0\",service = " + cacheServiceName + ".class)"));
					memberValuePair.asArrayInitializerExpr().getValues()
							.add(JavaParser.parseAnnotation("@CoService(name=\"" + cacheServiceName
									+ "\",version=\"1.0\",service = " + cacheServiceName + ".class)"));

				}
			}
		}
	}

	private void handleRequiredAnnotation(CompilationUnit classUnit, ClassOrInterfaceDeclaration c) {
		Optional<AnnotationExpr> annotation = c.getAnnotationByName(Required.class.getSimpleName());
		if (annotation.isPresent()) {
			AnnotationExpr annotationExpr = annotation.get();
			boolean dataManagmentAlreadyExist = false;
			if (annotationExpr != null) {
				SingleMemberAnnotationExpr singleMemberAnnotationExpr = annotationExpr.asSingleMemberAnnotationExpr();
				Expression memberValuePair = singleMemberAnnotationExpr.getMemberValue();
				List<Node> childNodes = memberValuePair.getChildNodes();
				for (Node node : childNodes) {
					NodeList<MemberValuePair> pairs = ((NormalAnnotationExpr) node).getPairs();
					for (MemberValuePair pair : pairs) {
						System.out.println(pair);
						System.out.println(pair.getName());
						// TODO Need to check
						if (pair.toString().indexOf(dataManagementServiceName) != -1) {
							dataManagmentAlreadyExist = true;
						}

					}
				}

				addCoServiceAnnotation(classUnit, dataManagmentAlreadyExist, memberValuePair,
						dataManagementServicePackage);
			}
		}
	}

	private void addCoServiceAnnotation(CompilationUnit classUnit, boolean alreadyExist, Expression memberValuePair,
			String dataManagementServicePackage) {
		if (!alreadyExist) {
			ImportDeclaration importDeclaration = new ImportDeclaration(CoService.class.getName(), false, false);
			classUnit.getImports().add(importDeclaration);
			importDeclaration = new ImportDeclaration(dataManagementServicePackage, false, false);
			classUnit.getImports().add(importDeclaration);
			memberValuePair.asArrayInitializerExpr().getValues().add(JavaParser.parseAnnotation("@CoService(name=\""
					+ dataManagementServiceName + "\",version=\"1.0\",service = DataManagement.class)"));

		}
	}

	

	

	public void exposeAsWeb(ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper,
			ClassOrInterfaceDeclaration c, CompilationUnit classUnit) {

		List<CoServiceMetaData> coServiceMetaDatas = serviceMetaDataWrapper.getMetaData().getCoServiceMetaDatas();
		for (CoServiceMetaData coServiceMetaData : coServiceMetaDatas) {
			if (coServiceMetaData.isExposeAsWeb() || coServiceMetaData.isExpose()) {
				String coServiceFullName = coServiceMetaData.getCoServiceClass();
				CoServiceInfo serviceInfo = APUtils.getCoServiceInfo(coServiceFullName, getClass());
				for (CoServiceMethod method : serviceInfo.getMethods()) {
					String methodName = method.getMethodName();
					if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {

						ST st = null;
						if (coServiceMetaData.isExposeAsWeb()) {
							String template = null;
							template = new CoServiceTemplates().getExposeWebTemplate();
							st = new ST(template);
							st.add("serviceApiSeparated", RedsStringUtils.splitCamelCaseStringAsString(methodName));

							if (method.getParameters() == null || method.getParameters().isEmpty()) {
								st.add("getOrpost", "@GET");
							} else {
								st.add("getOrpost", "@POST");
							}
							if (method.getReturnType() != null && !method.getReturnType().equals("void")) {
								st.add("returnValue", "Object result = ");
								st.add("returnResult", "result ");
							} else {
								st.add("returnValue", "");
								st.add("returnResult", methodName + " Success ");
							}

						} else if (coServiceMetaData.isExpose()) {
							String template = null;
							template = new CoServiceTemplates().getExposeTemplate();
							st = new ST(template);
							st.add("exception",
									RedsStringUtils.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
											+ RedsGeneratorConstants.EXCEPTION_POSTFIX));
							st.add("exceptionCode",
									RedsStringUtils.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
											+ RedsGeneratorConstants.ERRORCODE_POSTFIX));
							st.add("errorNumber", "11");

							if (method.getReturnType() != null && !method.getReturnType().equals("void")) {
								st.add("return", "return ");
							} else {
								st.add("return", "");
							}
							ImportDeclaration exection = JavaParser
									.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + "."
											+ RedsStringUtils
													.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
															+ RedsGeneratorConstants.EXCEPTION_POSTFIX)
											+ ";");
							if (!classUnit.getImports().contains(exection)) {
								classUnit.getImports().add(exection);
							}
							ImportDeclaration exectionCode = JavaParser
									.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + "."
											+ RedsStringUtils
													.capitalize(this.serviceMetaDataWrapper.getMetaData().getName()
															+ RedsGeneratorConstants.ERRORCODE_POSTFIX)
											+ ";");
							if (!classUnit.getImports().contains(exectionCode)) {
								classUnit.getImports().add(exectionCode);
							}
						} else {
							break;
						}
						st.add("serviceApi", methodName);
						StringBuilder builder = new StringBuilder();
						StringBuilder argumentBuilder = new StringBuilder();
						for (CoServiceParameter parameter : method.getParameters()) {
							builder.append(parameter.getType()).append(" ").append(parameter.getName()).append(",");
							argumentBuilder.append(parameter.getName()).append(",");
						}
						String argumentsWithType = APUtils.removeLastComma(builder.toString());
						String arguments = APUtils.removeLastComma(argumentBuilder.toString());
						st.add("argumentsWithType", argumentsWithType);
						if (coServiceMetaData.isExposeAsWeb()) {
							st.add("argumentsEntry", arguments != null && !arguments.isEmpty() ? arguments : "null");
						}
						st.add("arguments", arguments);
						st.add("serviceName", serviceInfo.getServiceName());
						st.add("treasuaryName", this.serviceMetaDataWrapper.getTreasuryName());
						st.add("returnType", method.getReturnType());
						st.add("tracker", this.serviceMetaDataWrapper.getTrackerName());
						System.out.println(st.render());
						c.addMember(JavaParser.parseBodyDeclaration(st.render()));
						for (String importString : method.getImports()) {
							if (importString.indexOf('.') < 0) {
								continue;
							}
							System.out.println("import " + importString + ";");
							ImportDeclaration imp = JavaParser.parseImport("import " + importString + ";");
							if (!classUnit.getImports().contains(imp)) {
								classUnit.getImports().add(imp);
							}
						}
					}
				}

				if (coServiceMetaData.isExposeAsWeb()) {

					ImportDeclaration operation = JavaParser
							.parseImport("import io.swagger.v3.oas.annotations.Operation;");
					if (!classUnit.getImports().contains(operation)) {
						classUnit.getImports().add(operation);
					}
					ImportDeclaration parameter = JavaParser
							.parseImport("import io.swagger.v3.oas.annotations.Parameter;");
					if (!classUnit.getImports().contains(parameter)) {
						classUnit.getImports().add(parameter);
					}

					ImportDeclaration apiResponse = JavaParser
							.parseImport("import io.swagger.v3.oas.annotations.responses.ApiResponse;");
					if (!classUnit.getImports().contains(apiResponse)) {
						classUnit.getImports().add(apiResponse);
					}

					ImportDeclaration consumes = JavaParser.parseImport("import javax.ws.rs.Consumes;");
					if (!classUnit.getImports().contains(consumes)) {
						classUnit.getImports().add(consumes);
					}
					ImportDeclaration status = JavaParser.parseImport("import javax.ws.rs.core.Response.Status;");
					if (!classUnit.getImports().contains(status)) {
						classUnit.getImports().add(status);
					}

					ImportDeclaration reponse = JavaParser.parseImport("import javax.ws.rs.core.Response;");
					if (!classUnit.getImports().contains(reponse)) {
						classUnit.getImports().add(reponse);
					}
					ImportDeclaration mediaType = JavaParser.parseImport("import javax.ws.rs.core.MediaType;");
					if (!classUnit.getImports().contains(mediaType)) {
						classUnit.getImports().add(mediaType);
					}
					ImportDeclaration produces = JavaParser.parseImport("import javax.ws.rs.Produces;");
					if (!classUnit.getImports().contains(produces)) {
						classUnit.getImports().add(produces);
					}
					ImportDeclaration get = JavaParser.parseImport("import javax.ws.rs.GET;");
					if (!classUnit.getImports().contains(get)) {
						classUnit.getImports().add(get);
					}
					ImportDeclaration post = JavaParser.parseImport("import javax.ws.rs.POST;");
					if (!classUnit.getImports().contains(post)) {
						classUnit.getImports().add(post);
					}
					ImportDeclaration path = JavaParser.parseImport("import javax.ws.rs.Path;");
					if (!classUnit.getImports().contains(path)) {
						classUnit.getImports().add(path);
					}
				}

			}
		}

	}

	public void exposeCrud(ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper,
			ClassOrInterfaceDeclaration c, CompilationUnit classUnit) {
		this.serviceMetaDataWrapper = serviceMetaDataWrapper;
		List<PersistenceCapableMetaData> persistenceCapables = serviceMetaDataWrapper.getMetaData()
				.getPersistenceCapables();
		boolean crudExposed = false;
		for (PersistenceCapableMetaData persistenceCapable : persistenceCapables) {
			if (!persistenceCapable.isDaoRequired()) {
				/* If No DAO Required. No Need to Expose CRUD */
				continue;
			}
			if (persistenceCapable.isExposeCrud()) {
				crudExposed = true;
				/* Save */
				addSaveMethod(c, persistenceCapable);
				/* Update */
				addUpdateMethod(c, persistenceCapable);
				/* Retrieve */
				addRetrieveMethod(c, persistenceCapable);
				/* Retrieve All */
				addRetrieveAllMethod(c, persistenceCapable);
				/* Delete */
				addDeleteMethod(c, persistenceCapable);
				/* Delete All */
				addDeleteAllMethod(c, persistenceCapable);

				/* All Query Methods */
				addQueryMethods(c, persistenceCapable);

				ImportDeclaration to = JavaParser
						.parseImport("import " + persistenceCapable.getQualifiedClassName() + ";");
				if (!classUnit.getImports().contains(to)) {
					classUnit.getImports().add(to);
				}

				ImportDeclaration daoImpl = JavaParser.parseImport("import " + persistenceCapable.getDoPackage() + "."
						+ persistenceCapable.getDaoImplName() + ";");
				if (!classUnit.getImports().contains(daoImpl)) {
					classUnit.getImports().add(daoImpl);
				}

			}
		}

		if (crudExposed) {
			if (!c.getFieldByName(RedsGeneratorConstants.CONNECTION_NAME).isPresent()) {
				c.addFieldWithInitializer("String", RedsGeneratorConstants.CONNECTION_NAME,
						JavaParser.parseExpression("DataManagementConstants.DEFAULT_CONNECTION"), Modifier.PRIVATE);

			}
			ImportDeclaration dataManagementConstant = new ImportDeclaration(
					RedsGeneratorConstants.DATA_MANAGEMENT_CONSTANT_PKG, false, false);
			if (!classUnit.getImports().contains(dataManagementConstant)) {
				classUnit.getImports().add(dataManagementConstant);
			}
			ImportDeclaration dataManagementException = new ImportDeclaration(
					RedsGeneratorConstants.DATA_MANAGEMENT_EXCEPTION_CONSTANT_PKG, false, false);
			if (!classUnit.getImports().contains(dataManagementException)) {
				classUnit.getImports().add(dataManagementException);
			}

		}

		ImportDeclaration list = new ImportDeclaration(List.class.getName(), false, false);
		if (!classUnit.getImports().contains(list)) {
			classUnit.getImports().add(list);
		}
		ImportDeclaration exection = JavaParser
				.parseImport("import " + this.serviceMetaDataWrapper.getExceptionPackage() + ".*;");
		if (!classUnit.getImports().contains(exection)) {
			classUnit.getImports().add(exection);
		}

	}

	private void addRetrieveMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.RETRIEVE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().retrieveToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("daoName", persistenceMetaData.getDaoImplName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);
		}
	}

	private void addRetrieveAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.RETRIEVE_ALL_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().retrieveAllToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("daoName", persistenceMetaData.getDaoImplName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);
		}
	}

	private void addDeleteAllMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.DELETE_ALL_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().deleteAllToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("daoName", persistenceMetaData.getDaoImplName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);
		}
	}

	private void addDeleteMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.DELETE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().deleteToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("daoName", persistenceMetaData.getDaoImplName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);
		}
	}

	private void addUpdateMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.UPDATE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().updateToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("saveFunctionName", RedsGeneratorConstants.SAVE_PREFIX + persistenceMetaData.getSimpleName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);
		}
	}

	private void addSaveMethod(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		String methodName = RedsGeneratorConstants.SAVE_PREFIX + persistenceMetaData.getSimpleName();
		if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
			String template = new DAOImplTemplates().saveToImplTemplate();
			ST st = new ST(template);
			String toName = persistenceMetaData.getSimpleName();
			st.add("toName", toName);
			st.add("functionName", methodName);
			st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
			st.add("daoName", persistenceMetaData.getDaoImplName());
			c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			c.getMethodsByName(methodName).get(0).addAnnotation(Override.class);

		}
	}

	private void addQueryMethods(ClassOrInterfaceDeclaration c, PersistenceCapableMetaData persistenceMetaData) {
		QueriesMetaData metaDatas = persistenceMetaData.getQueriesMetaData();
		if (null == metaDatas) {
			return;
		}
		for (QueryMetaData metaData : metaDatas.getQueries()) {
			String methodName = metaData.getName();
			if (!APUtils.isMethodAlreadyDeclaredByName(c, methodName)) {
				String template = null;
				if (metaData.isUnique()) {
					template = new DAOImplTemplates().queryUniqueImplTemplate();
				} else {
					template = new DAOImplTemplates().queryImplTemplate();
				}

				ST st = new ST(template);
				String toName = persistenceMetaData.getSimpleName();
				st.add("toName", toName);
				st.add("daoName", persistenceMetaData.getDaoImplName());
				st.add("functionName", methodName);
				st.add("serviceName", this.serviceMetaDataWrapper.getMetaData().getName());
				System.out.println(st.render());
				c.addMember(JavaParser.parseBodyDeclaration(st.render()));
			}

		}
	}
}
