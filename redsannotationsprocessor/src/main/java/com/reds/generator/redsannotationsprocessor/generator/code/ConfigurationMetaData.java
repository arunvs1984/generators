package com.reds.generator.redsannotationsprocessor.generator.code;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.layouts.ServiceLayout;

public class ConfigurationMetaData extends GeneratorMetaData {

	private String desc;
	private String id;
	private String packageName;

	public ConfigurationMetaData(String author, String name, String location) {
		super(author, name, location, new ServiceLayout());

	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPackageName() {

		return this.packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

}
