package com.reds.generator.redsannotationsprocessor.generator.metadata.serviceprovider;

import java.util.ArrayList;
import java.util.List;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.services.CoServiceMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public class ServiceProviderMetaData extends GeneratorMetaData {

	private ServicesMetaData parentServiceMetaData;

	private List<CoServiceMetaData> services = null;

	public ServiceProviderMetaData(ServicesMetaData parentServiceMetaData) {
		super(parentServiceMetaData.getAuthor(), parentServiceMetaData.getName(), parentServiceMetaData.getLocation(),
				parentServiceMetaData.getLayout());		
		this.services = new ArrayList<>();
	}

	

	public ServicesMetaData getParentServiceMetaData() {
		return parentServiceMetaData;
	}

	public void setParentServiceMetaData(ServicesMetaData parentServiceMetaData) {
		this.parentServiceMetaData = parentServiceMetaData;
	}



	public List<CoServiceMetaData> getServices() {
		return services;
	}



	public void setServices(List<CoServiceMetaData> services) {
		this.services = services;
	}

}
