package com.reds.generator.redsannotationsprocessor.generator.code.coservice;

import java.io.File;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.reds.generator.redsannotationsprocessor.APUtils;
import com.reds.generator.redsannotationsprocessor.ServiceInfo;
import com.reds.generator.redsannotationsprocessor.generator.code.DependencyMetaData;
import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.xml.RedsXmlManager;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.library.redsutils.json.Json;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.core.services.ServiceMetaDataWrapper;

public class AddCoserviceAnnotation {

	

	public ServiceInfo getServiceInfo(String projectPath) {
		String pathString = projectPath + File.separator + new ServiceLayout().getSrcResources() + "/service.xml";
		Path path = new File(pathString).toPath();
		if (path.toFile().exists()) {
			RedsXmlManager<ServiceInfo> serviceMetaDataManager = new RedsXmlManagerImpl<ServiceInfo>(
					path.getParent().toAbsolutePath().toString());
			try {
				return serviceMetaDataManager.read(new ServiceInfo(), ServiceInfo.class);
			} catch (RedsXmlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	public Set<ServicesMetaData> getAllServiceMetaData(String location) {
		Set<ServicesMetaData> serviceMetaDatas = new HashSet<ServicesMetaData>();
		try {
			List<Path> paths = RedsFileUtils.getAllFiles(location, RedsGeneratorConstants.AUTO_GEN_JSON);
			if (paths != null) {
				for (Path path : paths) {
					String text = RedsFileUtils.readFileFully(path);

					serviceMetaDatas.add(Json.fromJson(text, ServicesMetaData.class));
				}

			}
		} catch (RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serviceMetaDatas;
	}

	public void addCoService(DependencyMetaData dependencyMetaData) {
		try {
			ServiceLayout layout = new ServiceLayout();
			String location = dependencyMetaData.getLocation() + File.separator + layout.getSrc() + File.separator
					+ RedsGeneratorConstants.AUTO_GEN_JSON;
			if (!new File(location).exists()) {
				System.out.println("No Auto Json Found. May be API");
			}
			ServicesMetaData servicesMetaData = Json.fromJsonFile(location, ServicesMetaData.class);
			if (servicesMetaData == null) {
				/* No need to do with API */
				return;
			}

			String serviceName = dependencyMetaData.getName();
			String packageName = dependencyMetaData.getPackageName();
			String version = dependencyMetaData.getVersion();
			boolean exposeWeb = dependencyMetaData.isExposeAsWeb();
			boolean expose = dependencyMetaData.isExpose();
			System.out.println("Adding Required Annotation " + dependencyMetaData);
			ServiceMetaDataWrapper<ServicesMetaData> serviceMetaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(
					servicesMetaData);
			System.out.println("ServiceMetaDataWrapper" + serviceMetaDataWrapper.toString());
			String serviceImplPath = APUtils.getServiceImplPath(serviceMetaDataWrapper) + ".java";
			System.out.println("Adding Required Annotation to " + serviceImplPath);
			CompilationUnit classUnit = APUtils.getServiceImplClassCompilationUnit(serviceImplPath);
			classUnit.findAll(ClassOrInterfaceDeclaration.class).stream().forEach(c -> {
				APUtils.handleRequiredAnnotation(classUnit, c, serviceName, packageName, version, exposeWeb, expose);
			});

			String newSource = classUnit.toString();
			System.out.println(newSource);
			RedsFileUtils.write(serviceImplPath, newSource);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
