package com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager;

import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public class ConfigManagerLayout extends AbstractProjectLayout {
	public ConfigManagerLayout(String rootPackage) {
		setRootPackage(rootPackage);
	}

}
