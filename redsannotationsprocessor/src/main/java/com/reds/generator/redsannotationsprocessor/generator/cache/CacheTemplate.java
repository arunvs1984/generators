package com.reds.generator.redsannotationsprocessor.generator.cache;

import org.stringtemplate.v4.ST;

public class CacheTemplate {

	/**
	 * cacheNameCap
	 * cacheName
	 * errorCode
	 * @return
	 */
	public String getInitCache() {
		String template = "private void init<cacheNameCap>() throws ServiceException {\r\n" + "		try {\r\n"
				+ "			this.<cacheName> = cacheManagement.createBasicCache(config.get<cacheNameCap>Config());\r\n"
				+ "		} catch (CacheManagementException e) {\r\n"
				+ "			throw new ServiceException(<errorCode>.errorCode(12), \"Failed to init<cacheNameCap>\", e);\r\n"
				+ "		}\r\n" + "	}";
		return template;
	}
	
	/**
	 * treasury
	 * configName
	 * @return
	 */
	public String getInit(String treasury,String configName,String initCache) {
		String template = "{\r\n" + 
				"			this.config = <treasury>.open.take<configName>();\r\n" + 
				"			super.initCacheManagement(<treasury>.open.accessCacheManagement());\r\n" + 
				"\r\n" + 
				"			<initCache>\r\n" + 
				"		}";
		ST st = new ST(template);
		st.add("treasury", treasury);
		st.add("configName", configName);
		st.add("initCache", initCache);		
		return st.render();
	}
}
