package com.reds.generator.redsannotationsprocessor.generator.code;

import java.io.File;

import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;

public class AddDependencyGenerator {

	public void addDependecy(DependencyMetaData metaData) {
		try {
			String location = metaData.getLocation()+ File.separator + "build.gradle";
			String depend = metaData.getDependency();
			if (depend == null) {
				depend = metaData.getPackageName() + ":" + metaData.getName() + ":" + metaData.getVersion();
			}
			String dependency = "  implementation '" + depend + "'\n}";
			if(metaData.isSameProject()) {
				dependency="  implementation project(':"+metaData.getApiName()+"')\n}";
			}
			
			
			System.out.println("Adding dependency on "+location+" > "+dependency);
			RedsFileUtils.replaceFilesLastContent(location, dependency);
		} catch (RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
