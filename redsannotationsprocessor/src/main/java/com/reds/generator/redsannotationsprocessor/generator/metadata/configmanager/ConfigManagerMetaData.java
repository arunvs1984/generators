package com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager;

import java.util.ArrayList;
import java.util.List;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public class ConfigManagerMetaData extends GeneratorMetaData {

	private ServicesMetaData parentServiceMetaData;

	private List<ConfigMetaData> configs = null;

	public ConfigManagerMetaData(ServicesMetaData parentServiceMetaData) {
		super(parentServiceMetaData.getAuthor(), parentServiceMetaData.getName(), parentServiceMetaData.getLocation(),
				parentServiceMetaData.getLayout());
		//setName(parentServiceMetaData.getName());
		this.configs = new ArrayList<>();
		this.parentServiceMetaData=parentServiceMetaData;
	}

	public List<ConfigMetaData> getConfigs() {
		return configs;
	}

	public void setConfigs(List<ConfigMetaData> configs) {
		this.configs = configs;
	}

	public ServicesMetaData getParentServiceMetaData() {
		return parentServiceMetaData;
	}

	public void setParentServiceMetaData(ServicesMetaData parentServiceMetaData) {
		this.parentServiceMetaData = parentServiceMetaData;
	}

}
