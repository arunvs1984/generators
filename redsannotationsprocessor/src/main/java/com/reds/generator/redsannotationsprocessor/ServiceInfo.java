package com.reds.generator.redsannotationsprocessor;

import java.util.Date;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.to.PlatformTO;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.platform.redsplatformcommons.inject.RedsplatformcommonsTreasury;
import com.reds.platform.redsplatformcommons.service.ServiceType;

public class ServiceInfo implements PlatformTO {

	private ServiceType type;
	private String name;
	private String version;
	private String implementation;
	private boolean managedLib;
	private String ctxPath;
	private String warName;
	private String category;
	private String description;
	private String icon;
	private String owner;
	private Date releaseDate;

	public boolean isManagedLib() {
		return managedLib;
	}

	public void setManagedLib(boolean managedLib) {
		this.managedLib = managedLib;
	}

	public ServiceType getType() {
		return type;
	}

	public void setType(ServiceType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String getId() {
		return RedsplatformcommonsTreasury.open.takePlatformLayout().getServicMetaDataName();
	}

	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}

	public static void main(String[] args) throws RedsXmlException {
		RedsXmlManagerImpl<ServiceInfo> xml = new RedsXmlManagerImpl<>("./");
		ServiceInfo metaData = new ServiceInfo();
		metaData.setName("redsdateprovider");
		metaData.setVersion("1.0");
		metaData.setType(ServiceType.PLATFORM);
		metaData.setImplementation("");
		xml.write(metaData);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(">").append(type).append(":").append(name).append(":").append(version).append("\n");
		return builder.toString();
	}

	public String getCtxPath() {
		return ctxPath;
	}

	public void setCtxPath(String ctxPath) {
		this.ctxPath = ctxPath;
	}

	public String getWarName() {
		return warName;
	}

	public void setWarName(String warName) {
		this.warName = warName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

}
