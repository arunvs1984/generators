package com.reds.generator.redsannotationsprocessor.generator;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;

public class TestJavaParser {
	
	public static void main(String[] args) {
		CompilationUnit compilationUnit = new CompilationUnit();
		compilationUnit.findAll(ClassOrInterfaceDeclaration.class)
		.stream()
        .filter(c -> !c.isInterface()
                && c.isAbstract()
                && !c.getNameAsString().startsWith("Abstract"))
        .forEach(c -> {
            String oldName = c.getNameAsString();
            String newName = "Abstract" + oldName;
            System.out.println("Renaming class " + oldName + " into " + newName);
            c.setName(newName);
        });
	}

}
