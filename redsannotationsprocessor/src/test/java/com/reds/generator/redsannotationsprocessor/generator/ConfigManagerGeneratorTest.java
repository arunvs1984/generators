package com.reds.generator.redsannotationsprocessor.generator;

import org.testng.annotations.Test;

import com.reds.generator.redsannotationsprocessor.generator.metadata.component.ComponentMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.component.DataObjectMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigManagerMetaData;
import com.reds.generator.redsannotationsprocessor.generator.metadata.configmanager.ConfigMetaData;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public class ConfigManagerGeneratorTest {

	@Test
	public void generate() {
		ConfigManagerGenerator generator = new ConfigManagerGenerator(null,null);		
		ServiceLayout layout= new ServiceLayout();		
		String location="D:\\reds\\redsworkspace\\redsservices\\redsdateproviderservice\\src\\redsgenerated\\java\\";
		ServicesMetaData parentServiceMetaData= new ServicesMetaData("SHINEED", "redsdateproviderservice",
				location, layout,"");			
		ConfigManagerMetaData metaData = new ConfigManagerMetaData(parentServiceMetaData);
		ConfigMetaData config1 = new ConfigMetaData("MyNewConfig",location);
		metaData.getConfigs().add(config1);
		ConfigMetaData config2 = new ConfigMetaData("RedsdateproviderserviceConfig",location);
		metaData.getConfigs().add(config2);
		generator.generate(metaData);
		
		ComponentGenerator generatorComponent = new ComponentGenerator(null,null);				
		ComponentMetaData compmetaData = new ComponentMetaData(parentServiceMetaData);
		compmetaData.getDataObjects().add(new DataObjectMetaData("MyNewConfig",location));
			
		generatorComponent.generate(compmetaData);
		
		
		
	}
	
}
