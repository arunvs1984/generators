package com.reds.generator.redsannotationsprocessor.inject;

import dagger.internal.Preconditions;
import javax.annotation.processing.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerRedsannotationsprocessorComponent
    implements RedsannotationsprocessorComponent {
  private DaggerRedsannotationsprocessorComponent(Builder builder) {}

  public static Builder builder() {
    return new Builder();
  }

  public static RedsannotationsprocessorComponent create() {
    return new Builder().build();
  }

  public static final class Builder {
    private Builder() {}

    public RedsannotationsprocessorComponent build() {
      return new DaggerRedsannotationsprocessorComponent(this);
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder redsannotationsprocessorModule(
        RedsannotationsprocessorModule redsannotationsprocessorModule) {
      Preconditions.checkNotNull(redsannotationsprocessorModule);
      return this;
    }
  }
}
