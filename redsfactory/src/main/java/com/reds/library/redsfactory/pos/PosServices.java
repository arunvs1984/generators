package com.reds.library.redsfactory.pos;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.library.redsfactory.ServiceRootFactory;
import com.reds.library.redsfactory.WebServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class PosServices {
	public static void main(String[] args) throws RedsGeneratorException {
		//posWebGenerator1();
		posRoot();
		//posWebGenerator();
//		genService("Sales");
//		genService("Purchase");
//		genService("Stock");
//		genService("UserManagement");
//		genService("Supplier");
//		genService("Company");
//		genService("Estimate");
//		genService("Settings");
//		genService("Customer");
//		genService("Backup");
//		genService("BasicReports");
		
	}
	
	private static void posRoot() throws RedsGeneratorException {

		new ServiceRootFactory("SHINEED BASHEER", "Pos1", "C:\\algos\\", "1.0").generate();
	}
	
	private static void posWebGenerator1() throws RedsGeneratorException {

		WebServiceFactory factory = new WebServiceFactory("SHINEED BASHEER", "PosWebTest", "C:\\algos\\pos", "1.0");

		factory.getMetaData().setCtxPath("pos1");

		factory.generate();
	}

	private static void posWebGenerator() throws RedsGeneratorException {

		WebServiceFactory factory = new WebServiceFactory("SHINEED BASHEER", "PosWeb", "C:\\\\algos\\\\pos", "1.0");

		factory.getMetaData().setCtxPath("pos");

		factory.generate();
	}
	private static void genService(String name) throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", name, "C:\\algos\\pos", "1.0");
		factory.setType(ServiceType.BUSINESS);
		factory.generate();
	}
	
	private static void purchaseService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "purchaseService", "C:\\algos\\pos", "1.0");
		factory.setType(ServiceType.BUSINESS);
		factory.generate();
	}
	
	private static void salesService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "salesService", "C:\\algos\\pos", "1.0");
		factory.setType(ServiceType.BUSINESS);
		factory.generate();
	}

	private static void startService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "PosStarted", "C:\\algos\\pos", "1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

	private static void stockService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "StockService", "C:\\algos\\pos", "1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

	private static void reportingServerExecutorService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "TradeReports", "D:\\reds\\actualgit\\algols",
				"1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

	private static void strategyExecutorService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "StrategyExecutor",
				"D:\\reds\\actualgit\\algols", "1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

	private static void nseOrderGatewayService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "NseOrder", "D:\\reds\\actualgit\\algols",
				"1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

	private static void nseMarketDataGatewayService() throws RedsGeneratorException {
		ServiceFactory factory = new ServiceFactory("SHINEED BASHEER", "NseMarketData", "D:\\reds\\actualgit\\algols",
				"1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}
}
