package com.reds.library.redsfactory;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.core.AbstractRedsGenerator;

public abstract class AbstractRedsFactory<T extends GeneratorMetaData> {
	protected AbstractRedsGenerator<T> generator;
	protected T metaData = null;

	public AbstractRedsFactory(AbstractRedsGenerator<T> generator, T metaData) {
		super();
		this.generator = generator;
		this.metaData = metaData;
	}

	public abstract void generate() throws RedsGeneratorException;

	public AbstractRedsGenerator<T> getGenerator() {
		return generator;
	}

	public T getMetaData() {
		return metaData;
	}

}
