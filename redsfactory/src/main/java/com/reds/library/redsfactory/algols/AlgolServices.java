package com.reds.library.redsfactory.algols;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class AlgolServices {
	public static void main(String[] args) throws RedsGeneratorException {
		//uiInterfaceServerExecutorService();
		//reportingServerExecutorService();
		//strategyExecutorService();
		//nseMarketDataGatewayService();
		//nseOrderGatewayService();
	}
	
	private static void uiInterfaceServerExecutorService() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "UserInterface", 
						"D:\\reds\\actualgit\\algols","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void reportingServerExecutorService() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "TradeReports", 
						"D:\\reds\\actualgit\\algols","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void strategyExecutorService() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "StrategyExecutor", 
						"D:\\reds\\actualgit\\algols","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void nseOrderGatewayService() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "NseOrder", 
						"D:\\reds\\actualgit\\algols","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void nseMarketDataGatewayService() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "NseMarketData", 
						"D:\\reds\\actualgit\\algols","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
}
