package com.reds.library.redsfactory.hexa;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class HexaETimsServices {

	public static void main(String[] args) throws RedsGeneratorException {

		service();
	}

	private static void service() throws RedsGeneratorException {
		ServiceFactory factory 
		= new ServiceFactory("SHINEED BASHEER", "EtimsBatch"
				, "G:\\hexa\\etimsbatch",
				"1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

}
