package com.reds.library.redsfactory.library;

import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class TestLibrary {

	public static void main(String[] args) throws RedsGeneratorException {
		LibraryFactory factory = 
				new LibraryFactory("Shineed", "testLibrary", 
						"D:\\reds\\gitlocalrepo\\redsplatformlibraries\\redslibraries");
		factory.generate();
	}

}
