package com.reds.library.redsfactory.platform;

import com.reds.library.redsfactory.library.LibraryFactory;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class PlatformCommonsGenerator {
	public static void main(String[] args) throws RedsGeneratorException {
		LibraryFactory factory = 
				new LibraryFactory("SHINEED BASHEER", "redsplatformcommons", 
						"D:\\reds\\redsworkspace\\redsplatform");
		factory.generate();
	}
}
