package com.reds.library.redsfactory.integration;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.library.redsfactory.WebServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class TelecomIntegrationServiceGen {
	public static void main(String[] args) throws RedsGeneratorException {
		//integrationStarter();
		//integrationApiGenerator();
		//integrationMockGenerator();
		mpesaMockGenerator();
		//uiInterfaceServerExecutorService();
		//reportingServerExecutorService();
		//strategyExecutorService();
		//nseMarketDataGatewayService();
		//nseOrderGatewayService();
	}
	
	private static void integrationCore() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "IntegrationCore", 
						"C:\\Users\\shine\\Integration\\telecomintegration","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void integrationStarter() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "IntegrationStarter", 
						"C:\\Users\\shine\\Integration\\telecomintegration","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void integrationApiGenerator() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "IntegrationApiGenerator", 
						"C:\\Users\\shine\\Integration\\telecomintegration","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	
	private static void mpesaMockGenerator() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "MpesaMock", 
						"C:\\Users\\shine\\Integration\\telecomintegration","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}

	
	
	private static void integrationMockGenerator() throws RedsGeneratorException {
		
		WebServiceFactory factory = new WebServiceFactory("SHINEED BASHEER", "MockVodacomMiddleware",
				"C:\\Users\\shine\\Integration\\telecomintegration", "1.0");


		factory.getMetaData().setCtxPath("mock");

		
		
		factory.generate();
	}
	
	
	
	
}
