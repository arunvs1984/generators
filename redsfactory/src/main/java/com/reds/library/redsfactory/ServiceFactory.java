package com.reds.library.redsfactory;

import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.core.services.ServiceGenerator;

public class ServiceFactory extends AbstractRedsFactory<ServicesMetaData> {
	public ServiceFactory(String author, String name, String location, String version) {
		super(new ServiceGenerator<ServicesMetaData>(),
				new ServicesMetaData(author, name, location, new ServiceLayout(), version));
	}

	@Override
	public void generate() throws RedsGeneratorException {
		generator.generate(metaData);

	}
	
	public void setType(ServiceType type) {
			super.metaData.setType(type);
	}

}
