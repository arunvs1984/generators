package com.reds.library.redsfactory.esb;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class ESBServices {
	public static void main(String[] args) throws RedsGeneratorException {
		
		//esbEngine();
		//togocelesb();
		//stcesb();
		testESB();
	}
	//C:\Users\shine\Integration\togocelesb\
	
	private static void testESB() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "TashiCellCore", 
						"G:\\esb\\tashicell","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void stcesb() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "STCEsbCore", 
						"G:\\esb\\stcesb","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}

	private static void togocelesb() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "TogocelEsbCore", 
						"C:\\Users\\shine\\Integration\\togocelesb","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	private static void esbEngine() throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "EsbEngine", 
						"D:\\reds\\actualgit\\redsservices","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
	
	
}
