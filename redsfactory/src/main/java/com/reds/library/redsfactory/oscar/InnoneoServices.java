package com.reds.library.redsfactory.oscar;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class InnoneoServices {

	public static void main(String[] args) throws RedsGeneratorException {

		innoneoEmr();
	}

	private static void innoneoEmr() throws RedsGeneratorException {
		ServiceFactory factory 
		= new ServiceFactory("SHINEED BASHEER", "InnoneoEMR"
				, "G:\\health\\innoneo",
				"1.0");
		// factory.generate();

		factory.setType(ServiceType.BUSINESS);

		factory.generate();
	}

}
