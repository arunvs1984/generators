package com.reds.library.redsfactory;

import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.WebServiceMetaData;
import com.reds.redsgenerator.core.services.WebServiceGenerator;

public class WebServiceFactory extends AbstractRedsFactory<WebServiceMetaData> {

	public WebServiceFactory(String author, String name, String location, String version) {
		super(new WebServiceGenerator(),
				new WebServiceMetaData(author, name, location, new ServiceLayout(), version));

	}

	@Override
	public void generate() throws RedsGeneratorException {
		generator.generate(metaData);

	}

}
