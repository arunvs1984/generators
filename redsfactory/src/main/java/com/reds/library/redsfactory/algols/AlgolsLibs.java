package com.reds.library.redsfactory.algols;

import com.reds.library.redsfactory.library.LibraryFactory;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class AlgolsLibs {
	public static void main(String[] args) throws RedsGeneratorException {
		//nseOrderGateWay();
		algolsCommons();
	}

	private static void nseOrderGateWay() throws RedsGeneratorException {
		LibraryFactory factory = 
				new LibraryFactory("SHINEED BASHEER", "nsegatewaylib", 
						"D:\\reds\\actualgit\\algols");
		factory.generate();
	}
	
	private static void algolsCommons() throws RedsGeneratorException {
		LibraryFactory factory = 
				new LibraryFactory("SHINEED BASHEER", "algolscommonslib", 
						"D:\\reds\\actualgit\\algols");
		factory.generate();
	}
}


