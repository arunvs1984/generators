package com.reds.library.redsfactory.library;

import com.reds.library.redsfactory.AbstractRedsFactory;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.LibraryLayout;
import com.reds.redsgenerator.api.library.LibraryMetaData;
import com.reds.redsgenerator.core.library.LibraryGenerator;

public class LibraryFactory extends AbstractRedsFactory<LibraryMetaData> {
	public LibraryFactory(String author, String name, String location) {
		super(new LibraryGenerator(), 
				new LibraryMetaData(author, name, location, new LibraryLayout()));
	}

	@Override
	public void generate() throws RedsGeneratorException {
		generator.generate(metaData);

	}

}
