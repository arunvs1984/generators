package com.reds.library.redsfactory.platformservices.general;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class OcrService {
	public static void main(String[] args) throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "OpticalCharacterReader", 
						"D:\\reds\\actualgit\\redsservices","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
}
