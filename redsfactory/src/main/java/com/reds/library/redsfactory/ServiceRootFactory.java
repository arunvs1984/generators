package com.reds.library.redsfactory;

import java.util.List;

import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceMetaData;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.core.services.ServiceRootGenerator;

public class ServiceRootFactory extends AbstractRedsFactory<ServicesMetaData> {

	public ServiceRootFactory(String author, String name, String location, String version) {
		super(new ServiceRootGenerator<ServicesMetaData>(),
				new ServicesMetaData(author, name, location, new ServiceLayout(), version));

	}

	@Override
	public void generate() throws RedsGeneratorException {
		generator.generate(metaData);
	}

	public void deploy(String zipLocation, String destinationLocation) throws RedsGeneratorException {
		
		ServiceRootGenerator<ServicesMetaData> gen=	(ServiceRootGenerator<ServicesMetaData>) generator;
		gen.deploy(zipLocation, destinationLocation);
	}
	
	public List<ServiceMetaData> getAllServicesMetaData(){
		ServiceRootGenerator<ServicesMetaData> gen=	(ServiceRootGenerator<ServicesMetaData>) generator;
		return gen.getAllRespoServices();
	}
	
	public List<ServiceInformation> getAllDeployed(String workspaceLocation){
		ServiceRootGenerator<ServicesMetaData> gen=	(ServiceRootGenerator<ServicesMetaData>) generator;
		return gen.getAllDeployedServices(workspaceLocation);
	}

}
