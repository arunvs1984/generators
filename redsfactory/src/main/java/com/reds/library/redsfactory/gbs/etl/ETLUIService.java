package com.reds.library.redsfactory.gbs.etl;

import com.reds.library.redsfactory.ServiceFactory;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public class ETLUIService {
	public static void main(String[] args) throws RedsGeneratorException {
		ServiceFactory factory = 
			new ServiceFactory("SHINEED BASHEER", "AdminUI", 
						"D:\\reds\\GBS\\etl","1.0");
		//factory.generate();
		
		factory.setType(ServiceType.BUSINESS);
		
		factory.generate();
	}
}
