package com.reds.redsgenerator.api.services;

import java.io.Serializable;

public class AddToTreasuryMetaData  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String implName;
	private String implPackage;
	private String interfaceName;
	private String interfacePackage;
	private boolean singleton;
	private boolean interfaceRequired;
	public String getImplName() {
		return implName;
	}
	public void setImplName(String implName) {
		this.implName = implName;
	}
	public String getImplPackage() {
		return implPackage;
	}
	public void setImplPackage(String implPackage) {
		this.implPackage = implPackage;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	public String getInterfacePackage() {
		return interfacePackage;
	}
	public void setInterfacePackage(String interfacePackage) {
		this.interfacePackage = interfacePackage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public boolean isSingleton() {
		return singleton;
	}
	public void setSingleton(boolean singleton) {
		this.singleton = singleton;
	}
	public boolean isInterfaceRequired() {
		return interfaceRequired;
	}
	public void setInterfaceRequired(boolean interfaceRequired) {
		this.interfaceRequired = interfaceRequired;
	}
	
}
