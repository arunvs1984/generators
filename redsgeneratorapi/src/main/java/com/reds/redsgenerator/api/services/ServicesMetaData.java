package com.reds.redsgenerator.api.services;

import java.util.ArrayList;
import java.util.List;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.platform.redsplatformcommons.service.ServiceType;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.cache.CacheableMetaData;
import com.reds.redsgenerator.api.services.persistence.PersistenceCapableMetaData;

public class ServicesMetaData extends GeneratorMetaData {
	private String apiName;
	private String apiCamelCaseName;
	/**
	 * Location of apiLocation
	 */
	private String apiLocation;
	
	private String version;
	
	private ServiceType type=ServiceType.BUSINESS;
	
	private List<AddToTreasuryMetaData> addToTreasuryObjects;
	private List<PersistenceCapableMetaData> persistenceCapables;
	private List<CoServiceMetaData> coServiceMetaDatas=null;
	private CacheableMetaData cacheableMetaData=null; 
	private String shortName;
	
	 
	
	public ServicesMetaData(String author,String name, String location, ServiceLayout layout,String version) {
		super(author,name, location, layout);		
		this.apiName=getName()+RedsGeneratorConstants.SERVICE_API_POSTFIX;
		this.apiCamelCaseName = getCamelCaseName()+RedsGeneratorConstants.SERVICE_API_POSTFIX;
		this.apiLocation=location;
		this.version = version;
		this.addToTreasuryObjects= new ArrayList<>();
		this.persistenceCapables = new ArrayList<>();
		this.coServiceMetaDatas = new ArrayList<>();
		this.cacheableMetaData= new CacheableMetaData();
		if(name.length()>3) {
			/*Tacking firsts three characters of the serviceName as ShotName*/
			this.shortName=(name.substring(0, 3)+RedsStringUtils.removeDotByUnderScore(version)).toUpperCase();	
		}
		
	}
	
	public ServiceLayout getServiceLayout() {
		return (ServiceLayout) super.getLayout();
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiLocation() {
		return apiLocation;
	}

	public String getVersion() {
		return this.version;
	}

	public String getApiCamelCaseName() {
		return apiCamelCaseName;
	}

	public ServiceType getType() {
		return type;
	}

	public List<AddToTreasuryMetaData> getAddToTreasuryObjects() {
		return addToTreasuryObjects;
	}

	public List<PersistenceCapableMetaData> getPersistenceCapables() {
		return persistenceCapables;
	}

	public String getShortName() {
		
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public List<CoServiceMetaData> getCoServiceMetaDatas() {
		return coServiceMetaDatas;
	}

	public void setCoServiceMetaDatas(List<CoServiceMetaData> coServiceMetaDatas) {
		this.coServiceMetaDatas = coServiceMetaDatas;
	}

	public void setType(ServiceType type) {
		this.type = type;
	}

	public CacheableMetaData getCacheableMetaData() {
		return cacheableMetaData;
	}

	public void setCacheableMetaData(CacheableMetaData cacheableMetaData) {
		this.cacheableMetaData = cacheableMetaData;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((apiLocation == null) ? 0 : apiLocation.hashCode());
		result = prime * result + ((apiName == null) ? 0 : apiName.hashCode());
		result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicesMetaData other = (ServicesMetaData) obj;
		if (apiLocation == null) {
			if (other.apiLocation != null)
				return false;
		} else if (!apiLocation.equals(other.apiLocation))
			return false;
		if (apiName == null) {
			if (other.apiName != null)
				return false;
		} else if (!apiName.equals(other.apiName))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		if (type != other.type)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	

	

	
	

}
