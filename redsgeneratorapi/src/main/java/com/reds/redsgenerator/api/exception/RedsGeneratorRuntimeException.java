package com.reds.redsgenerator.api.exception;

import com.reds.library.redscommons.exception.AbstractPlatformRuntimeException;

/**
 * <b>Purpose:</b> Runtime Exception class for reds generator
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class RedsGeneratorRuntimeException extends AbstractPlatformRuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RedsGeneratorRuntimeException(String code) {
		super(code);

	}

	/**
	 * @param code
	 * @param message
	 */
	public RedsGeneratorRuntimeException(String code, String message) {
		super(message, code);

	}

	/**
	 * @param code
	 * @param message
	 * @param cause
	 */
	public RedsGeneratorRuntimeException(String code, String message, Throwable cause) {
		super(code, message, cause);

	}

	/**
	 * @param code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedsGeneratorRuntimeException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);

	}

}
