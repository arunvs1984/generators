package com.reds.redsgenerator.api.type;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <b>Purpose:</b> To hold data type
 * 
 * @author <b>Shineed Basheer</b>
 *
 */
public enum DataType {
	OBJECT("Object"), BYTE("byte"),BYTES("bytes"), INT("int"), SHORT("short"), LONG("long"), FLOAT("float"), DOUBLE("double"),
	BOOLEAN("boolean"), CHAR("char"), STRING("String"), LIST("List"), MAP("Map"), DATE("Date"),TimeStamp("Timestamp"), GENERIC(""),
	ENUM("enum"), VOID("void"), BLOB(""), DO("");
	
	
	
	

	String value;

	private DataType(String value) {
		this.value = value;
	}

	public static DataType getDataType(String type) {

		for (DataType dataType : DataType.values()) {
			if (dataType.value.equals(type)) {
				return dataType;
			}
		}

		return DataType.OBJECT;

	}

	

	public String getJavaWrappingMapping() {
		String wrapper = null;

		switch (this) {
		case BOOLEAN:
			wrapper = Boolean.class.getName();
			break;
		case BYTE:
			wrapper = Byte.class.getName();
			break;
		
			
		case CHAR:
			wrapper = Character.class.getName();
			break;
		case DOUBLE:
			wrapper = Double.class.getName();
			break;
		case FLOAT:
			wrapper = Float.class.getName();
			break;
		case INT:
			wrapper = Integer.class.getName();
			break;
		case LIST:
			wrapper = List.class.getName();
			break;
		case LONG:
			wrapper = Long.class.getName();
			break;
		case MAP:
			wrapper = Map.class.getName();
			break;
		case OBJECT:
			wrapper = Object.class.getName();
			break;
		case SHORT:
			wrapper = Short.class.getName();
			break;
		case STRING:
			wrapper = String.class.getName();
			break;
		case DATE:
			wrapper = Date.class.getName();
			break;
		case DO:
			break;

		default:
			break;

		}
		return wrapper;
	}

	public static String getJavaToJDBCMapping(DataType talosDataTypes) {

		String jdbcType = null;

		switch (talosDataTypes) {
		case BOOLEAN:
			jdbcType = "BIT";
			break;
		case BYTE:
			jdbcType = "TINYINT";
			break;
		case CHAR:
			jdbcType = "CHAR";
			break;
		case DOUBLE:
			jdbcType = "DOUBLE";
			break;
		case FLOAT:
			jdbcType = "FLOAT";
			break;
		case INT:
			jdbcType = "INTEGER";
			break;
		case LIST:
			break;
		case LONG:
			jdbcType = "BIGINT";
			break;
		case MAP:
			break;
		case OBJECT:
			jdbcType = "BLOB";
			break;
		case SHORT:
			jdbcType = "SMALLINT";
			break;
		case STRING:
			jdbcType = "VARCHAR";
			break;
		case DO:
			break;
		case DATE:
			jdbcType = "TIMESTAMP";
			break;
		case BLOB:
			jdbcType = "BLOB";
			break;
		case BYTES:
			jdbcType = "BLOB";
			break;
		default:
			break;

		}
		return jdbcType;
	}

	public String getValue() {
		return value;
	}
}
