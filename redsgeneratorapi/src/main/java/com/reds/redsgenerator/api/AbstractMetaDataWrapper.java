package com.reds.redsgenerator.api;

import java.io.File;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public abstract class AbstractMetaDataWrapper<T extends GeneratorMetaData> {
	private T metaData;
	private String actualPackage;	
	private String actualClassName;
	private String actualPackagePath;
	private String exceptionPackage;
	private String exceptionPath;
	private String trackerPackage;	
	private String trackerPackagePath;	
	private String trackerName;
	private String infoPackage;
	private String infoPath;
	private String infoClassName;
	private String injectPackage;
	private String injectPackagePath;
	private String moduleClassName;
	private String componentClassName;
	private String treasuryPackage;
	private String treasuryName;
	private String configPackage;
	private String configfolderName;
	private String configPackagePath;
	private String configClassName;
	private String managerClassName;
	
	private String redsGenfolderName;
	protected AbstractProjectLayout layout;
	protected String rootPackageName;
	

	public AbstractMetaDataWrapper(T metaData) {
		super();
		this.metaData = metaData;
		this.layout = metaData.getLayout();
		this.rootPackageName = layout.getRootPackage();
		this.actualPackage = rootPackageName + "." + metaData.getCamelCaseName();
		String tempPackageString = this.actualPackage.replaceAll("\\.", "/");
		this.actualPackagePath = metaData.getLocation() + File.separator + metaData.getCamelCaseName() + File.separator
				+ layout.getSrc() + File.separator + tempPackageString;
		this.exceptionPackage = this.actualPackage + ".exception";
		this.exceptionPath = this.actualPackagePath + File.separator + "exception";
		this.trackerPackage = this.actualPackage + ".tracker";
		this.trackerPackagePath = this.actualPackagePath + File.separator + "tracker";
		this.trackerName = RedsStringUtils
				.capitalize(metaData.getName() + RedsGeneratorConstants.TRACKER_POSTFIX);
		this.infoPackage = this.actualPackage + ".info";
		this.infoPath = this.actualPackagePath + File.separator + "info";
		this.injectPackage = this.actualPackage + "."+ RedsGeneratorConstants.REDS_GEN_FOLDER;
		this.injectPackagePath = this.actualPackagePath + File.separator + RedsGeneratorConstants.REDS_GEN_FOLDER;
		this.configfolderName = "config";
		this.configPackage = this.actualPackage + "." + configfolderName;
		this.configPackagePath = this.actualPackagePath + File.separator + configfolderName;
		this.treasuryName = RedsStringUtils
				.capitalize(metaData.getName() + RedsGeneratorConstants.TREASURY_POSTFIX);
		this.treasuryPackage = this.actualPackage+"."+RedsGeneratorConstants.REDS_GEN_FOLDER;
		this.redsGenfolderName=RedsGeneratorConstants.REDS_GEN_FOLDER;
		this.moduleClassName=metaData.getName()+RedsGeneratorConstants.INJECT_MODULE_POSTFIX;
		this.configClassName = RedsStringUtils.capitalize(metaData.getName() + "Config");
		
	}
	
	
	
	public T getMetaData() {
		return metaData;
	}

	public void setMetaData(T metaData) {
		this.metaData = metaData;
	}

	public String getActualPackage() {
		return actualPackage;
	}

	public void setActualPackage(String actualPackage) {
		this.actualPackage = actualPackage;
	}

	public String getActualPackagePath() {
		return actualPackagePath;
	}

	public void setActualPackagePath(String actualPackagePath) {
		this.actualPackagePath = actualPackagePath;
	}

	public String getExceptionPackage() {
		return exceptionPackage;
	}

	public void setExceptionPackage(String exceptionPackage) {
		this.exceptionPackage = exceptionPackage;
	}

	public String getExceptionPath() {
		return exceptionPath;
	}

	public void setExceptionPath(String exceptionPath) {
		this.exceptionPath = exceptionPath;
	}

	public String getTrackerPackage() {
		return trackerPackage;
	}

	public void setTrackerPackage(String trackerPackage) {
		this.trackerPackage = trackerPackage;
	}

	public String getTrackerPackagePath() {
		return trackerPackagePath;
	}

	public void setTrackerPackagePath(String trackerPackagePath) {
		this.trackerPackagePath = trackerPackagePath;
	}

	public String getInfoPackage() {
		return infoPackage;
	}

	public void setInfoPackage(String infoPackage) {
		this.infoPackage = infoPackage;
	}

	public String getInfoPath() {
		return infoPath;
	}

	public void setInfoPath(String infoPath) {
		this.infoPath = infoPath;
	}

	public String getInjectPackage() {
		return injectPackage;
	}

	public void setInjectPackage(String injectPackage) {
		this.injectPackage = injectPackage;
	}

	public String getInjectPackagePath() {
		return injectPackagePath;
	}

	public void setInjectPackagePath(String injectPackagePath) {
		this.injectPackagePath = injectPackagePath;
	}

	public String getModuleClassName() {
		return moduleClassName;
	}

	public void setModuleClassName(String moduleClassName) {
		this.moduleClassName = moduleClassName;
	}

	public String getInfoClassName() {
		return infoClassName;
	}

	public void setInfoClassName(String infoClassName) {
		this.infoClassName = infoClassName;
	}

	public String getTreasuryName() {
		return treasuryName;
	}

	public void setTreasuryName(String treasuryName) {
		this.treasuryName = treasuryName;
	}

	public String getComponentClassName() {
		return componentClassName;
	}

	public void setComponentClassName(String componentClassName) {
		this.componentClassName = componentClassName;
	}

	public String getConfigPackage() {
		return configPackage;
	}

	public void setConfigPackage(String configPackage) {
		this.configPackage = configPackage;
	}

	public String getConfigPackagePath() {
		return configPackagePath;
	}

	public void setConfigPackagePath(String configPackagePath) {
		this.configPackagePath = configPackagePath;
	}

	public String getManagerClassName() {
		return managerClassName;
	}

	public void setManagerClassName(String managerClassName) {
		this.managerClassName = managerClassName;
	}

	public String getTrackerName() {
		return trackerName;
	}

	public void setTrackerName(String trackerName) {
		this.trackerName = trackerName;
	}

	public String getConfigClassName() {
		return configClassName;
	}

	public void setConfigClassName(String configClassName) {
		this.configClassName = configClassName;
	}

	public String getActualClassName() {
		return actualClassName;
	}

	public void setActualClassName(String actualClassName) {
		this.actualClassName = actualClassName;
	}

	public String getConfigfolderName() {
		return configfolderName;
	}

	public String getTreasuryPackage() {
		return treasuryPackage;
	}

	public String getRedsGenfolderName() {
		return redsGenfolderName;
	}

}
