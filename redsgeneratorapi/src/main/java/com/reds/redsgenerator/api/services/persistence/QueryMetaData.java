package com.reds.redsgenerator.api.services.persistence;

import java.io.Serializable;

public class QueryMetaData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String filter;
	private String argName;
	private String declareParams;
	private String setParams;
	private boolean unique=false;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getArgName() {
		return argName;
	}
	public void setArgName(String argName) {
		this.argName = argName;
	}
	public String getDeclareParams() {
		return declareParams;
	}
	public void setDeclareParams(String declareParams) {
		this.declareParams = declareParams;
	}
	public String getSetParams() {
		return setParams;
	}
	public void setSetParams(String setParams) {
		this.setParams = setParams;
	}
	public boolean isUnique() {
		return unique;
	}
	public void setUnique(boolean unique) {
		this.unique = unique;
	}

}
