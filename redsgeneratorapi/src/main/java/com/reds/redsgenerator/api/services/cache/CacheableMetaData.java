package com.reds.redsgenerator.api.services.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CacheableMetaData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<CacheMetaData> cacheMetaDatas = null;

	public CacheableMetaData() {
		this.cacheMetaDatas = new ArrayList<>();
	}

	public List<CacheMetaData> getCacheMetaDatas() {
		return cacheMetaDatas;
	}

	public void setCacheMetaDatas(List<CacheMetaData> cacheMetaDatas) {
		this.cacheMetaDatas = cacheMetaDatas;
	}

}
