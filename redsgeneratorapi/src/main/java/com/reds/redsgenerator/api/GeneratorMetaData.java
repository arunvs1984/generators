package com.reds.redsgenerator.api;

import java.io.File;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public abstract class GeneratorMetaData {

	/**
	 * Composer of project
	 */
	private String author;

	/**
	 * Name of the project
	 */
	private String name;
	
	private String camelCaseName;	
	/**
	 * Location of subproject
	 */
	private String location;
	
	private String root;

	private AbstractProjectLayout layout;
	
	protected boolean fromPlugin=false;
	
	private static String algolsHome="C:/algolsHome";
	
	protected String desc;
	static {
		
	}

	public GeneratorMetaData(String author, String name, String location, AbstractProjectLayout layout) {
		this.name = name;
		this.location = location;
		this.layout = layout;
		this.author = author;
		this.camelCaseName= RedsStringUtils.toCamelCase(name, true);
		this.root=new File(location).getName();
		
		

	}
	
	public static void setAlgolsHome(String algolsHome) {
		GeneratorMetaData.algolsHome=algolsHome;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public AbstractProjectLayout getLayout() {
		return layout;
	}

	public void setLayout(AbstractProjectLayout layout) {
		this.layout = layout;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCamelCaseName() {
		return camelCaseName;
	}

	public void setCamelCaseName(String camelCaseName) {
		this.camelCaseName = camelCaseName;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public boolean isFromPlugin() {
		return fromPlugin;
	}

	public void setFromPlugin(boolean fromPlugin) {
		this.fromPlugin = fromPlugin;
	}

	public static String getAlgolsHome() {
		return algolsHome;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((root == null) ? 0 : root.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeneratorMetaData other = (GeneratorMetaData) obj;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (root == null) {
			if (other.root != null)
				return false;
		} else if (!root.equals(other.root))
			return false;
		return true;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
