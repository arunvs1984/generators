package com.reds.redsgenerator.api;

import java.io.File;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public abstract class AbstractServiceMetaDataWrapper extends AbstractMetaDataWrapper<ServicesMetaData> {
	private String apiPackage;	
	private String apiClassName;
	private String apiPackagePath;
	
	private String componentClassGenName;
	
	private String cacheManagerPath;
	private String cacheManagerPackage;
	
	public AbstractServiceMetaDataWrapper(ServicesMetaData metaData) {
		super(metaData);
		this.apiPackage = rootPackageName + "." + metaData.getCamelCaseName()+RedsGeneratorConstants.SERVICE_API_POSTFIX;
		String tempPackageString = this.apiPackage.replaceAll("\\.", "/");
		this.apiPackagePath = metaData.getApiLocation() 
				+ File.separator + metaData.getApiCamelCaseName() + File.separator
				+ layout.getSrc() + File.separator + tempPackageString;			
		this.setTrackerName( RedsStringUtils
				.capitalize(metaData.getName() + RedsGeneratorConstants.TRACKER_POSTFIX));
		this.setTrackerPackage(this.apiPackage + ".tracker");
		this.setTrackerPackagePath(this.apiPackagePath + File.separator + "tracker");
		this.setExceptionPackage(this.apiPackage + ".exception");
		this.setExceptionPath(this.apiPackagePath+File.separator+"exception");
		this.setInfoPackage(this.apiPackage + ".info");
		this.setInfoPath(this.apiPackagePath+File.separator+"info");
		this.cacheManagerPath = super.getActualPackagePath()+File.separator+"cache";
		this.cacheManagerPackage=super.getActualPackage()+".cache";
		//this.setInjectPackage(super.getActualPackage() + ".autogen");
		//this.setInjectPackagePath(super.getActualPackage() + File.separator + "autogen");
		
	}

	public String getApiPackage() {
		return apiPackage;
	}

	public String getApiClassName() {
		return apiClassName;
	}

	public String getApiPackagePath() {
		return apiPackagePath;
	}

	public String getComponentClassGenName() {
		return componentClassGenName;
	}

	public void setComponentClassGenName(String componentClassGenName) {
		this.componentClassGenName = componentClassGenName;
	}

	public String getCacheManagerPath() {
		return cacheManagerPath;
	}

	public String getCacheManagerPackage() {
		return cacheManagerPackage;
	}

	@Override
	public String toString() {
		return "AbstractServiceMetaDataWrapper [apiPackage=" + apiPackage + ", apiClassName=" + apiClassName
				+ ", apiPackagePath=" + apiPackagePath + ", componentClassGenName=" + componentClassGenName
				+ ", cacheManagerPath=" + cacheManagerPath + ", cacheManagerPackage=" + cacheManagerPackage + "]";
	}

}
