package com.reds.redsgenerator.api;

public class RedsGeneratorConstants {
	public static final String REDS_GEN_FOLDER = "inject";
	public static final String CONFIG_POSTFIX = "Config";
	public static final String TRACKER_CONFIG_POSTFIX = "TrackerConfig";
	public static final String MANAGER_POSTFIX = "Manager";
	public static final String GENERATED_CONFIG_MANAGER_POSTFIX = "ConfigManagerGen";
	public static final String GENERATED_SERVICE_PROVIDER_POSTFIX = "ServiceProviderGen";
	public static final String GENERATED_COMPONENT_POSTFIX = "ComponentGen";
	public static final String INJECT_COMPONENT_POSTFIX = "Component";
	public static final String INJECT_MODULE_POSTFIX = "Module";
	public static final String INFO_POSTFIX = "Info";
	public static final String TRACKER_POSTFIX = "Track";
	public static final String TREASURY_POSTFIX = "Treasury";
	public static final String EXCEPTION_POSTFIX = "Exception";
	public static final String RUNTIME_EXCEPTION_POSTFIX = "RuntimeException";
	public static final String ERRORCODE_POSTFIX = "ErrorCodes";
	public static final String TOCKEN_PATH = ".\\template\\tockens.stg";
	public static String SERVICE_API_POSTFIX ="api";
	public static String SERVICE_IMPL_CLASS_POSTFIX ="Impl";
	public static String SERVICE_CACHE_MANAGER_POST ="CacheManager";
	public static String AUTO_GEN_JSON="autogen.json";
	
	
	public static final String REDS_PERSISTENCE_FOLDER = "persistence";
	public static final String DATAOBJECT_POSTFIX = "Do";
	public static final String MAPPER_POSTFIX = "ToDoMapper";
	public static final String DAO_POSTFIX = "Dao";
	public static final String DAO_IMP_POSTFIX = "DaoImpl";
	public static final String SAVE_PREFIX="save";
	public static final String UPDATE_PREFIX="update";	
	public static final String RETRIEVE_PREFIX="retrieve";
	public static final String RETRIEVE_ALL_PREFIX="retrieveAll";
	public static final String DELETE_ALL_PREFIX="deleteAll";
	public static final String DELETE_PREFIX="delete";
	public static final String CONNECTION_NAME="connection";
	public static final String DATA_MANAGEMENT_CONSTANT_PKG="com.reds.service.datamanagementapi.DataManagementConstants";
	public static final String DATA_MANAGEMENT_EXCEPTION_CONSTANT_PKG="com.reds.service.datamanagementapi.exception.DataManagementException";
}


