package com.reds.redsgenerator.api.services.persistence;

import java.io.Serializable;

public class FieldMetaData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String fieldName;
	
	private String initialValue;

	private String type;
	
	private String toType;
	
	private String toName;
	
	private String importString;
	
	private boolean isPrimaryKey=false;
	
	private boolean isPrimaryAutoIncrement=true;
	
	private boolean isEnumType=false;
	
	private boolean isArray=false;
	
	private boolean isList=false;
	
	private boolean isDataObject=false;
	
	private String toImportString;
	
	public String getImportString() {
		return importString;
	}

	public void setImportString(String importString) {
		this.importString = importString;
	}

	

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public boolean isArray() {
		return isArray;
	}

	public void setArray(boolean isArray) {
		this.isArray = isArray;
	}

	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}

	public boolean isDataObject() {
		return isDataObject;
	}

	public void setDataObject(boolean isDataObject) {
		this.isDataObject = isDataObject;
	}

	public String getToType() {
		return toType;
	}

	public void setToType(String toType) {
		this.toType = toType;
	}

	public String getToName() {
		return toName;
	}

	public void setToName(String toName) {
		this.toName = toName;
	}

	public String getToImportString() {
		return toImportString;
	}

	public void setToImportString(String toImportString) {
		this.toImportString = toImportString;
	}

	public boolean isEnumType() {
		return isEnumType;
	}

	public void setEnumType(boolean isEnumType) {
		this.isEnumType = isEnumType;
	}

	public boolean isPrimaryAutoIncrement() {
		return isPrimaryAutoIncrement;
	}

	public void setPrimaryAutoIncrement(boolean isPrimaryAutoIncrement) {
		this.isPrimaryAutoIncrement = isPrimaryAutoIncrement;
	}

	public String getInitialValue() {
		return initialValue;
	}

	public void setInitialValue(String initialValue) {
		this.initialValue = initialValue;
	}
	
	
	

}
