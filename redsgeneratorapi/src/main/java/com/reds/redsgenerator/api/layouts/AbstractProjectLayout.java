package com.reds.redsgenerator.api.layouts;

/**
 * <b>Purpose:</b>Abstract base class for all Project layouts
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 *NOTE: Cannot be abstract. Because it need to be json serialised
 */
public class AbstractProjectLayout implements ProjectLayout {
	private String src = "src/main/java";
	private String test = "src/test/java";
	private String generated = "src/generated/java";
	private String srcResources = "src/main/resources";
	private String testResources = "src/test/resources";
	private String rootPackage = "com.reds.generated";
	private String webApp="src/main/webapp";
	private String webInf=webApp+"/WEB-INF";

	public AbstractProjectLayout() {

	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getSrcResources() {
		return srcResources;
	}

	public void setSrcResources(String srcResources) {
		this.srcResources = srcResources;
	}

	public String getTestResources() {
		return testResources;
	}

	public void setTestResources(String testResources) {
		this.testResources = testResources;
	}

	public String getRootPackage() {
		return rootPackage;
	}

	public void setRootPackage(String rootPackage) {
		this.rootPackage = rootPackage;
	}

	public String getGenerated() {
		return generated;
	}

	public void setGenerated(String generated) {
		this.generated = generated;
	}

	public String getWebApp() {
		return webApp;
	}

	public void setWebApp(String webApp) {
		this.webApp = webApp;
	}

	public String getWebInf() {
		return webInf;
	}

	public void setWebInf(String webInf) {
		this.webInf = webInf;
	}
}
