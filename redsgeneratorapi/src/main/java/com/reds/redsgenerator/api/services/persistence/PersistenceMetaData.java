package com.reds.redsgenerator.api.services.persistence;

import java.io.Serializable;
import java.util.List;

import com.reds.redsgenerator.api.RedsGeneratorApiUtils;
import com.reds.redsgenerator.api.RedsGeneratorConstants;

public class PersistenceMetaData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String qualifiedClassName;
	protected List<FieldMetaData> fields;
	protected String simpleName;
	protected String doPackage;
	protected String doName;
	protected String daoName;
	protected String daoImplName;
	
	public void setQualifiedClassName(String qualifiedClassName) {
		this.qualifiedClassName = qualifiedClassName;
		this.simpleName = RedsGeneratorApiUtils.getSimpleNameFormPackage(qualifiedClassName);
		this.doName = this.simpleName + RedsGeneratorConstants.DATAOBJECT_POSTFIX;		
		this.daoName = this.simpleName + RedsGeneratorConstants.DAO_POSTFIX;
		this.daoImplName = this.simpleName + RedsGeneratorConstants.DAO_IMP_POSTFIX;

	}
	
	public String getQualifiedClassName() {
		return qualifiedClassName;
	}
	
	public List<FieldMetaData> getFields() {
		return fields;
	}
	public void setFields(List<FieldMetaData> fields) {
		this.fields = fields;
	}
	public String getSimpleName() {
		return simpleName;
	}
	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}
	public String getDoPackage() {
		return doPackage;
	}
	public void setDoPackage(String doPackage) {
		this.doPackage = doPackage;
	}
	public String getDoName() {
		return doName;
	}
	public void setDoName(String doName) {
		this.doName = doName;
	}
	public String getDaoName() {
		return daoName;
	}
	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}
	public String getDaoImplName() {
		return daoImplName;
	}
	public void setDaoImplName(String daoImplName) {
		this.daoImplName = daoImplName;
	}

}
