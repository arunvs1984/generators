package com.reds.redsgenerator.api.services;

import com.reds.redsgenerator.api.layouts.ServiceLayout;

public class WebServiceMetaData extends ServicesMetaData {

	private String ctxPath="";
	
	public WebServiceMetaData(String author, String name, String location, ServiceLayout layout, String version) {
		super(author, name, location, layout, version);

	}

	public String getCtxPath() {
		return ctxPath;
	}

	public void setCtxPath(String ctxPath) {
		this.ctxPath = ctxPath;
	}

}
