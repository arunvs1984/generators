package com.reds.redsgenerator.api.services;

import com.reds.generator.redsannotations.resilience.ResilienceAction;
import com.reds.redsgenerator.api.GeneratorMetaData;

public class CoServiceMetaData extends GeneratorMetaData {
	private String coServiceClass;
	private String version;
	private ResilienceAction resilienceAction;
	private String id;
	private String coServicePackage;
	private boolean exposeAsWeb=false;
	private boolean expose=false;

	public CoServiceMetaData(String id, String name,String location, String version, String coServiceClass,
			ResilienceAction resilienceAction) {
		super(null, name, location, null);
		this.coServiceClass = coServiceClass;
		this.version = version;
		this.resilienceAction = resilienceAction;
		this.id = id;
		this.coServicePackage=coServiceClass.substring(0, coServiceClass.lastIndexOf('.'));

	}

	public String getCoServiceClass() {
		return coServiceClass;
	}

	public String getVersion() {
		return version;
	}

	public ResilienceAction getResilienceAction() {
		return resilienceAction;
	}

	public String getId() {
		return id;
	}

	public String getCoServicePackage() {
		return coServicePackage;
	}

	public boolean isExposeAsWeb() {
		return exposeAsWeb;
	}

	public void setExposeAsWeb(boolean exposeAsWeb) {
		this.exposeAsWeb = exposeAsWeb;
	}

	public boolean isExpose() {
		return expose;
	}

	public void setExpose(boolean expose) {
		this.expose = expose;
	}

}
