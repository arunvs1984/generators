package com.reds.redsgenerator.api.services.persistence;

import java.util.ArrayList;

import com.reds.redsgenerator.api.RedsGeneratorConstants;

public class PersistenceCapableMetaData extends PersistenceMetaData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	private String mapperName;	
	private boolean exposeCrud;
	private boolean daoRequired;
	
	private QueriesMetaData queriesMetaData;
	
	public PersistenceCapableMetaData() {
		this.fields = new ArrayList<>();
		this.queriesMetaData = new QueriesMetaData();
	}

	public String getQualifiedClassName() {
		return qualifiedClassName;
	}

	@Override
	public void setQualifiedClassName(String qualifiedClassName) {
		super.setQualifiedClassName(qualifiedClassName);
		this.mapperName = this.simpleName + RedsGeneratorConstants.MAPPER_POSTFIX;
		

	}

	public String getMapperName() {
		return mapperName;
	}

	public void setMapperName(String mapperName) {
		this.mapperName = mapperName;
	}

	public boolean isExposeCrud() {
		return exposeCrud;
	}

	public void setExposeCrud(boolean exposeCrud) {
		this.exposeCrud = exposeCrud;
	}

	public boolean isDaoRequired() {
		return daoRequired;
	}

	public void setDaoRequired(boolean daoRequired) {
		this.daoRequired = daoRequired;
	}

	public QueriesMetaData getQueriesMetaData() {
		return queriesMetaData;
	}

	public void setQueriesMetaData(QueriesMetaData queriesMetaData) {
		this.queriesMetaData = queriesMetaData;
	}



}
