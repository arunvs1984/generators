package com.reds.redsgenerator.api.exception;

import com.reds.library.redscommons.exception.AbstractPlatformException;

/**
 * <b>Purpose:</b> Exception class for reds generator
 * 
 * @author <b>SHINEED BASHEER</b>
 *
 */
public class RedsGeneratorException extends AbstractPlatformException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RedsGeneratorException(String code) {
		super(code);

	}

	/**
	 * @param code
	 * @param message
	 */
	public RedsGeneratorException(String code, String message) {
		super(message, code);

	}

	/**
	 * @param code
	 * @param message
	 * @param cause
	 */
	public RedsGeneratorException(String code, String message, Throwable cause) {
		super(code, message, cause);

	}

	/**
	 * @param code
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RedsGeneratorException(String code, String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(code, message, cause, enableSuppression, writableStackTrace);

	}

}
