package com.reds.redsgenerator.api.library;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;

public class LibraryMetaData extends GeneratorMetaData {

	public LibraryMetaData(String author,String name, String location, AbstractProjectLayout layout) {
		super(author,name, location, layout);		
	}

	

}
