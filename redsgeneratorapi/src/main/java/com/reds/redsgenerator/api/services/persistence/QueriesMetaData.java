package com.reds.redsgenerator.api.services.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QueriesMetaData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<QueryMetaData> queries = null;

	public QueriesMetaData() {
		this.queries = new ArrayList<>();
	}

	public List<QueryMetaData> getQueries() {
		return queries;
	}

	public void setQueries(List<QueryMetaData> queries) {
		this.queries = queries;
	}
}
