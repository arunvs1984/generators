package com.reds.redsgenerator.api.tracker;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.reds.library.redscommons.tracker.PlatformTrack;
import com.reds.library.redscommons.tracker.TrackerConfig;

/**
 * @author <b>SHINEED</b>
 *
 */
public class Track extends PlatformTrack {

	public static Logger me = LogManager.getLogger(Track.class.getName());

	public static void init(String name) {
		Track.me = PlatformTrack.getTracker(new TrackerConfig(name));
	}
	
	public static void init(Logger me) {
		Track.me = me;
	}

	public static void init(TrackerConfig trackerConfig) {
		Track.me = PlatformTrack.getTracker(trackerConfig);
	}

}