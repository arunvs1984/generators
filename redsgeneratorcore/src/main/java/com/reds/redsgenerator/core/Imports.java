package com.reds.redsgenerator.core;

import java.util.HashSet;
import java.util.Set;
import org.stringtemplate.v4.ST;

public class Imports
{
  private static Set<String> imports = new HashSet();
  
  public static void removeImports()
  {
    imports.clear();
  }
  
  public static Set<String> getImports()
  {
    return imports;
  }
  
  public static void addImport(String importClass)
  {
    imports.add(getImport(importClass));
  }
  
  private static String getImport(String importString)
  {
    ST template = RedsGeneratorUtils.getTemplate(".\\template\\tockens.stg", "importDeclaration");
    
    template.add("importString", importString);
    
    return template.render();
  }
}
