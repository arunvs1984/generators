package com.reds.redsgenerator.core.services;

import java.io.File;

import org.stringtemplate.v4.ST;

import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.services.WebServiceMetaData;
import com.reds.redsgenerator.api.tracker.Track;
import com.reds.redsgenerator.core.Imports;
import com.reds.redsgenerator.core.RedsGeneratorUtils;

public class WebServiceGenerator extends ServiceGenerator<WebServiceMetaData> {
	protected final String WEBXML = ".\\template\\service\\webxml.stg";
	protected final String INDEXPAGE = ".\\template\\service\\index.stg";
	@Override
	public void generate(WebServiceMetaData metaData) throws RedsGeneratorException {
		Track.me.debug("Generating  web service module {} at {}", metaData.getName(), metaData.getLocation());
		WebServiceMetaDataWrapper webServiceMetaDataWrapper = new WebServiceMetaDataWrapper(metaData);
		createWebServiceProjectLayout(webServiceMetaDataWrapper);
		super.createApiProjectLayout(metaData);
		super.createGradle(webServiceMetaDataWrapper,"gradleWeb",webServiceMetaDataWrapper.getVersionWithDot());
		super.createApiGradle(webServiceMetaDataWrapper,"gradleWebapi");
		if(metaData.isFromPlugin()) {
			CLASSPATH=GeneratorMetaData.getAlgolsHome()+	"\\template\\service\\serviceclasspath.txt";
			DEFAULT_ICON=GeneratorMetaData.getAlgolsHome()+"\\template\\service\\serviceclasspath.txt";
		}
		super.copyClassPath(metaData, CLASSPATH);
		super.copyApiClassPath(webServiceMetaDataWrapper, CLASSPATH);
		super.createProject(webServiceMetaDataWrapper);
		super.createApiProject(webServiceMetaDataWrapper);
		super.updateRootBuild(webServiceMetaDataWrapper);
		super.updateApiRootBuild(webServiceMetaDataWrapper);
		super.createPackages( webServiceMetaDataWrapper);
		super.createApiPackages(webServiceMetaDataWrapper);
		super.createExceptions(webServiceMetaDataWrapper);
		super.createTracker( webServiceMetaDataWrapper);
		super.createInfo( webServiceMetaDataWrapper);
		super.createInject(webServiceMetaDataWrapper);
		super.createConfig(webServiceMetaDataWrapper,true);
		super.createServiceInterface(webServiceMetaDataWrapper);
		super.createServiceImp(webServiceMetaDataWrapper,true,metaData.getVersion());
		createDocServlet(webServiceMetaDataWrapper, true, metaData.getVersion());
		super.createServiceXML(webServiceMetaDataWrapper,"webservicexml",metaData.getCtxPath());
		createWebXML(webServiceMetaDataWrapper);
		createIndexPage(webServiceMetaDataWrapper);
	}
	
	private void createDocServlet(ServiceMetaDataWrapper metaDataWrapper, boolean isweb, String serviceVersion)
			throws RedsGeneratorException {
		ServicesMetaData metaData = (ServicesMetaData) metaDataWrapper.getMetaData();

		ST template = RedsGeneratorUtils.getTemplate(SERVICE, "docImpl");
		String serviceName = metaData.getName() + RedsGeneratorConstants.SERVICE_IMPL_CLASS_POSTFIX;
		metaDataWrapper.setManagerClassName(serviceName);
		String fileName = metaDataWrapper.getActualPackagePath() + File.separator + "DocServlet.java";
		template.add("className", "DocServlet");
		template.add("package", metaDataWrapper.getActualPackage());
		template.add("module", metaData.getName());
		template.add("author", metaData.getAuthor());
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("generatedDate", generatedDate);
		template.add("infoClassName", metaDataWrapper.getInfoClassName());
		template.add("serviceApi", metaData.getName());
		template.add("serviceName", metaData.getName());
		template.add("treasury", metaDataWrapper.getTreasuryName());
		template.add("tracker", metaDataWrapper.getTrackerName());
		template.add("isweb", isweb);
		template.add("serviceVersion", serviceVersion);
		
		Imports.removeImports();
		Imports.addImport(metaDataWrapper.getConfigPackage()+"."+"DocumentConfig");
		Imports.addImport(metaDataWrapper.getTreasuryPackage() + "." + metaDataWrapper.getTreasuryName());
		Imports.addImport(metaDataWrapper.getTrackerPackage() + "." + metaDataWrapper.getTrackerName());
		template.add("importString", Imports.getImports());
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}

	
	private void createIndexPage(WebServiceMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		
		WebServiceMetaData metaData = metaDataWrapper.getMetaData();
		ST template = RedsGeneratorUtils.getTemplate(INDEXPAGE, "indexpage");		
		String rootLocation = getRootLocation(metaData);
		AbstractProjectLayout layout = metaData.getLayout();		
		String fileName = rootLocation+layout.getWebApp()+ "/index.html";
		template.add("serviceName", metaData.getName());
		template.add("version", metaData.getVersion());
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("author", metaData.getAuthor());
		template.add("generatedDate", generatedDate);
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
		
		
	}


	private void createWebXML(WebServiceMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		WebServiceMetaData metaData = metaDataWrapper.getMetaData();
		ST template = RedsGeneratorUtils.getTemplate(WEBXML, "webxml");
		
		String rootLocation = getRootLocation(metaData);
		AbstractProjectLayout layout = metaData.getLayout();		
		String fileName = rootLocation+layout.getWebInf()+ "/web.xml";
		template.add("serviceName", metaData.getName());
		template.add("version", metaData.getVersion());
		template.add("impl", metaDataWrapper.getActualPackage() + "." + metaData.getName()
				+ RedsGeneratorConstants.SERVICE_IMPL_CLASS_POSTFIX);

		template.add("packageName", metaDataWrapper.getActualPackage() + "." + metaData.getName());
		template.add("docImpl", metaDataWrapper.getActualPackage() + ".DocServlet" );
		
		
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("author", metaData.getAuthor());
		template.add("generatedDate", generatedDate);
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}
	private void createWebServiceProjectLayout(WebServiceMetaDataWrapper webServiceMetaDataWrapper) throws RedsGeneratorException {
		WebServiceMetaData metaData =webServiceMetaDataWrapper.getMetaData();
		super.creatProjectLayout(metaData);
		Track.me.debug("Creating Web App folder{}", metaData.getName());
		String rootLocation = getRootLocation(metaData);
		AbstractProjectLayout layout = metaData.getLayout();		
		try {
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getWebApp());
			RedsFileUtils.makeDirForcefuly(rootLocation +layout.getWebInf());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create project layout", e);
		}
	}
}
