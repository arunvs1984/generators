package com.reds.redsgenerator.core.services;

import com.reds.redsgenerator.api.services.WebServiceMetaData;

public class WebServiceMetaDataWrapper extends ServiceMetaDataWrapper<WebServiceMetaData> {

	public WebServiceMetaDataWrapper(WebServiceMetaData metaData) {
		super(metaData);

	}

	public WebServiceMetaData getMetaData() {
		return (WebServiceMetaData) super.getMetaData();
	}

}
