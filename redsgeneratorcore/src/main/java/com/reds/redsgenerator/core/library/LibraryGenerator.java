package com.reds.redsgenerator.core.library;

import java.io.File;

import org.stringtemplate.v4.ST;

import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.library.LibraryMetaData;
import com.reds.redsgenerator.api.tracker.Track;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.reds.redsgenerator.core.Imports;
import com.reds.redsgenerator.core.RedsGeneratorUtils;

public class LibraryGenerator extends AbstractRedsGenerator<LibraryMetaData> {
	private final String GRADLE_TEMPLATE_PATH = ".\\template\\library\\library.stg";
	private final String CLASSPATH = ".\\template\\library\\libraryclasspath.txt";
	private final String INJECT = ".\\template\\library\\libraryinject.stg";
	private final String CONFIG = ".\\template\\library\\libraryconfig.stg";
	private final String MANAGER = ".\\template\\library\\librarymanager.stg";

	public void generate(LibraryMetaData metaData) throws RedsGeneratorException {
		Track.me.debug("Generating library module {} at {}", metaData.getName(), metaData.getLocation());
		LibraryMetaDataWrapper metaDataWrapper = new LibraryMetaDataWrapper(metaData);
		creatProjectLayout(metaData);
		createGradle(metaData);
		copyClassPath(metaData, ".\\template\\library\\libraryclasspath.txt");
		createProject(metaData);
		updateRootBuild(metaDataWrapper);
		createPackages(metaDataWrapper);
		createExceptions(metaDataWrapper);
		createTracker(metaDataWrapper);
		createInfo(metaDataWrapper);
		createInject(metaDataWrapper);
		createConfig(metaDataWrapper);
		createManager(metaDataWrapper);
	}

	private void createManager(LibraryMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();

		ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\librarymanager.stg", "manager");

		String managerName = RedsStringUtils.capitalize(metaData.getName() + "Manager");
		metaDataWrapper.setManagerClassName(managerName);
		String fileName = metaDataWrapper.getActualPackagePath() + File.separator + managerName + ".java";
		template.add("className", managerName);
		template.add("package", metaDataWrapper.getActualPackage());
		template.add("module", metaData.getName());
		template.add("author", metaData.getAuthor());
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("generatedDate", generatedDate);
		template.add("configClassName", metaDataWrapper.getConfigClassName());
		template.add("trackerName", metaDataWrapper.getTrackerName());
		template.add("infoClassName", metaDataWrapper.getInfoClassName());

		String treasuryName = RedsStringUtils.capitalize(metaDataWrapper.getTreasuryName());
		template.add("treasuryName", treasuryName);
		Imports.removeImports();
		Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
		Imports.addImport(metaDataWrapper.getConfigPackage() + "." + metaDataWrapper.getConfigClassName());
		Imports.addImport(metaDataWrapper.getInjectPackage() + "." + treasuryName);
		Imports.addImport(metaDataWrapper.getTrackerPackage() + "." + metaDataWrapper.getTrackerName());
		template.add("importString", Imports.getImports());

		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}

	private void createConfig(LibraryMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();
		try {
			ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\libraryconfig.stg", "config");

			String configName = RedsStringUtils.capitalize(metaData.getName() + "Config");
			metaDataWrapper.setConfigClassName(configName);
			String fileName = metaDataWrapper.getConfigPackagePath() + File.separator + configName + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getConfigPackagePath());
			template.add("className", configName);
			template.add("package", metaDataWrapper.getConfigPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();

			template.add("importString", Imports.getImports());

			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
		}
	}

	private void createInject(LibraryMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();
		try {
			ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\libraryinject.stg", "injectcomponent");

			String injectName = RedsStringUtils.capitalize(metaData.getName() + "Component");
			metaDataWrapper.setComponentClassName(injectName);
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + injectName + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", injectName);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			template.add("moduleName", metaDataWrapper.getModuleClassName());
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
		}
		try {
			ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\libraryinject.stg", "injectmodule");

			String name = RedsStringUtils.capitalize(metaData.getName() + "Module");
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + name + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", name);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject modules", e);
		}
		try {
			ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\libraryinject.stg", "treasury");

			String name = RedsStringUtils.capitalize(metaDataWrapper.getTreasuryName());
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + name + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", name);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			template.add("componentClass", metaDataWrapper.getComponentClassName());
			template.add("treasuryName", name);
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create treasury", e);
		}
	}

	private void createProject(LibraryMetaData metaData) throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\library.stg", "project");
		template.add("projectName", metaData.getName());
		String content = template.render();
		String fileName = metaData.getLocation() + File.separator + metaData.getName() + File.separator + ".project";
		write(fileName, content);
		Track.me.debug(content);
	}

	private void createGradle(LibraryMetaData metaData) throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(".\\template\\library\\library.stg", "gradle");
		String content = template.render();
		String fileName = metaData.getLocation() + File.separator + metaData.getName() + File.separator
				+ "build.gradle";

		write(fileName, content);
		Track.me.debug(content);
	}
}
