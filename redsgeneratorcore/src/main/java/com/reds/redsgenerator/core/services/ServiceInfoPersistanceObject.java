package com.reds.redsgenerator.core.services;

import com.reds.library.redscommons.to.PlatformTO;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;

public class ServiceInfoPersistanceObject implements PlatformTO {

	private ServiceInformation information;

	@Override
	public String getId() {

		return information.getMetaData().getName() + "_" + information.getMetaData().getVersion();
	}

	public final ServiceInformation getInformation() {
		return information;
	}

	public final void setInformation(ServiceInformation information) {
		this.information = information;
	}

}
