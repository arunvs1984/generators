package com.reds.redsgenerator.core.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.stringtemplate.v4.ST;

import com.reds.library.redscommons.exception.RedsXmlException;
import com.reds.library.redscommons.exception.RedscommonsErrorCodes;
import com.reds.library.redscommons.xml.RedsXmlManager;
import com.reds.library.redscommons.xml.RedsXmlManagerImpl;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import com.reds.platform.redsplatformcommons.service.ServiceInformation;
import com.reds.platform.redsplatformcommons.service.ServiceMetaData;
import com.reds.platform.redsplatformcommons.service.ServiceStatus;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.ServiceLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.tracker.Track;
import com.reds.redsgenerator.core.RedsGeneratorUtils;

public class ServiceRootGenerator<T extends ServicesMetaData> extends ServiceGenerator<T> {

	public void generate(T metaData) throws RedsGeneratorException {
		Track.me.debug("Generating service root {} at {}", metaData.getName(), metaData.getLocation());
		/* Create Root */
		String rootLocation = metaData.getLocation();
		try {
			ServiceMetaDataWrapper<T> metaDataWrapper = new ServiceMetaDataWrapper<T>(metaData);
			RedsFileUtils.makeDirForcefuly(rootLocation + "/" + metaData.getName().toLowerCase());
			this.createGradle(metaDataWrapper, "gradleRoot", metaDataWrapper.getVersion());
			this.createGradleProperties(metaDataWrapper, "gradleProp", metaDataWrapper.getVersion());
			this.createGradleSettings(metaDataWrapper, "gradleSetting", metaDataWrapper.getVersion());
			this.createProjectSettings(metaDataWrapper, "projectSettings", metaDataWrapper.getVersion());
		} catch (RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/* Create Main */
		try {
			rootLocation = rootLocation + "/" + metaData.getName().toLowerCase();
			ServicesMetaData metadataMain = new ServicesMetaData(metaData.getAuthor(), metaData.getName() + "Main",
					rootLocation, new ServiceLayout(), metaData.getVersion());

			ServiceMetaDataWrapper<ServicesMetaData> metaDataWrapper = new ServiceMetaDataWrapper<ServicesMetaData>(
					metadataMain);
			rootLocation = metadataMain.getLocation();
			RedsFileUtils.makeDirForcefuly(rootLocation);
			// RedsFileUtils.makeDirForcefuly(rootLocation + "/" +
			// metadataMain.getLayout().getSrc());

			this.createGradle(metaDataWrapper, "gradleMain", metaDataWrapper.getVersion());
			this.createProjectSettings(metaDataWrapper, "projectSettings", metaDataWrapper.getVersion());
			super.updateRootBuild(metaDataWrapper);
			this.createMainClass(metaDataWrapper, "mainCode", metaDataWrapper.getVersion());

			copyPlatform(metaDataWrapper);

		} catch (RedsutilsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<ServiceInformation> getAllDeployedServices(String workspaceLocation) {
		List<ServiceInformation> deployed = new ArrayList<ServiceInformation>();
		Track.me.info("Workspace {}", workspaceLocation);
		String serviceTempLocation = workspaceLocation + "/.workarea/services/";
		File workspace = new File(serviceTempLocation);
		if (workspace.exists()) {
			try {
				List<Path> files = RedsFileUtils.getAllFiles(serviceTempLocation);
				RedsXmlManager<ServiceInfoPersistanceObject> xmlManagerInfo = null;
				for (Path file : files) {
					xmlManagerInfo = new RedsXmlManagerImpl<ServiceInfoPersistanceObject>(serviceTempLocation);
					ServiceInfoPersistanceObject object = xmlManagerInfo.read(file.toString(),
							ServiceInfoPersistanceObject.class);
					if (null != object) {
						deployed.add(object.getInformation());
					}

				}

			} catch (RedsutilsException | RedsXmlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return deployed;
	}

	public List<ServiceMetaData> getAllRespoServices() {
		List<ServiceMetaData> repoServices = new ArrayList<ServiceMetaData>();
		String algolsHome = GeneratorMetaData.getAlgolsHome();
		String repoLocation = algolsHome + "/" + "repo";
		Track.me.info("Algols Home {}", algolsHome);
		File service = new File(algolsHome);
		if (service.exists()) {
			try {
				List<Path> files = RedsFileUtils.getAllFiles(repoLocation);
				for (Path file : files) {
					if (file.toString().endsWith(".zip") && !file.toString().endsWith("Platform_1_0.zip")) {
						String tempLocation = algolsHome + "/temp/service";
						try {
							RedsFileUtils.makeDirForcefuly(tempLocation);
							RedsFileUtils.unZip(file.toString(), tempLocation);
							ServiceMetaData serviceMeta = new ServiceMetaData();
							String xmlLocation = tempLocation + "/service.xml";
							RedsXmlManager<ServiceMetaData> xmlManager = new RedsXmlManagerImpl<ServiceMetaData>(
									xmlLocation);
							serviceMeta = xmlManager.read(xmlLocation, ServiceMetaData.class);
							// RedsFileUtils.copyDir(tempLocation, workspaceLocation);
							if (null != serviceMeta) {
								repoServices.add(serviceMeta);
							}
						} finally {
							RedsFileUtils.deleteQuietly(tempLocation);
						}
					}
				}

			} catch (RedsutilsException | RedsXmlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return repoServices;
	}

	public void deploy(String zipLocation, String workspaceLocation) throws RedsGeneratorException {
		String algolsHome = GeneratorMetaData.getAlgolsHome();
		if (algolsHome != null) {
			Track.me.info("Algols Home {}", algolsHome);
			Track.me.info("Deploying Service  {} to {} ", zipLocation, workspaceLocation);
			File service = new File(zipLocation);
			if (service.exists()) {
				if (new File(workspaceLocation).exists()) {
					String tempLocation = algolsHome + "/temp/service";
					try {
						RedsFileUtils.makeDirForcefuly(tempLocation);
						RedsFileUtils.unZip(zipLocation, tempLocation);
						ServiceMetaData serviceMeta = new ServiceMetaData();
						String xmlLocation = tempLocation + "/service.xml";
						RedsXmlManager<ServiceMetaData> xmlManager = new RedsXmlManagerImpl<ServiceMetaData>(
								xmlLocation);
						serviceMeta = xmlManager.read(xmlLocation, ServiceMetaData.class);
						// RedsFileUtils.copyDir(tempLocation, workspaceLocation);
						if (null != serviceMeta) {
							PlatformLayout layout = new PlatformLayout();
							String serviceFolder = "";
							String serviceConfigLocation = "";
							String serviceLibs = "";
							switch (serviceMeta.getType()) {
							case BUSINESS:
								String businessPot = workspaceLocation + "/" + "businesspot" + "/"
										+ serviceMeta.getName() + serviceMeta.getVersion();
								RedsFileUtils.makeDirForcefuly(businessPot);
								RedsFileUtils.copyDir(tempLocation, businessPot);
								serviceFolder = businessPot + serviceMeta.getName() + serviceMeta.getVersion() + "/";
								serviceConfigLocation = serviceFolder + layout.getBusinessServiceConfig();
								serviceLibs = serviceFolder + layout.getBusinessServiceLibs();
								break;
							case PLATFORM:
								String redspot = workspaceLocation + "/" + "redspot" + "/" + serviceMeta.getName()
										+ serviceMeta.getVersion();
								RedsFileUtils.makeDirForcefuly(redspot);
								RedsFileUtils.copyDir(tempLocation, redspot);
								String redsPot = layout.getPlatformServiceRoot();
								serviceFolder = redsPot + serviceMeta.getName() + serviceMeta.getVersion() + "/";
								serviceConfigLocation = serviceFolder + layout.getPlatformServiceConfig();
								serviceLibs = serviceFolder + layout.getPlatformServiceLibs();

								break;
							default:
								break;

							}
							ServiceInformation serviceInformation = new ServiceInformation();
							String icon = serviceMeta.getIcon();
							if (icon != null) {
								icon = tempLocation + "/" + icon;
								String destination = workspaceLocation + "/marketplace/icon/" + serviceMeta.getIcon();
								RedsFileUtils.copy(new File(icon), new File(destination));
								serviceInformation.setServiceIcon("./marketplace/icon/" + serviceMeta.getIcon());
							}

							serviceInformation.setMetaData(serviceMeta);
							serviceInformation.setLastUpdatedTime(System.currentTimeMillis());
							serviceInformation.setServiceConfigLocation(serviceConfigLocation);
							serviceInformation.setServiceLibs(serviceLibs);
							serviceInformation.setServiceLocation(serviceFolder);
							serviceInformation.setStatus(ServiceStatus.INITIALIZED);
							ServiceInfoPersistanceObject persistenceObject = new ServiceInfoPersistanceObject();
							persistenceObject.setInformation(serviceInformation);
							String serviceTempLocation = workspaceLocation + "/.workarea/services/";
							RedsXmlManager<ServiceInfoPersistanceObject> xmlManagerInfo = new RedsXmlManagerImpl<ServiceInfoPersistanceObject>(
									serviceTempLocation);
							xmlManagerInfo.write(persistenceObject);

						}

					} catch (RedsutilsException | RedsXmlException | IOException e) {
						throw new RedsGeneratorException(RedscommonsErrorCodes.errorCode(1), "Failed to copy libs", e);
					} finally {
						RedsFileUtils.deleteQuietly(tempLocation);
					}
				}
			}
		}
	}

	private void copyPlatform(ServiceMetaDataWrapper<ServicesMetaData> metaDataWrapper)
			throws RedsutilsException, RedsGeneratorException {
		String algolsHome = GeneratorMetaData.getAlgolsHome();
		if (algolsHome != null) {
			Track.me.info("Algols Home {}", algolsHome);
			String platformZip = algolsHome + "/repo/Platform_1_0.zip";
			File platform = new File(platformZip);
			if (platform.exists()) {
				String destinationLocation = metaDataWrapper.getMetaData().getLocation() + "/"
						+ metaDataWrapper.getMetaData().getCamelCaseName();

				if (new File(destinationLocation).exists()) {
					String tempLocation = algolsHome + "/temp/workspace";
					try {
						destinationLocation = destinationLocation + "/workspace";
						RedsFileUtils.makeDirForcefuly(tempLocation);
						RedsFileUtils.unZip(platformZip, tempLocation);
						String platformPath = tempLocation + "/" + "Platform-1.0.jar";
						String destPlatformPath = tempLocation + "/" + "lib/Platform-1.0.jar";
						RedsFileUtils.copy(new File(platformPath), new File(destPlatformPath));
						RedsFileUtils.deleteQuietly(platformPath);
						RedsFileUtils.copyDir(tempLocation, destinationLocation);

					} catch (IOException e) {
						throw new RedsGeneratorException(RedscommonsErrorCodes.errorCode(1), "Failed to copy libs", e);
					} finally {
						RedsFileUtils.deleteQuietly(tempLocation);
					}

				} else {

				}

			} else {
				Track.me.error("Platform Zip not found {} ", platformZip);
			}
		} else {
			Track.me.error("Algols Home Not found {} ", algolsHome);
		}

	}

	private void createMainClass(ServiceMetaDataWrapper<ServicesMetaData> wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		try {
			ST template = RedsGeneratorUtils.getTemplate(SERVICE_MAIN, templateKey);
			String className = RedsStringUtils.capitalize(wrapper.getMetaData().getName());
			String serviceName = ((ServicesMetaData) wrapper.getMetaData()).getName().toLowerCase();

			template.add("className", className);
			template.add("package", wrapper.getActualPackage());
			template.add("author", wrapper.getMetaData().getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			RedsFileUtils.makeDirForcefuly(wrapper.getActualPackagePath());
			String content = template.render();
			String fileName = wrapper.getActualPackagePath() + "/" + className + ".java";
			write(fileName, content);
			Track.me.debug("Main File - {} - {}", fileName, content);
		} catch (RedsGeneratorException | RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create Main Project", e);
		}
	}

	protected void createGradle(ServiceMetaDataWrapper wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		String serviceName = ((ServicesMetaData) wrapper.getMetaData()).getName().toLowerCase();
		template.add("serviceName", serviceName);
		template.add("version", version);
		template.add("apiName", ((ServicesMetaData) wrapper.getMetaData()).getApiCamelCaseName());
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + "/" + serviceName + "/"
				+ "build.gradle";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createGradleProperties(ServiceMetaDataWrapper wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		String serviceName = ((ServicesMetaData) wrapper.getMetaData()).getName().toLowerCase();
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + "/" + serviceName + "/"
				+ "gradle.properties";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createGradleSettings(ServiceMetaDataWrapper wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		String serviceName = ((ServicesMetaData) wrapper.getMetaData()).getName().toLowerCase();
		template.add("serviceName", serviceName);
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + "/" + serviceName + "/"
				+ "settings.gradle";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createProjectSettings(ServiceMetaDataWrapper wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		String serviceName = ((ServicesMetaData) wrapper.getMetaData()).getName().toLowerCase();
		template.add("serviceName", serviceName);
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + "/" + serviceName + "/"
				+ ".project";
		write(fileName, content);
		Track.me.debug(content);
	}

}
