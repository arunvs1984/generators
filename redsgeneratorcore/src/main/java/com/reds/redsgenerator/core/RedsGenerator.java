package com.reds.redsgenerator.core;

import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;

public abstract interface RedsGenerator<T extends GeneratorMetaData>
{
  public abstract void generate(T paramT)
    throws RedsGeneratorException;
}
