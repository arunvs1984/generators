package com.reds.redsgenerator.core;

import java.io.File;

import org.stringtemplate.v4.ST;

import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import com.reds.redsgenerator.api.AbstractMetaDataWrapper;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;
import com.reds.redsgenerator.api.tracker.Track;

public abstract class AbstractRedsGenerator<T extends GeneratorMetaData> implements RedsGenerator<T> {
	private final String COMMONS_TEMPLATE_PATH = ".\\template\\common.stg";

	protected void copyClassPath(T metaData, String source) throws RedsGeneratorException {
		try {
			String rootLocation = getRootLocation(metaData) + ".classpath";
			RedsFileUtils.copy(source, rootLocation);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to copy project classpath", e);
		}
	}
	
	

	protected void createInfo(AbstractMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		try {
			GeneratorMetaData metaData = metaDataWrapper.getMetaData();

			ST template = RedsGeneratorUtils.getTemplate(".\\template\\common.stg", "info");
			String infoName = RedsStringUtils.capitalize(metaData.getName() + "Info");
			metaDataWrapper.setInfoClassName(infoName);
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInfoPath());
			String fileName = metaDataWrapper.getInfoPath() + File.separator + infoName + ".java";
			template.add("className", infoName);
			template.add("package", metaDataWrapper.getInfoPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			String content = template.render();
			write(fileName, content);
			Track.me.debug(content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject", e);
		}
	}

	protected void createTracker(AbstractMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();

		ST template = RedsGeneratorUtils.getTemplate(".\\template\\common.stg", "tracker");

		String trackerName = RedsStringUtils.capitalize(metaData.getName() + "Track");
		metaDataWrapper.setTrackerName(trackerName);
		String fileName = metaDataWrapper.getTrackerPackagePath() + File.separator + trackerName + ".java";
		template.add("className", trackerName);
		template.add("package", metaDataWrapper.getTrackerPackage());
		template.add("module", metaData.getName());
		template.add("author", metaData.getAuthor());

		String content = template.render();
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createPackages(AbstractMetaDataWrapper<T> metaData) throws RedsGeneratorException {
		try {
			RedsFileUtils.makeDirForcefuly(metaData.getActualPackagePath());
			RedsFileUtils.makeDirForcefuly(metaData.getExceptionPath());
			RedsFileUtils.makeDirForcefuly(metaData.getTrackerPackagePath());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed create package structure");
		}
	}

	protected void createExceptions(AbstractMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();

		ST templateException = RedsGeneratorUtils.getTemplate(".\\template\\common.stg", "exception");

		String exceptionName = RedsStringUtils.capitalize(metaData.getName() + "Exception");
		String fileNameException = metaDataWrapper.getExceptionPath() + File.separator + exceptionName + ".java";
		templateException.add("className", exceptionName);
		templateException.add("package", metaDataWrapper.getExceptionPackage());
		templateException.add("module", metaData.getName());
		templateException.add("author", metaData.getAuthor());

		String contentException = templateException.render();
		write(fileNameException, contentException);
		Track.me.debug(contentException);

		ST templateRuntimeException = RedsGeneratorUtils.getTemplate(".\\template\\common.stg", "runtimeexception");

		String runTimeExceptionName = RedsStringUtils.capitalize(metaData.getName() + "RuntimeException");
		String fileNameRuntimeException = metaDataWrapper.getExceptionPath() + File.separator + runTimeExceptionName
				+ ".java";

		templateRuntimeException.add("className", runTimeExceptionName);
		templateRuntimeException.add("package", metaDataWrapper.getExceptionPackage());
		templateRuntimeException.add("module", metaData.getName());
		templateRuntimeException.add("author", metaData.getAuthor());
		String contentRuntimeException = templateRuntimeException.render();
		write(fileNameRuntimeException, contentRuntimeException);
		Track.me.debug(contentRuntimeException);

		ST errorCode = RedsGeneratorUtils.getTemplate(".\\template\\common.stg", "errorcode");

		String errorCodeName = RedsStringUtils.capitalize(metaData.getName() + "ErrorCodes");
		String fileNameErrorCode = metaDataWrapper.getExceptionPath() + File.separator + errorCodeName + ".java";
		errorCode.add("className", errorCodeName);
		errorCode.add("package", metaDataWrapper.getExceptionPackage());
		errorCode.add("module", metaData.getName());
		errorCode.add("author", metaData.getAuthor());
		String contentErrorCode = errorCode.render();
		write(fileNameErrorCode, contentErrorCode);
		Track.me.debug(contentErrorCode);
	}

	protected void updateRootBuild(AbstractMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		try {
			String moduleName = metaDataWrapper.getMetaData().getCamelCaseName();
			String include = "\n include '" + moduleName + "'";
			String location = metaDataWrapper.getMetaData().getLocation() + File.separator + "settings.gradle";
			RedsFileUtils.write(location, include, true);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to write file", e);
		}
	}

	protected String getRootLocation(T metaData) {
		return metaData.getLocation() + File.separator + metaData.getCamelCaseName() + File.separator;
	}

	protected void creatProjectLayout(T metaData) throws RedsGeneratorException {
		
		AbstractProjectLayout layout = metaData.getLayout();
		try {
			Track.me.debug("Creating Project Layout for {}", metaData.getName());
			String rootLocation = getRootLocation(metaData);
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getSrc());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getGenerated());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getSrcResources());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getTest());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getTestResources());
			Track.me.debug("Project Layout created for {}", metaData.getName());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create project layout", e);
		}
	}

	public void write(String location, String code) throws RedsGeneratorException {
		try {
			RedsFileUtils.write(location, code);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to write source", e);
		}
	}
}
