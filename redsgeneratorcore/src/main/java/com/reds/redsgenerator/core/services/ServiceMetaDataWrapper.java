package com.reds.redsgenerator.core.services;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.redsgenerator.api.AbstractServiceMetaDataWrapper;
import com.reds.redsgenerator.api.services.ServicesMetaData;

public class ServiceMetaDataWrapper<T extends ServicesMetaData> extends AbstractServiceMetaDataWrapper {
	public ServiceMetaDataWrapper(T metaData) {
		super(metaData);
	}

	public String getVersion() {
		return RedsStringUtils.removeDotByUnderScore(((ServicesMetaData) getMetaData()).getVersion());
	}
	
	public String getVersionWithDot() {
		return ((ServicesMetaData) getMetaData()).getVersion();
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
