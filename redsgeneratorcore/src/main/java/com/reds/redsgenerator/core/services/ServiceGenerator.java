package com.reds.redsgenerator.core.services;

import java.io.File;

import org.stringtemplate.v4.ST;

import com.reds.generator.redsannotations.Configuration;
import com.reds.generator.redsannotations.Required;
import com.reds.generator.redsannotations.Service;
import com.reds.library.redscommons.config.PlatformConfig;
import com.reds.library.redscommons.tracker.TrackerConfig;
import com.reds.library.redsutils.RedsDateUtils;
import com.reds.library.redsutils.RedsFileUtils;
import com.reds.library.redsutils.RedsStringUtils;
import com.reds.library.redsutils.exception.RedsutilsException;
import com.reds.platform.redsplatformcommons.layout.PlatformLayout;
import com.reds.platform.servicecommons.context.CloseContext;
import com.reds.platform.servicecommons.context.DeploymentContext;
import com.reds.platform.servicecommons.context.InitContext;
import com.reds.platform.servicecommons.context.ServiceContext;
import com.reds.redsgenerator.api.AbstractMetaDataWrapper;
import com.reds.redsgenerator.api.GeneratorMetaData;
import com.reds.redsgenerator.api.RedsGeneratorConstants;
import com.reds.redsgenerator.api.exception.ErrorCodes;
import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.AbstractProjectLayout;
import com.reds.redsgenerator.api.services.ServicesMetaData;
import com.reds.redsgenerator.api.tracker.Track;
import com.reds.redsgenerator.core.AbstractRedsGenerator;
import com.reds.redsgenerator.core.Imports;
import com.reds.redsgenerator.core.RedsGeneratorUtils;

public class ServiceGenerator<T extends ServicesMetaData> extends AbstractRedsGenerator<T> {
	
	protected final String GRADLE_TEMPLATE_PATH = ".\\template\\service\\service.stg";
	protected  String CLASSPATH = ".\\template\\service\\serviceclasspath.txt";
	protected  String DEFAULT_ICON = ".\\template\\service\\default.jpg";
	protected final String AUTOGEN = ".\\template\\service\\serviceautogen.stg";
	protected final String CONFIG = ".\\template\\service\\serviceconfig.stg";
	protected final String SERVICE = ".\\template\\service\\serviceclass.stg";
	protected final String SERVICEXML = ".\\template\\service\\servicexml.stg";
	protected final String SERVICE_MAIN = ".\\template\\service\\servicemain.stg";

	public void generate(T metaData) throws RedsGeneratorException {
		Track.me.debug("Generating service module {} at {} is from pluggin{}", metaData.getName(), metaData.getLocation(),metaData.isFromPlugin());
		ServiceMetaDataWrapper<T> metaDataWrapper = new ServiceMetaDataWrapper<T>(metaData);
		creatProjectLayout(metaData);
		createApiProjectLayout(metaData);
		createGradle(metaDataWrapper, "gradle", metaDataWrapper.getVersion());
		createApiGradle(metaDataWrapper, "gradleapi");
		if(metaData.isFromPlugin()) {
			CLASSPATH=GeneratorMetaData.getAlgolsHome()+	"\\template\\service\\serviceclasspath.txt";
			DEFAULT_ICON=GeneratorMetaData.getAlgolsHome()+"\\template\\service\\serviceclasspath.txt";
		}
		copyClassPath(metaData, CLASSPATH);
		copyApiClassPath(metaDataWrapper, CLASSPATH);
		copyDefaultIcon(metaData, DEFAULT_ICON);
		createProject(metaDataWrapper);
		createApiProject(metaDataWrapper);
		updateRootBuild((AbstractMetaDataWrapper<T>) metaDataWrapper);
		updateApiRootBuild(metaDataWrapper);
		createPackages((AbstractMetaDataWrapper<T>) metaDataWrapper);
		createApiPackages(metaDataWrapper);
		createExceptions((AbstractMetaDataWrapper<T>) metaDataWrapper);
		createTracker((AbstractMetaDataWrapper<T>) metaDataWrapper);
		createInfo((AbstractMetaDataWrapper<T>) metaDataWrapper);
		createInject(metaDataWrapper);
		createConfig(metaDataWrapper,false);
		createServiceInterface(metaDataWrapper);
		createServiceImp(metaDataWrapper, false, metaData.getVersion());
		createServiceXML(metaDataWrapper, "servicexml", null);
	}

	protected void createPackages(AbstractMetaDataWrapper wrapper) throws RedsGeneratorException {
		try {
			RedsFileUtils.makeDirForcefuly(wrapper.getActualPackagePath());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed create package structure");
		}
	}

	public void createApiPackages(ServiceMetaDataWrapper wrapper) throws RedsGeneratorException {
		try {
			RedsFileUtils.makeDirForcefuly(wrapper.getApiPackagePath());
			RedsFileUtils.makeDirForcefuly(wrapper.getExceptionPath());
			RedsFileUtils.makeDirForcefuly(wrapper.getTrackerPackagePath());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed create api package structure");
		}
	}

	protected void updateApiRootBuild(ServiceMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		try {
			String moduleName = ((ServicesMetaData) metaDataWrapper.getMetaData()).getApiCamelCaseName();
			String include = "\n include '" + moduleName + "'";
			String location = ((ServicesMetaData) metaDataWrapper.getMetaData()).getLocation() + File.separator
					+ "settings.gradle";
			RedsFileUtils.write(location, include, true);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to write file", e);
		}
	}

	protected void copyApiClassPath(ServiceMetaDataWrapper wrapper, String source) throws RedsGeneratorException {
		try {
			String rootLocation = getApiRootLocation((ServicesMetaData) wrapper.getMetaData()) + ".classpath";
			RedsFileUtils.copy(source, rootLocation);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to copy project classpath", e);
		}
	}

	protected void createApiProjectLayout(T metaData) throws RedsGeneratorException {
		AbstractProjectLayout layout = metaData.getLayout();
		try {
			Track.me.debug("Creating Project API Layout for {}", metaData.getName());
			String rootLocation = getApiRootLocation(metaData);
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getSrc());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getSrcResources());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getTest());
			RedsFileUtils.makeDirForcefuly(rootLocation + layout.getTestResources());
			Track.me.debug("Project Layout created for {}", metaData.getName());
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create api project layout", e);
		}
	}

	protected String getApiRootLocation(ServicesMetaData metaData) {
		return metaData.getApiLocation() + File.separator + metaData.getApiCamelCaseName() + File.separator;
	}

	protected void createServiceXML(ServiceMetaDataWrapper metaDataWrapper, String templateKey, String ctxPath)
			throws RedsGeneratorException {
		ServicesMetaData metaData = (ServicesMetaData) metaDataWrapper.getMetaData();
		ST template = RedsGeneratorUtils.getTemplate(SERVICEXML, templateKey);

		String fileName = metaData.getLocation() + File.separator + metaData.getCamelCaseName() + File.separator
				+ metaData.getLayout().getSrcResources() + File.separator + new PlatformLayout().getServicMetaDataName()
				+ ".xml";
		template.add("serviceName", metaData.getName());
		template.add("version", metaData.getVersion());

		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("author", metaData.getAuthor());
		template.add("generatedDate", generatedDate);
		template.add("desc", metaData.getDesc());
		
		if (null != ctxPath) {
			template.add("ctxPath", ctxPath);
			String name=metaData.getName();
			name=name.toLowerCase();
			template.add("serviceNameLower", name);
			
			
		} else {
			template.add("impl", metaDataWrapper.getActualPackage() + "." + metaData.getName()
					+ RedsGeneratorConstants.SERVICE_IMPL_CLASS_POSTFIX);
			template.add("type", metaData.getType());
		}
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}

	protected void copyDefaultIcon(T metaData, String source) throws RedsGeneratorException {
		try {
			String fileName = metaData.getLocation() + File.separator + metaData.getCamelCaseName() + File.separator
					+ metaData.getLayout().getSrcResources() + File.separator + metaData.getName()
					+ metaData.getVersion() + ".jpg";
			RedsFileUtils.copy(source, fileName);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to copy project classpath", e);
		}
	}

	protected void createServiceImp(ServiceMetaDataWrapper metaDataWrapper, boolean isweb, String serviceVersion)
			throws RedsGeneratorException {
		ServicesMetaData metaData = (ServicesMetaData) metaDataWrapper.getMetaData();

		ST template = RedsGeneratorUtils.getTemplate(SERVICE, "serviceImpl");
		String serviceName = metaData.getName() + RedsGeneratorConstants.SERVICE_IMPL_CLASS_POSTFIX;
		metaDataWrapper.setManagerClassName(serviceName);
		String fileName = metaDataWrapper.getActualPackagePath() + File.separator + serviceName + ".java";
		template.add("className", serviceName);
		template.add("package", metaDataWrapper.getActualPackage());
		template.add("module", metaData.getName());
		template.add("author", metaData.getAuthor());
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("generatedDate", generatedDate);
		template.add("infoClassName", metaDataWrapper.getInfoClassName());
		template.add("serviceApi", metaData.getName());
		template.add("serviceName", metaData.getName());
		template.add("treasury", metaDataWrapper.getTreasuryName());
		template.add("tracker", metaDataWrapper.getTrackerName());
		template.add("isweb", isweb);
		template.add("serviceVersion", serviceVersion);

		Imports.removeImports();
		Imports.addImport(Required.class.getName());
		Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
		Imports.addImport(metaDataWrapper.getApiPackage() + "." + metaData.getName());
		Imports.addImport(metaDataWrapper.getTreasuryPackage() + "." + metaDataWrapper.getTreasuryName());
		Imports.addImport(metaDataWrapper.getTrackerPackage() + "." + metaDataWrapper.getTrackerName());
		template.add("importString", Imports.getImports());
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}

	private String getRequired() {
		return "";
	}

	protected void createServiceInterface(ServiceMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		ServicesMetaData metaData = (ServicesMetaData) metaDataWrapper.getMetaData();

		ST template = RedsGeneratorUtils.getTemplate(SERVICE, "serviceInterface");
		String serviceName = metaData.getName();
		metaDataWrapper.setManagerClassName(serviceName);
		String fileName = metaDataWrapper.getApiPackagePath() + File.separator + serviceName + ".java";
		template.add("className", serviceName);
		template.add("package", metaDataWrapper.getApiPackage());
		template.add("module", metaData.getName());
		template.add("author", metaData.getAuthor());
		String generatedDate = RedsDateUtils.getCurrenTimeStamp();
		template.add("generatedDate", generatedDate);
		template.add("infoClassName", metaDataWrapper.getInfoClassName());
		template.add("serviceName", metaData.getName());
		template.add("serviceType", metaData.getType());
		template.add("required", getRequired());
		template.add("root", metaData.getRoot());
		Imports.removeImports();
		Imports.addImport(Service.class.getName());
		Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
		template.add("importString", Imports.getImports());
		String content = template.render();
		write(fileName, content);
		Track.me.debug("File - {} - {}", fileName, content);
	}

	protected void createConfig(ServiceMetaDataWrapper metaDataWrapper, boolean isweb) throws RedsGeneratorException {
		GeneratorMetaData metaData = metaDataWrapper.getMetaData();
		try {
			ST template = RedsGeneratorUtils.getTemplate(CONFIG, "config");
			String configName = RedsStringUtils.capitalize(metaData.getName() + RedsGeneratorConstants.CONFIG_POSTFIX);
			metaDataWrapper.setConfigClassName(configName);
			String fileName = metaDataWrapper.getConfigPackagePath() + File.separator + configName + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getConfigPackagePath());
			template.add("className", configName);
			template.add("package", metaDataWrapper.getConfigPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(Configuration.class.getName());
			Imports.addImport(PlatformConfig.class.getName());
			template.add("importString", Imports.getImports());
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
		}
		if (isweb) {
			try {
				ST template = RedsGeneratorUtils.getTemplate(CONFIG, "docConfig");
				String configName = RedsStringUtils
						.capitalize(metaData.getName() + RedsGeneratorConstants.CONFIG_POSTFIX);
				metaDataWrapper.setConfigClassName(configName);
				String fileName = metaDataWrapper.getConfigPackagePath() + File.separator + "DocumentConfig.java";
				RedsFileUtils.makeDirForcefuly(metaDataWrapper.getConfigPackagePath());
				template.add("className", "DocumentConfig");
				template.add("package", metaDataWrapper.getConfigPackage());
				template.add("module", metaData.getName());
				template.add("author", metaData.getAuthor());
				String generatedDate = RedsDateUtils.getCurrenTimeStamp();
				template.add("generatedDate", generatedDate);
				Imports.removeImports();
				Imports.addImport(Configuration.class.getName());
				Imports.addImport(PlatformConfig.class.getName());
				template.add("importString", Imports.getImports());
				String content = template.render();
				write(fileName, content);
				Track.me.debug("File - {} - {}", fileName, content);
			} catch (RedsutilsException e) {
				throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
			}

		}
		try {
			ST template = RedsGeneratorUtils.getTemplate(CONFIG, "trackerconfig");
			String configName = RedsStringUtils
					.capitalize(metaData.getName() + RedsGeneratorConstants.TRACKER_CONFIG_POSTFIX);
			metaDataWrapper.setConfigClassName(configName);
			String fileName = metaDataWrapper.getConfigPackagePath() + File.separator + configName + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getConfigPackagePath());
			template.add("className", configName);
			template.add("package", metaDataWrapper.getConfigPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			template.add("trackerBase", TrackerConfig.class.getSimpleName());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(Configuration.class.getName());
			Imports.addImport(TrackerConfig.class.getName());
			Imports.addImport(PlatformConfig.class.getName());
			template.add("importString", Imports.getImports());
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
		}
	}

	protected void createInject(ServiceMetaDataWrapper metaDataWrapper) throws RedsGeneratorException {
		ServicesMetaData metaData = (ServicesMetaData) metaDataWrapper.getMetaData();
		String serviceProviderGen = metaData.getName() + RedsGeneratorConstants.GENERATED_SERVICE_PROVIDER_POSTFIX;
		try {
			ST template = RedsGeneratorUtils.getTemplate(AUTOGEN, "injectcomponent");
			String injectName = RedsStringUtils.capitalize(metaData.getName() + "Component");
			metaDataWrapper.setComponentClassName(injectName);
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + injectName + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", injectName);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			template.add("moduleName", metaDataWrapper.getModuleClassName());
			String configManagetGenName = metaData.getName() + RedsGeneratorConstants.GENERATED_CONFIG_MANAGER_POSTFIX;

			template.add("configManagerGen", configManagetGenName);
			template.add("serviceProviderGen", serviceProviderGen);
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject component", e);
		}

		/*
		 * { ST template = RedsGeneratorUtils.getTemplate(AUTOGEN,
		 * "injectcomponentgen"); String injectName =
		 * RedsStringUtils.capitalize(metaData.getName() + "ComponentGen");
		 * metaDataWrapper.setComponentClassGenName(injectName); String fileName =
		 * metaDataWrapper.getInjectPackagePath() + File.separator + injectName +
		 * ".java"; template.add("className", injectName); template.add("package",
		 * metaDataWrapper.getInjectPackage()); String content = template.render();
		 * write(fileName, content); Track.me.debug("File - {} - {}", fileName,
		 * content); }
		 */
		try {
			ST template = RedsGeneratorUtils.getTemplate(AUTOGEN, "injectmodule");
			String name = RedsStringUtils.capitalize(metaData.getName() + "Module");
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + name + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", name);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(metaDataWrapper.getInfoPackage() + "." + metaDataWrapper.getInfoClassName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create inject modules", e);
		}
		/*
		 * try { ST template = RedsGeneratorUtils.getTemplate(AUTOGEN,
		 * "injectconfigmanagergen");
		 * 
		 * String name = RedsStringUtils.capitalize(metaData.getName() +
		 * "ConfigManagerGen"); String fileName = metaDataWrapper.getInjectPackagePath()
		 * + File.separator + name + ".java";
		 * RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
		 * template.add("className", name); template.add("package",
		 * metaDataWrapper.getInjectPackage()); String content = template.render();
		 * write(fileName, content); Track.me.debug("File - {} - {}", fileName,
		 * content); } catch (RedsutilsException e) { throw new
		 * RedsGeneratorException(ErrorCodes.errorCode(1),
		 * "Failed to create inject configuration manager modules", e); }
		 */
		try {

			ST template = RedsGeneratorUtils.getTemplate(AUTOGEN, "treasury");
			String name = RedsStringUtils.capitalize(metaDataWrapper.getTreasuryName());
			String fileName = metaDataWrapper.getInjectPackagePath() + File.separator + name + ".java";
			RedsFileUtils.makeDirForcefuly(metaDataWrapper.getInjectPackagePath());
			template.add("className", name);
			template.add("package", metaDataWrapper.getInjectPackage());
			template.add("module", metaData.getName());
			template.add("author", metaData.getAuthor());
			String generatedDate = RedsDateUtils.getCurrenTimeStamp();
			template.add("generatedDate", generatedDate);
			Imports.removeImports();
			Imports.addImport(DeploymentContext.class.getName());
			Imports.addImport(ServiceContext.class.getName());
			Imports.addImport(InitContext.class.getName());
			Imports.addImport(CloseContext.class.getName());
			Imports.addImport(metaDataWrapper.getTrackerPackage() + "." + metaDataWrapper.getTrackerName());
			template.add("importString", Imports.getImports());
			template.add("infoClass", metaDataWrapper.getInfoClassName());
			template.add("componentClass", metaDataWrapper.getComponentClassName());
			template.add("componentClassGen", metaDataWrapper.getComponentClassGenName());
			template.add("treasuryName", name);
			String configName = metaData.getName() + RedsGeneratorConstants.TRACKER_CONFIG_POSTFIX;
			template.add("trackerConfig", configName);
			template.add("tracker", metaDataWrapper.getTrackerName());

			String content = template.render();
			write(fileName, content);
			Track.me.debug("File - {} - {}", fileName, content);
		} catch (RedsutilsException e) {
			throw new RedsGeneratorException(ErrorCodes.errorCode(1), "Failed to create treasury", e);
		}
	}

	protected void createApiProject(ServiceMetaDataWrapper wrapper) throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, "project");
		template.add("projectName", ((ServicesMetaData) wrapper.getMetaData()).getApiCamelCaseName());
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getApiLocation() + File.separator
				+ ((ServicesMetaData) wrapper.getMetaData()).getApiCamelCaseName() + File.separator + ".project";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createProject(ServiceMetaDataWrapper wrapper) throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, "project");
		template.add("projectName", ((ServicesMetaData) wrapper.getMetaData()).getCamelCaseName());
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + File.separator
				+ ((ServicesMetaData) wrapper.getMetaData()).getCamelCaseName() + File.separator + ".project";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createApiGradle(ServiceMetaDataWrapper wrapper, String templateKey) throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getApiLocation() + File.separator
				+ ((ServicesMetaData) wrapper.getMetaData()).getApiCamelCaseName() + File.separator + "build.gradle";
		write(fileName, content);
		Track.me.debug(content);
	}

	protected void createGradle(ServiceMetaDataWrapper wrapper, String templateKey, String version)
			throws RedsGeneratorException {
		ST template = RedsGeneratorUtils.getTemplate(GRADLE_TEMPLATE_PATH, templateKey);
		template.add("serviceName", ((ServicesMetaData) wrapper.getMetaData()).getName());
		template.add("version", version);
		template.add("apiName", ((ServicesMetaData) wrapper.getMetaData()).getApiCamelCaseName());
		String content = template.render();

		String fileName = ((ServicesMetaData) wrapper.getMetaData()).getLocation() + File.separator
				+ ((ServicesMetaData) wrapper.getMetaData()).getCamelCaseName() + File.separator + "build.gradle";
		write(fileName, content);
		Track.me.debug(content);
	}
}
