package com.reds.redsgenerator.core.library;

import com.reds.library.redsutils.RedsStringUtils;
import com.reds.redsgenerator.api.AbstractMetaDataWrapper;
import com.reds.redsgenerator.api.library.LibraryMetaData;

public class LibraryMetaDataWrapper extends AbstractMetaDataWrapper<LibraryMetaData> {
	public LibraryMetaDataWrapper(LibraryMetaData metaData) {
		super(metaData);
		setModuleClassName(RedsStringUtils.capitalize(metaData.getName() + "Module"));
	}
}
