package com.reds.redsgenerator.core.library;

import org.testng.annotations.Test;

import com.reds.redsgenerator.api.exception.RedsGeneratorException;
import com.reds.redsgenerator.api.layouts.LibraryLayout;
import com.reds.redsgenerator.api.library.LibraryMetaData;

public class LibraryGeneratorTest {
	
	//@Test
	public void generateRedsFactory() throws RedsGeneratorException {
		LibraryGenerator generator = new LibraryGenerator();
		LibraryLayout layout = new LibraryLayout();
		String authorName="SHINEED BASHEER";
		String location = "D:\\reds\\redsworkspace\\redsgenerator";
		LibraryMetaData metaData = new LibraryMetaData(authorName,"redsfactory", location, layout);

		// metaData.setName("testlibrary");
		// metaData.setLocation("./temp/library");
		// metaData.setLayout(layout );
		generator.generate(metaData);
		
	}

	@Test
	public void generate() throws RedsGeneratorException {
		LibraryGenerator generator = new LibraryGenerator();
		LibraryLayout layout = new LibraryLayout();
		String authorName="SHINEED BASHEER";
		String location = "D:\\reds\\redsworkspace\\redslibraries";
		LibraryMetaData metaData = new LibraryMetaData(authorName,"testlibrary", location, layout);

		// metaData.setName("testlibrary");
		// metaData.setLocation("./temp/library");
		// metaData.setLayout(layout );
		generator.generate(metaData);
		
	}
}
